package einfuehrung;

import java.awt.Graphics;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author dennis
 */
public class Einfuehrung extends JFrame{
    
    public Einfuehrung(int x, int y, int w, int h) {
        super();
        this.setTitle("Mein erstes Fenster!");
        this.setBounds(x, y, h, h);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        RechteckPanel rp = new RechteckPanel(20, 40, 40, 50);
        this.getContentPane().add(rp);
    }
    
    class RechteckPanel extends JPanel{
        int x, y, w, h;
        
        public RechteckPanel(int x, int y, int w, int h) {
            super();
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            repaint();
        }
        
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawRect(x, y, w, h);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Einfuehrung e = new Einfuehrung(100, 150, 500, 250);
        e.setVisible(true);
    }
}
