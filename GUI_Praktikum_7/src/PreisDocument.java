
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dennis
 */
public class PreisDocument extends PlainDocument {

    @Override
    public void insertString(int offs, String s, AttributeSet a)
            throws BadLocationException {
        String erlaubteZeichen = "[0-9]|\\.|\\,";
        boolean istGueltig = true;

        for (int i = 0; i < s.length() && istGueltig; i++) {
            if (!("" + s.charAt(i)).matches(erlaubteZeichen)) {
                Toolkit.getDefaultToolkit().beep();
                istGueltig = false;
            }
        }
        if (istGueltig) {
            super.insertString(offs, s, a);
        }
    }

}
