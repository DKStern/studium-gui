
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 * Ist die Legende des WeinreifeDiagramms.
 * 
 * @author dennis
 */
public class WeinreifeLegende extends JPanel {

    /**
     * Die Lagerdauer des Weins.
     */
    int lagerdauer;
    /**
     * Der Jahrgang des Weins.
     */
    int jahrgang;
    /**
     * Konstanten für die Farben.
     */
    private Color farbeUnreif = Color.GRAY;
    private Color[] farbeSteigerung = {Color.GRAY, Color.GREEN};
    private Color farbeOptimal = Color.GREEN;
    private Color farbeUeberlagert = Color.YELLOW;
    private Color farbeSchrift = Color.BLACK;
    private Color farbeUmrandung = Color.BLACK;

    /**
     * Variablen zur Positionierung.
     */
    private final float oben = 10f;
    private final float unten = 10f;
    private final float rechts = 10f;
    private final float links = 10f;

    private Dimension d;

    private boolean legendeAnzeigen = true;

    /**
     * Variablen für die X-Koordinaten.
     */
    private float xProzent;
    private float yProzent;

    /**
     * Im Konstruktor werden verschiedene Listener eingebunden
     * die dafuer sorgen das die Legende sichtbar und unsichtbar gemacht
     * werden kann, zudem wird die Legende Fokusierbar gemacht.
     */
    public WeinreifeLegende() {
        super();
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ((WeinreifeLegende) e.getSource()).requestFocusInWindow();
            }
        });
        this.setFocusable(true);
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                farbeUmrandung = Color.RED;
                farbeSchrift = Color.RED;
                repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                farbeUmrandung = Color.BLACK;
                farbeSchrift = Color.BLACK;
                repaint();
            }
        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                evtKeyPressed(e);
            }
        });
    }
    
    /**
     * Ist eine Methode für den KeyListeenr.
     * Diese Methode sorgt dafuer, dass die Legende unsichtbar gemacht werden
     * kann.
     * 
     * @param e KeyEvent 
     */
    private void evtKeyPressed(KeyEvent e) {
        if (!legendeAnzeigen && e.getKeyChar() == '+') {
            legendeAnzeigen = true;
            repaint();
        } else if (legendeAnzeigen && e.getKeyChar() == '-') {
            legendeAnzeigen = false;
            repaint();
        }
    }

    /**
     * Zeichnet die Legende.
     * 
     * @param g Grafikkontext
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        d = this.getSize();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        xProzent = (float) (d.getWidth() / 100);
        yProzent = (float) (d.getHeight() / 100);
        if (legendeAnzeigen) {
            /*
             * Legende Bezeichnungen
             */
            g2d.setPaint(farbeSchrift);

            g2d.drawString(
                    "Legende", links * xProzent, oben * yProzent);
            g2d.drawString(
                    "Zu früh", (links + 5) * xProzent, (oben + 20) * yProzent);
            g2d.drawString(
                    "Steigert sich noch", (links + 5) * xProzent, (oben + 40) * yProzent);
            g2d.drawString(
                    "Optimaler Trinkzeitpunkt",
                    (links + 5) * xProzent, (oben + 60) * yProzent);
            g2d.drawString(
                    "Überlagert", (links + 5) * xProzent, (oben + 80) * yProzent);

            /*
             * Legende zeichnen
             */
            g2d.scale(d.getWidth() / 100, d.getHeight() / 100);

            g2d.setColor(farbeUnreif);

            g2d.fill(
                    new Rectangle2D.Double(links, oben + 5, 4, 15));

            g2d.setPaint(
                    new GradientPaint(
                            10f, 0f, farbeSteigerung[0], 13f, 0f,
                            farbeSteigerung[1]));
            g2d.fill(
                    new Rectangle2D.Double(links, (oben + 25), 4, 15));

            g2d.setColor(farbeOptimal);

            g2d.fill(
                    new Rectangle2D.Double(links, (oben + 45), 4, 15));

            g2d.setColor(farbeUeberlagert);

            g2d.fill(
                    new Rectangle2D.Double(links, (oben + 65), 4, 15));

            /*
             * Schwarze umrandungen
             */
            //Strichstärke setzen
            g2d.setStroke(
                    new BasicStroke(0f));
            g2d.setColor(farbeUmrandung);

            g2d.draw(
                    new Rectangle2D.Double(links, oben + 5, 4, 15));
            g2d.draw(
                    new Rectangle2D.Double(links, (oben + 25), 4, 15));
            g2d.draw(
                    new Rectangle2D.Double(links, (oben + 45), 4, 15));
            g2d.draw(
                    new Rectangle2D.Double(links, (oben + 65), 4, 15));
        }

    }
}
