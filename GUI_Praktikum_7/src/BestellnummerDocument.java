
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dennis
 */
public class BestellnummerDocument extends PlainDocument {

    @Override
    public void insertString(int offset, String s, AttributeSet a) throws BadLocationException {
        String erlaubteZeichen = "[a-zA-Z0-9]";
        boolean gueltig = true;
        for (int i = 0; i < s.length(); i++) {
            if (!("" + s.charAt(i)).matches(erlaubteZeichen)) {
                Toolkit.getDefaultToolkit().beep();
                gueltig = false;
            }
        }
        if (gueltig) {
            super.insertString(offset, s, a);
        }
    }
}
