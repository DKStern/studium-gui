/*
 * TastaturEingabe.java
 *
 * Created on 21. Februar 2006, 12:05
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package utilities;

import java.io.*;

/**
 *
 * @author amh
 */
public class TastaturEingabe {
    
    static BufferedReader eing = new BufferedReader
            (new InputStreamReader(System.in));
    
    public static boolean readBoolean(String prompt) {
        while (true) {
//            System.out.print(prompt);
            System.out.println(prompt);
            try {
                String zeile = eing.readLine();
                if (zeile.equals("j"))
                    return true;
                if (zeile.equals("J"))
                    return true;
                if (zeile.equals("n"))
                    return false;
                if (zeile.equals("N"))
                    return false;
                System.out.println("Bitte J oder N eingeben.");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    
    public static int readInt(String prompt) {
        while (true) {
//            System.out.print(prompt);
            System.out.println(prompt);
            try {
                String zeile = eing.readLine();
                return Integer.parseInt(zeile);
            } catch (NumberFormatException e) {
                System.out.println
                        ("Bitte eine ganze Zahl eingeben.");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    
    public static double readDouble(String prompt) {
        while (true) {
//            System.out.print(prompt);
            System.out.println(prompt);
            try {
                String zeile = eing.readLine();
                return Double.parseDouble(zeile);
            } catch (NumberFormatException e) {
                System.out.println("Bitte eine Zahl " +
                        "eingeben (ggf. mit Dezimalpunkt).");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    
    public static String readString(String prompt) {
        while (true) {
//            System.out.print(prompt);
            System.out.println(prompt);
            try {
                return eing.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    
}
