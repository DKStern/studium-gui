
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.GregorianCalendar;
import javax.swing.JPanel;

/**
 * Diese Klasse erzeugt ein Diagramm.
 *
 * @author dennis
 */
public class WeinreifeDiagramm extends JPanel {

    /**
     * Die Lagerdauer des Weins.
     */
    int lagerdauer;
    /**
     * Der Jahrgang des Weins.
     */
    int jahrgang;
    /**
     * Konstanten für die Farben.
     */
    private static final Color ZUFRUEH = Color.GRAY;
    private static final Color[] STEIGERUNG = {Color.GRAY, Color.GREEN};
    private static final Color OPTIMAL = Color.GREEN;
    private static final Color UEBERLAGERT = Color.YELLOW;
    private static final Color SCHRIFT = Color.BLACK;
    private static final Color UMRANDUNG = Color.BLACK;

    /**
     * Zeichnet das Diagramm.
     *
     * @param g Grafikkontext
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Dimension d = this.getSize();

        /*
         * X-Koordinaten berechnen
         */
        float groesseEinesJahres = 80f / (this.lagerdauer + 1);
        float xKoordinateUnreif = 10;
        float xKoordinateSteigerung =
                10 + groesseEinesJahres * Math.round(lagerdauer / 8.0f);
        float xKoordinateOptimal =
                10 + groesseEinesJahres
                * (Math.round(lagerdauer / 2.0f) - lagerdauer % 2);
        float xKoordinateUeberlagert =
                10 + groesseEinesJahres * this.lagerdauer;
        int aktuellesJahr =
                new GregorianCalendar().get(GregorianCalendar.YEAR);
        float xKoordinateAktuellesJahr =
                10 + (aktuellesJahr - jahrgang) * groesseEinesJahres;


        float xProzent = (float) (d.getWidth() / 100);
        float yProzent = (float) (d.getHeight() / 100);

        /*
         * Unreif Bereich zeichnen.
         */

        g2d.setPaint(ZUFRUEH);
        g2d.fill(new Rectangle2D.Double(
                xKoordinateUnreif * xProzent,
                10 * yProzent,
                (xKoordinateSteigerung - xKoordinateUnreif) * xProzent,
                20 * yProzent));

        /*
         * Steigerungsbereich zeichnen.
         */

        g2d.setPaint(new GradientPaint(xKoordinateSteigerung * xProzent,
                0f,
                STEIGERUNG[0],
                xKoordinateOptimal * xProzent,
                0f,
                STEIGERUNG[1]));
        g2d.fill(new Rectangle2D.Double(
                (xKoordinateSteigerung * xProzent) + 1,
                (10 * yProzent) + 1,
                (xKoordinateOptimal - xKoordinateSteigerung) * xProzent,
                20 * yProzent));

        /*
         * Optimalen Bereich zeichnen.
         */
        g2d.setPaint(OPTIMAL);
        g2d.fill(new Rectangle2D.Double(
                xKoordinateOptimal * xProzent,
                10 * yProzent,
                (xKoordinateUeberlagert - xKoordinateOptimal) * xProzent,
                20 * yProzent));

        /*
         * Ueberlagerten Bereich zeichnen.
         */
        g2d.setPaint(UEBERLAGERT);
        g2d.fill(new Rectangle2D.Double(
                xKoordinateUeberlagert * xProzent,
                10 * yProzent,
                groesseEinesJahres * xProzent,
                20 * yProzent));

        /*
         * Rahmen zeichnen.
         */
        g2d.setPaint(UMRANDUNG);
        g2d.draw(new Rectangle2D.Double(10 * xProzent,
                10 * yProzent,
                80 * xProzent,
                20 * yProzent));

        /*
         * Trenner einzeichnen.
         */
        g2d.setPaint(UMRANDUNG);
        g2d.draw(new Line2D.Double(
                xKoordinateSteigerung * xProzent,
                10 * yProzent,
                xKoordinateSteigerung * xProzent,
                30 * yProzent));
        g2d.draw(new Line2D.Double(
                xKoordinateOptimal * xProzent,
                10 * yProzent,
                xKoordinateOptimal * xProzent,
                30 * yProzent));
        g2d.draw(new Line2D.Double(
                xKoordinateUeberlagert * xProzent,
                10 * yProzent,
                xKoordinateUeberlagert * xProzent,
                30 * yProzent));



        /*
         * Beschriftungen einzeichnen.
         */
        g2d.setPaint(Color.BLACK);

        // Unreif
        g2d.drawString(
                "" + this.jahrgang,
                xKoordinateUnreif * xProzent,
                35 * yProzent);

        // Steigert sich noch
        g2d.drawString(
                "" + (this.jahrgang + Math.round(this.lagerdauer / 8.0)),
                xKoordinateSteigerung * xProzent,
                35 * yProzent);

        // Optimal
        /*
         * Wenn Lagerdauer 2 beträgt muss die Lagerdauer für die Anzeige 
         * umgerechnet werden.
         */
        int lagerdauerTemp;
        if (lagerdauer == 2 || lagerdauer == 4) {
            lagerdauerTemp = lagerdauer + 1;
        } else {
            lagerdauerTemp = lagerdauer;
        }
        g2d.drawString(
                "" + (this.jahrgang + Math.round(lagerdauerTemp / 2.0) - 1),
                xKoordinateOptimal * xProzent,
                35 * yProzent);

        // Ueberlagert
        g2d.drawString(
                "" + (this.jahrgang + this.lagerdauer),
                xKoordinateUeberlagert * xProzent,
                35 * yProzent);

        /*
         * Aktuelles Jahr einzeichnen.
         */
        if (aktuellesJahr >= jahrgang
                && aktuellesJahr <= (jahrgang + lagerdauer)) {
            g2d.setColor(Color.RED);
            g2d.draw(new Rectangle2D.Double(xKoordinateAktuellesJahr * xProzent,
                    9.9 * yProzent,
                    groesseEinesJahres * xProzent,
                    20.3 * yProzent));
            // aktuelles Jahr schreiben
            g2d.setPaint(Color.RED);
            g2d.drawString(
                    "" + aktuellesJahr,
                    xKoordinateAktuellesJahr * xProzent,
                    35 * yProzent);
        }

        /*
         * Legende Bezeichnungen
         */
        g2d.setPaint(SCHRIFT);
        g2d.drawString("Legende", 10 * xProzent, 47 * yProzent);
        g2d.drawString("Zu früh", 15 * xProzent, 57 * yProzent);
        g2d.drawString("Steigert sich noch", 15 * xProzent, 67 * yProzent);
        g2d.drawString("Optimaler Trinkzeitpunkt",
                15 * xProzent, 77 * yProzent);
        g2d.drawString("Überlagert", 15 * xProzent, 87 * yProzent);

        /*
         * Legende zeichnen
         */
        g2d.scale(d.getWidth() / 100, d.getHeight() / 100);

        g2d.setColor(ZUFRUEH);
        g2d.fill(new Rectangle2D.Double(10, 52, 3, 5));

        g2d.setPaint(new GradientPaint(
                10f, 0f, STEIGERUNG[0], 13f, 0f, STEIGERUNG[1]));
        g2d.fill(new Rectangle2D.Double(10, 62, 3, 5));

        g2d.setColor(OPTIMAL);
        g2d.fill(new Rectangle2D.Double(10, 72, 3, 5));

        g2d.setColor(UEBERLAGERT);
        g2d.fill(new Rectangle2D.Double(10, 82, 3, 5));

        /*
         * Schwarze umrandungen
         */
        //Strichstärke setzen
        g2d.setStroke(new BasicStroke(0f));
        g2d.setColor(UMRANDUNG);
        g2d.draw(new Rectangle2D.Double(9.9, 51.9, 3, 5));
        g2d.draw(new Rectangle2D.Double(9.9, 61.9, 3, 5));
        g2d.draw(new Rectangle2D.Double(9.9, 71.9, 3, 5));
        g2d.draw(new Rectangle2D.Double(9.9, 81.9, 3, 5));
    }

    /**
     * Diese Methode prüft und setzt den Jahrgang des Weins.
     *
     * @param jahrgang <= aktuelles Jahr && > 1899
     * @throws IllegalArgumentException wenn der Wert nicht gültig ist
     */
    public void setzeJahrgang(int jahrgang) throws IllegalArgumentException {
        int aktuellesJahr = new GregorianCalendar().get(GregorianCalendar.YEAR);

        if (jahrgang <= aktuellesJahr && jahrgang > 1899) {
            this.jahrgang = jahrgang;
        } else {
            throw new IllegalArgumentException("Jahrgang hat einen"
                    + "ungültigen Wert!");
        }
    }

    /**
     * Die Methode prüft und setzt die Lagerdauer des Weins.
     *
     * @param lagerdauer min. 1 Jahr max 25 Jahre
     * @throws IllegalArgumentException wenn der Wert nicht gültig ist
     */
    public void setzeLagerdauer(int lagerdauer)
            throws IllegalArgumentException {
        if (lagerdauer >= 1 && lagerdauer <= 25) {
            this.lagerdauer = lagerdauer;
        } else {
            throw new IllegalArgumentException("Lagerdauer hat einen"
                    + "ungültigen Wertebereich!");
        }
    }
}
