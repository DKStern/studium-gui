import java.awt.Dimension;
import javax.swing.JFrame;
import utilities.TastaturEingabe;

/**
 * Diese Klasse erzeugt ein Diagramm und nimmt Eingaben entgegeben.
 * 
 * @author dennis
 */
public class Weinreife extends JFrame {

    /**
     * Breite des Fensters.
     */
    private static final int BREITE = 800;
    /**
     * Höhe des Fensters.
     */
    private static final int HOEHE = 500;

    /**
     * Erzeugt ein neues Fenster.
     * 
     * @param w Breite des Fensters
     * @param h Hoehe des Fensters
     */
    public Weinreife(int w, int h) {

        Dimension d = this.getToolkit().getScreenSize();
        int x = (int) d.getWidth() / 2 - w / 2;
        int y = (int) d.getHeight() / 2 - h / 2;

        this.setTitle("Weinreife");
        this.setBounds(x, y, w, h);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * Erzeugt ein Fenster, erfragt Jahrgang und Lagerdauer und erzeugt das
     * Diagramm.
     * 
     * @param args wird nicht verwendet.
     */
    public static void main(String[] args) {
        Weinreife fenster = new Weinreife(BREITE, HOEHE);
        WeinreifeDiagramm diagramm = new WeinreifeDiagramm();
        fenster.setContentPane(diagramm);

        do {
            fenster.setVisible(false);

            int jahrgang;
            int lagerdauer;

            /*
             * Es wird der Jahrgang erfragt, dem Diagramm weitergegeben
             * und geprüft ob die Eingabe gültig ist.
             * Falls die Eingabe nicht gültig war, wird die Eingabe wiederholt
             * bist die Eingabe gültig ist.
             */
            while (true) {
                try {
                    jahrgang = TastaturEingabe.readInt("Jahrgang: ");
                    diagramm.setzeJahrgang(jahrgang);
                    break;
                } catch (IllegalArgumentException e) {
                    System.out.println("Der Jahgrang darf nicht über das "
                            + "aktuell Jahr hinaus gehen und muss mindestens"
                            + "ab dem Jahr 1900 beginnen.");
                }
            }

            /*
             * Es wird die Lagerdauer erfragt, dem Diagramm weitergegeben
             * und geprüft ob die Eingabe gültig ist.
             * Falls die Eingabe nicht gültig war, wird die Eingabe wiederholt
             * bist die Eingabe gültig ist.
             */
            while (true) {
                try {
                    lagerdauer = TastaturEingabe.readInt("Lagerdauer: ");
                    diagramm.setzeLagerdauer(lagerdauer);
                    break;
                } catch (IllegalArgumentException e) {
                    System.out.println("Die Lagerdauer muss mindestens 1 Jahr"
                            + "oder maximal 25 Jahre betragen.");
                }
            }

            /*
             * Fenster sichtbar machen und Diagramm neu zeichnen
             */
            fenster.setVisible(true);
            diagramm.repaint();
        } while (TastaturEingabe.readBoolean("Möchten Sie ein neues "
                + "Diagramm zeichnen?"));
        System.exit(0);
    }
}
