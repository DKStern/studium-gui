
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Prüft Eingaben auf Gültigkeit.
 * 
 * @author dennis
 */
public class EingabeVerifier extends InputVerifier{
    
    private static final String REGEX_KOMMA = "(\\d*,?\\d{0,2})|(\\d{0,3}(\\.\\d{3})*,?\\d{0,2})";

    @Override
    public boolean verify(JComponent input) {
        return ((JTextField) input).getText().matches(REGEX_KOMMA);
    }
}
