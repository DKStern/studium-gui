
/**
 *
 * @author dennis
 */
public class Wein {

    private String bestellnummer;
    private int jahrgang;
    private String name;
    private String farbe;
    private String[] rebsorten;
    private String anbaugebiet;
    private String alkoholgehalt;
    private int lagerfaehigkeit;
    private String flaschengroesse;
    private double flaschenpreis;

    public Wein(String bestellnummer, int jahrgang, String name, String farbe,
            String[] rebsorten, String anbaugebiet, String alkoholgehalt,
            int lagerfaehigkeit, String flaschengroesse, double flaschenpreis) {
        this.bestellnummer = bestellnummer;
        this.jahrgang = jahrgang;
        this.name = name;
        this.farbe = farbe;
        this.rebsorten = rebsorten;
        this.anbaugebiet = anbaugebiet;
        this.alkoholgehalt = alkoholgehalt;
        this.lagerfaehigkeit = lagerfaehigkeit;
        this.flaschengroesse = flaschengroesse;
        this.flaschenpreis = flaschenpreis;
    }

    @Override
    public String toString() {
        String wein;

        wein = "Bestellnr: " + bestellnummer + "\n"
                + "Jahrgang: " + jahrgang + "\n"
                + "Name: " + name + "\n"
                + "Farbe: " + farbe + "\n"
                + "Rebsorte: " + getRebsorten() + "\n"
                + "Anbaugebiet: " + anbaugebiet + "\n"
                + "Alkoholgehalt: " + alkoholgehalt + "\n"
                + "Lagerfähigkeit: " + lagerfaehigkeit + "\n"
                + "Flaschengrröße: " + flaschengroesse + "\n"
                + "Flaschenpreis: " + flaschenpreis + "€";

        return wein;
    }

    public String getRebsorten() {
        String reb = "";
        if(rebsorten[0] != null) {
            reb = rebsorten[0];
        }
        for (int i = 1; i < rebsorten.length && rebsorten[i] != null; i++) {
            reb += ", " + rebsorten[i];
        }

        return reb;
    }
}
