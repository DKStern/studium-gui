
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
//import java.awt.List;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
//import javax.swing.ButtonGroup;
import javax.swing.InputVerifier;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dennis
 */
public class Weinverwaltung extends javax.swing.JFrame {

    private final String CLOSE_MASSAGE = "Möchten Sie das Programm wirklich beenden?";

    private final String CLOSE_TITLE = "Programm Beenden?";

    private final String ABORT_MESSAGE = "Möchten Sie die Weinaufnahme wirklich "
            + "abbrechen?";

    private final String ABORT_TITLE = "Weinaufnahme Beenden?";

    private final String INFO_MESSAGE = "The cake ist a lie! \n Version 0.01";

    private final String INFO_TITLE = "The cake is a lie!";

    private final String LAGERDAUER_MESSAGE = "Die Lagerdauer muss über den aktuellen Jahr liegen \n"
            + "und darf nicht länger als 25 Jahre haltbar sein.";

    private final String LAGERDAUER_TITLE = "Ungültige Lagerdauer";

    private final String BESTELLNR_MESSAGE = "Bitte geben Sie eine gültige"
            + "Bestellnummer ein.\n"
            + "Das Format der Bestellnummer muss wie folgt aussehen:"
            + "\nAA99 oder aa99 (2 Buchstaben 2 Zahlen)";
    private final String BESTELLNR_TITLE = "Ungültige Bestellnummer";

    private final String JAHR_MESSAGE = "Bitte geben Sie ein gültigen Jahrgang ein!"
            + "\nDer Jahrgang darf 25 vor Jahre bis 25 nach dem aktuellen Jahr liegen.";

    private final String JAHR_TITLE = "Ungültiges Jahr!";

    private final String ERROR_MESSAGE = "Es wurde(n) ein oder mehrere "
            + "ungültige(r) Wert(e) gefunden!";

    private final String ERROR_TITLE = "Ungültige(r) Wert(e) gefunden!";

    private final BestellnummerVerifier bestellNrVerifier;

    private final JahrgangVerifier jahrVerifier;

    private final LagerdauerVerifier lagerdauerVerifier;

    private final String[] ARGENTINIENREG = new String[]{
        "Jujuy", "Salta",
        "Catamarca", "La Rioja", "San Juan", "Mendoza", "Valle De Uco",
        "San Rafael", "Rio Negro"};
    private final String[] AUSTRALIENREG = new String[]{"Hunter Valley",
        "Swan River Valley", "Barossa Valley"};
    private final String[] CHILEREG = new String[]{"Valle del Elqui",
        "Valle de Casablanca", "Valle de San Antonio", "Valle del Maipo",
        "Valle de Rapel", "Valle de Curicó", "Valle del Maule", "Valle del Itata",
        "Valle del Bío-Bío", "Valle del Malleco"};
    private final String[] DEUTSCHLANDREG = new String[]{"Ahrtal", "Baden", "Bayern",
        "Main", "Moseltal", "Nordfriesland-Holstein", "Saar", "Ruwer",
        "Mitteldeutschland", "Nahegau", "Pfalz", "Regensburg", "Rheingau",
        "Rhein", "Saarland-Mosel", "Sachsen", "Schwaben", "Starkenburg",
        "Taubertal"};
    private final String[] FRANKREICHREG = new String[]{"Margaux", "Pauillac",
        "Saint-Julien", "Saint-Estèphe", "Listrac", "Chablis", "Côte de Nuits",
        "Côte de Beaune", "Côte Chalonnaise", "Mâconnais", "Côtes du Marmandais",
        "Floc de Gascogne", "Gaillac", "Irouléguy", "Jurançon", "Côtes du Rhône",
        "Côte du Rhône Villages", "Châteauneuf-du-Pape", "Condrieu", "Cornas"};
    private final String[] GRIECHENLANDREG = new String[]{"Aminteon", "Goumenissa",
        "Naoussa", "Cotes de Meliton", "Anchialos", "Rapsani", "Zitsa",
        "Mantinia", "Nemea", "Patras", "Robola von Kefallonia", "Archánes",
        "Daphnes", "Peza", "Sitia", "Paros", "Rhodos", "Santorini",
        "Kantza", "Samos"};
    private final String[] ITALIENREG = new String[]{"Aostatal", "Piemont",
        "Ligurien", "Lombardei", "Trentino", "Südtirol", "Venetien",
        "Emilia-Romagna", "Marken", "Toskana", "Umbrien", "Latium", "Abruzzen",
        "Molise", "Apulien", "Kampanien", "Kalabrien", "Basilikata", "Sizilien",
        "Sardinien"};
    private final String[] KROATIENREG = new String[]{"Srijemsko vinogorje",
        "Erdutsko vinogorje", "Baranjsko vinogorje", "Đakovačko vinogorje",
        "Slavonskobrodsko vinogorje", "Novogradiško vinogorje",
        "Požeško-pleterničko vinogorje", "Kutjevačko vinogorje",
        "Daruvarsko vinogorje", "Pakračko vinogorje", "Feričanačko vinogorje",
        "Orahovičko-slatinsko vinogorje", "Virovitičko vinogorje",
        "Volodersko-Ivanićgradsko vinogorje", "Čazmansko-Garešničko vinogorje",
        "Dugoselsko-Vrbovečko vinogorje", "Kalničko vinogorje",
        "Koprivničko-Đurđevačko vinogorje",
        "Bjelovarsko-Grubišnopoljsko vinogorje", "Zelinsko vinogorje"};
    private final String[] MAZEDONIENREG = new String[]{"Gevevelija-Valandovo",
        "Kochani-Vinica", "Ovce Pole", "Skopje", "Strumica-Radoviste", "Tikveš",
        "Titov Veles", "Veles", "Kocani", "Kratovo", "Kumanovo", "Pijanecki",
        "Bitola", "Ohrid", "Tetovo", "Kicevo", "Prilep", "Prespa"};
    private final String[] MEXIKOREG = new String[]{"Aguascalientes", "Coahuila",
        "Sonora", "Durango", "Zacatecas", "Querétaro"};
    private final String[] NEUSEELANDREG = new String[]{"Northland", "Auckland",
        "Gisborne", "Hawke’s Bay", "Marlborough", "Nelson", "Canterbury",
        "Central Otago"};
    private final String[] OESTERREICH = new String[]{"Carnuntum", "Kamptal",
        "Kremstal", "Thermenregion", "Traisental", "Wachau", "Wagram",
        "Weinviertel", "Mittelburgenland", "Neusiedler See", "Südburgenland",
        "Wien", "Mittelburgenland", "Südoststeiermark", "Südsteiermark",
        "Weststeiermark"};
    private final String[] PORTUGALREG = new String[]{"Tavora-Varosa", "Barraida",
        "Dão", "Alto Douro", "Beira Interior", "Ribatejo", "Lourinha", "Obidos",
        "Alenquer", "Torres Vedras", "Arruda", "Bucelas", "Palmela", "Sétubal",
        "Alentejo", "Lagos", "Portimão", "Lagoa", "Tavira", "Madeira",
        "Pico Collares"};
    private final String[] RUMÄNIENREG = new String[]{"Moldova", "Walachei",
        "Dobrogea", "Banat", "Crișana"};
    private final String[] SCHWEIZREG = new String[]{"Ostschweiz", "Graubünden",
        "Drei-Seen-Land", "Waadt", "Genf", "Wallis", "Tessin", "Aargau"};
    private final String[] SPANIENREG = new String[]{"Andalusien", "Aragón",
        "Cataluña", "Extremadura", "Galicia", "Castilla-La Mancha",
        "Vinos de Madrid", "Murcia", "Navarra", "Ribera del Duero",
        "del Bierzo", "Rioja", "Utiel-Requena", "Valdepeñas", "Valencia",
        "Yecla", "Mallorca", "Ibiza", "Menorca", "Formentera"};
    private final String[] SUEDAFRIEKAREG = new String[]{"Western Cape",
        "Breede Rivier Valley", "Klein Karoo", "Boberg", "Olifants River",
        "Cape Agulhas"};
    private final String[] TSCHECHIENREG = new String[]{"Brno", "Bzenec", "Mikulov",
        "Mutěnice", "Velké Pavlovice", "Znojmo", "Strážnice", "Kyjov",
        "Uherské Hradiště", "Podluží", "Valtice", "Prag", "Čáslav", "Mělník",
        "Roudnice nad Labem", "Velké Žernoseky"};
    private final String[] UNGARNREG = new String[]{"Tokaj-Hegyalja", "Plattensee",
        "Baranya"};
    private final String[] USAREG = new String[]{"Arizona", "Idaho", "Kalifornien",
        "New Mexico", "New York", "North Carolina", "Ohio", "Oregon", "Texas",
        "Virginia", "Washington"};
    private final String[] LAENDER = new String[]{
        "Argentinien", "Australien", "Chile", "Deutschland", "Frankreich",
        "Griechenland", "Italien", "Kroatien", "Mazedonien", "Mexiko",
        "Neuseeland", "Österreich", "Portugal", "Rumänien", "Schweiz",
        "Spanien", "Südafrika", "Tschechien", "Ungarn", "USA"};
    private final String[] REBSORTEN = new String[]{
        "", "Cabernet Sauvignon", "Brun Fourca", "Blauer Arbst",
        "Spätburgunder"};

    private boolean firstRun = true;

    private boolean preisUngueltig = false;

    private boolean comboBoxChanged = false;

    private final ArrayList<String[]> REB_LAENDER
            = new ArrayList<>();

    private final HashMap<String, String[]> LAND_UND_REGION
            = new HashMap<>();

    private final ArrayList<JComponent> PFLICHTFELDER = new ArrayList<>();

    private class BestellnummerVerifier extends InputVerifier {

        private final String REGEX = "([A-Z]{2}|[a-z]{2})[0-9]{2}";

        @Override
        public boolean verify(JComponent component) {
            return ((JTextField) component).getText().matches(REGEX);
        }
    }

    private class JahrgangVerifier extends InputVerifier {

        private final String REGEX = "[0-9]{4}";

        @Override
        public boolean verify(JComponent component) {
            boolean gueltig = false;
            String eingabe = ((JTextField) component).getText();

            if (eingabe.matches(REGEX)) {
                int aktuellesJahr
                        = new GregorianCalendar().get(GregorianCalendar.YEAR);
                int jahr = Integer.parseInt(eingabe);
                gueltig = jahr >= (aktuellesJahr - 25) && jahr <= aktuellesJahr;
            }

            return gueltig;
        }
    }

    private class LagerdauerVerifier extends InputVerifier {

        private final String REGEX = "[0-9]{4}";

        @Override
        public boolean verify(JComponent component) {
            boolean istGueltig = false;
            String inhalt = ((JTextField) component).getText();
            if (inhalt.matches(REGEX)) {
                if (!jTJahrgang.getText().equals("")) {
                    int lagerdauer = Integer.parseInt(inhalt);
                    int jahrgang = Integer.parseInt(jTJahrgang.getText());
                    int aktuellesJahr
                            = new GregorianCalendar().get(GregorianCalendar.YEAR);
                    istGueltig = (aktuellesJahr - 25) <= lagerdauer
                            && (aktuellesJahr + 25) >= lagerdauer
                            && jahrgang <= lagerdauer;
                }
            }
            return istGueltig;
        }

    }

    private class FocusOrder extends FocusTraversalPolicy {

        ArrayList<JComponent> order;

        public FocusOrder(ArrayList<JComponent> order) {
            this.order = order;
        }

        @Override
        public Component getComponentAfter(Container aContainer, Component aComponent) {
            int index = order.indexOf(aComponent);
            if(!order.get((index + 1) % order.size()).isEnabled()) {
                boolean enabled = false;
                while(!enabled) {
                    index = (index + 1) % order.size();
                    enabled = order.get(index).isEnabled();
                }
            } else {
                index = (order.indexOf(aComponent) + 1) % order.size();
            }
            return order.get(index);
        }

        @Override
        public Component getComponentBefore(Container aContainer, Component aComponent) {
            int index = order.indexOf(aComponent);
            if(!order.get((index - 1) % order.size()).isEnabled()) {
                boolean enabled = false;
                while(!enabled) {
                    index = (index - 1) % order.size();
                    enabled = order.get(index).isEnabled();
                }
            } else {
                index = (order.indexOf(aComponent) - 1) % order.size();
            }
            return order.get(index);
        }

        @Override
        public Component getFirstComponent(Container aContainer) {
            return order.get(0);
        }

        @Override
        public Component getLastComponent(Container aContainer) {
            return order.get(order.size() - 1);
        }

        @Override
        public Component getDefaultComponent(Container aContainer) {
            return order.get(0);
        }

    }

    /**
     * Creates new form Weinverwaltung
     */
    public Weinverwaltung() {
        fuelleRebLaender();
        fuelleLandUndRegionBeziehung(REB_LAENDER);
        initComponents();
        jTFlaschenpreis.setInputVerifier(new PreisVerifier());
        jTFlaschenpreis.setDocument(new PreisDocument());
        fuelleLaender(LAENDER);
        bestellNrVerifier = new BestellnummerVerifier();
        jahrVerifier = new JahrgangVerifier();
        lagerdauerVerifier = new LagerdauerVerifier();
        jTBestellnummer.setDocument(new BestellnummerDocument());
        jTJahrgang.setDocument(new JahrDocument());
        resetVisibility();

        // Pflichtfelder füllen
        PFLICHTFELDER.add(jTBestellnummer);
        PFLICHTFELDER.add(jTJahrgang);
        PFLICHTFELDER.add(jTName);
        PFLICHTFELDER.add(jRWeiss);
        PFLICHTFELDER.add(jCAnbaugebiet);
        PFLICHTFELDER.add(jCAlkoholgehalt);
        PFLICHTFELDER.add(jTLagerfaehigkeit);
        PFLICHTFELDER.add(jCFlaschengroesse);
        PFLICHTFELDER.add(jTFlaschenpreis);
        
        fuelleJCRebsorten(REBSORTEN);

        firstRun = false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        desktopPane = new javax.swing.JDesktopPane();
        jIFweinAufnehmen = new javax.swing.JInternalFrame();
        jTPWeinaufnahme = new javax.swing.JTabbedPane();
        jPFormular = new javax.swing.JPanel();
        jLBestellnummer = new javax.swing.JLabel();
        jTBestellnummer = new javax.swing.JTextField();
        jLJahrgang = new javax.swing.JLabel();
        jTJahrgang = new javax.swing.JTextField();
        jLName = new javax.swing.JLabel();
        jTName = new javax.swing.JTextField();
        jLFarbe = new javax.swing.JLabel();
        jRWeiss = new javax.swing.JRadioButton();
        jRRot = new javax.swing.JRadioButton();
        jRRose = new javax.swing.JRadioButton();
        jLRebsorte = new javax.swing.JLabel();
        jLAnbaugebiet = new javax.swing.JLabel();
        jCAnbaugebiet = new javax.swing.JComboBox();
        jLAlkoholgehalt = new javax.swing.JLabel();
        jCAlkoholgehalt = new javax.swing.JComboBox();
        jLLagerfaehigkeit = new javax.swing.JLabel();
        jTLagerfaehigkeit = new javax.swing.JTextField();
        jLFlaschengroesse = new javax.swing.JLabel();
        jCFlaschengroesse = new javax.swing.JComboBox();
        jLFlaschenpreis = new javax.swing.JLabel();
        jTFlaschenpreis = new javax.swing.JTextField();
        jBAbbrechen = new javax.swing.JButton();
        jBSpeichern = new javax.swing.JButton();
        jLUngueltigBestellNr = new javax.swing.JLabel();
        jLUngueltigJahrgang = new javax.swing.JLabel();
        jCRebsorte1 = new javax.swing.JComboBox();
        jCRebsorte2 = new javax.swing.JComboBox();
        jCRebsorte3 = new javax.swing.JComboBox();
        jLHilfeBestellNr = new javax.swing.JLabel();
        jLHilfeJahrgang = new javax.swing.JLabel();
        jLAusfuellenName = new javax.swing.JLabel();
        jLAuswaehlenAnbaugebiet = new javax.swing.JLabel();
        jLUngueltigLagerfaehigkeit = new javax.swing.JLabel();
        jLHilfeLagerfaehigkeit = new javax.swing.JLabel();
        jLUngueltigFlaschenpreis = new javax.swing.JLabel();
        jLEuro = new javax.swing.JLabel();
        jLComboboxDoppelt = new javax.swing.JLabel();
        jLRegion = new javax.swing.JLabel();
        jCRegion = new javax.swing.JComboBox();
        jPDiagramm = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        dateiMenue = new javax.swing.JMenu();
        beendenMenuItem = new javax.swing.JMenuItem();
        bearbeitenMenue = new javax.swing.JMenu();
        kundenMenue = new javax.swing.JMenu();
        kundenAufnehmenMenuItem = new javax.swing.JMenuItem();
        kundenAendernMenuItem = new javax.swing.JMenuItem();
        kundenLoeschenMenuItem = new javax.swing.JMenuItem();
        weinMenue = new javax.swing.JMenu();
        weinAufnehmenMenuItem = new javax.swing.JMenuItem();
        weinAendernMenuItem = new javax.swing.JMenuItem();
        weinLoeschenMenuItem = new javax.swing.JMenuItem();
        hilfeMenue = new javax.swing.JMenu();
        hilfeMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Weinverwaltung");
        setPreferredSize(new java.awt.Dimension(800, 800));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jIFweinAufnehmen.setClosable(true);
        jIFweinAufnehmen.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jIFweinAufnehmen.setIconifiable(true);
        jIFweinAufnehmen.setMaximizable(true);
        jIFweinAufnehmen.setResizable(true);
        jIFweinAufnehmen.setTitle("Wein aufnehmen");
        jIFweinAufnehmen.setMinimumSize(new java.awt.Dimension(143, 100));
        jIFweinAufnehmen.setNormalBounds(new java.awt.Rectangle(100, 10, 500, 500));
        jIFweinAufnehmen.setPreferredSize(new java.awt.Dimension(550, 600));
        jIFweinAufnehmen.setVisible(false);
        jIFweinAufnehmen.addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                jIFweinAufnehmenInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jTPWeinaufnahme.setPreferredSize(new java.awt.Dimension(800, 600));

        jPFormular.setFocusTraversalPolicy(new FocusOrder(getOrder()));
        jPFormular.setFocusTraversalPolicyProvider(true);

        jLBestellnummer.setText("Bestellnummer");

        jTBestellnummer.setMargin(new java.awt.Insets(0, 0, 0, 10));
        jTBestellnummer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTBestellnummerFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTBestellnummerFocusLost(evt);
            }
        });

        jLJahrgang.setText("Jahrgang");

        jTJahrgang.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTJahrgangFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTJahrgangFocusLost(evt);
            }
        });

        jLName.setText("Name");

        jTName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTNameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTNameFocusLost(evt);
            }
        });

        jLFarbe.setText("Farbe");

        buttonGroup1.add(jRWeiss);
        jRWeiss.setSelected(true);
        jRWeiss.setText("weiß");

        buttonGroup1.add(jRRot);
        jRRot.setText("rot");

        buttonGroup1.add(jRRose);
        jRRose.setText("rose");

        jLRebsorte.setText("Rebsorte");

        jLAnbaugebiet.setText("Anbaugebiet");

        jCAnbaugebiet.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCAnbaugebietItemStateChanged(evt);
            }
        });
        jCAnbaugebiet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCAnbaugebietActionPerformed(evt);
            }
        });

        jLAlkoholgehalt.setText("Alkoholgehalt");

        jCAlkoholgehalt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0 Vol%", "7,5 Vol%", "8 Vol%", "8,5 Vol%", "9 Vol%", "9,5 Vol%", "10 Vol%", "10,5 Vol%", "11 Vol%", "11,5 Vol%", "12 Vol%", "12,5 Vol%", "13 Vol%" }));
        jCAlkoholgehalt.setSelectedIndex(6);
        jCAlkoholgehalt.setSelectedItem(6);
        jCAlkoholgehalt.setToolTipText("");
        jCAlkoholgehalt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCAlkoholgehaltActionPerformed(evt);
            }
        });

        jLLagerfaehigkeit.setText("Lagerfähigkeit");

        jTLagerfaehigkeit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTLagerfaehigkeitFocusGained(evt);
            }
        });

        jLFlaschengroesse.setText("Flaschengröße");

        jCFlaschengroesse.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0,187 l", "0,25 l", "0,375 l", "0,5 l", "0,62 l", "0,7 l", "0,75 l", "0,8 l", "1 l", "1,5 l" }));
        jCFlaschengroesse.setSelectedIndex(5);
        jCFlaschengroesse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCFlaschengroesseActionPerformed(evt);
            }
        });

        jLFlaschenpreis.setText("Flaschenpreis");

        jTFlaschenpreis.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTFlaschenpreisFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFlaschenpreisFocusLost(evt);
            }
        });

        jBAbbrechen.setText("Abbrechen");
        jBAbbrechen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAbbrechenActionPerformed(evt);
            }
        });

        jBSpeichern.setText("Speichern");
        jBSpeichern.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSpeichernActionPerformed(evt);
            }
        });

        jLUngueltigBestellNr.setForeground(new java.awt.Color(255, 3, 0));
        jLUngueltigBestellNr.setText("ungültig!");

        jLUngueltigJahrgang.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigJahrgang.setText("ungültig!");

        jCRebsorte1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRebsorte1ActionPerformed(evt);
            }
        });

        jCRebsorte2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte2.setEnabled(false);
        jCRebsorte2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRebsorte2ActionPerformed(evt);
            }
        });

        jCRebsorte3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte3.setEnabled(false);

        jLHilfeBestellNr.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeBestellNr.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeBestellNr.setText("?");
        jLHilfeBestellNr.setToolTipText("Hilfe");
        jLHilfeBestellNr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeBestellNrMouseClicked(evt);
            }
        });

        jLHilfeJahrgang.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeJahrgang.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeJahrgang.setText("?");
        jLHilfeJahrgang.setToolTipText("Hilfe");
        jLHilfeJahrgang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLHilfeJahrgangMousePressed(evt);
            }
        });

        jLAusfuellenName.setForeground(new java.awt.Color(255, 0, 0));
        jLAusfuellenName.setText("bitte ausfüllen!");

        jLAuswaehlenAnbaugebiet.setForeground(new java.awt.Color(255, 0, 0));
        jLAuswaehlenAnbaugebiet.setText("bitte auswählen!");

        jLUngueltigLagerfaehigkeit.setForeground(new java.awt.Color(250, 0, 0));
        jLUngueltigLagerfaehigkeit.setText("ungültig!");

        jLHilfeLagerfaehigkeit.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeLagerfaehigkeit.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeLagerfaehigkeit.setText("?");
        jLHilfeLagerfaehigkeit.setToolTipText("Hilfe");
        jLHilfeLagerfaehigkeit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeLagerfaehigkeitMouseClicked(evt);
            }
        });

        jLUngueltigFlaschenpreis.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigFlaschenpreis.setText("ungültig!");

        jLEuro.setText("€");

        jLComboboxDoppelt.setForeground(new java.awt.Color(255, 0, 0));
        jLComboboxDoppelt.setText("doppelte Einträge!");

        jLRegion.setText("Region");

        jCRegion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bitte Anbaugebiet auswählen" }));

        javax.swing.GroupLayout jPFormularLayout = new javax.swing.GroupLayout(jPFormular);
        jPFormular.setLayout(jPFormularLayout);
        jPFormularLayout.setHorizontalGroup(
            jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLAnbaugebiet)
                    .addGroup(jPFormularLayout.createSequentialGroup()
                        .addComponent(jLRebsorte)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCRebsorte2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCRebsorte3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPFormularLayout.createSequentialGroup()
                                .addComponent(jCRebsorte1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLComboboxDoppelt))))
                    .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPFormularLayout.createSequentialGroup()
                            .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLFlaschengroesse)
                                .addComponent(jLFlaschenpreis))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPFormularLayout.createSequentialGroup()
                                    .addComponent(jBAbbrechen)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jBSpeichern))
                                .addComponent(jTFlaschenpreis, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCFlaschengroesse, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLEuro)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLUngueltigFlaschenpreis))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPFormularLayout.createSequentialGroup()
                            .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLAlkoholgehalt)
                                .addComponent(jLLagerfaehigkeit))
                            .addGap(18, 18, 18)
                            .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPFormularLayout.createSequentialGroup()
                                    .addComponent(jTLagerfaehigkeit, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLUngueltigLagerfaehigkeit)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLHilfeLagerfaehigkeit))
                                .addComponent(jCAlkoholgehalt, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPFormularLayout.createSequentialGroup()
                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLJahrgang)
                            .addComponent(jLName)
                            .addComponent(jLFarbe)
                            .addComponent(jLBestellnummer)
                            .addComponent(jLRegion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPFormularLayout.createSequentialGroup()
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTName, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTJahrgang, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTBestellnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jRWeiss, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(22, 22, 22)
                                        .addComponent(jRRot)
                                        .addGap(18, 18, 18)
                                        .addComponent(jRRose))
                                    .addComponent(jCAnbaugebiet, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jLUngueltigJahrgang)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLHilfeJahrgang))
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jLUngueltigBestellNr)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLHilfeBestellNr))
                                    .addComponent(jLAusfuellenName)
                                    .addComponent(jLAuswaehlenAnbaugebiet))))))
                .addContainerGap(233, Short.MAX_VALUE))
        );

        jPFormularLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jCAlkoholgehalt, jCAnbaugebiet, jCFlaschengroesse, jTBestellnummer, jTFlaschenpreis, jTJahrgang, jTLagerfaehigkeit, jTName});

        jPFormularLayout.setVerticalGroup(
            jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLBestellnummer)
                    .addComponent(jTBestellnummer)
                    .addComponent(jLUngueltigBestellNr)
                    .addComponent(jLHilfeBestellNr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTJahrgang)
                    .addComponent(jLJahrgang)
                    .addComponent(jLUngueltigJahrgang)
                    .addComponent(jLHilfeJahrgang))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTName)
                    .addComponent(jLName)
                    .addComponent(jLAusfuellenName))
                .addGap(8, 8, 8)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRWeiss)
                    .addComponent(jLFarbe)
                    .addComponent(jRRot)
                    .addComponent(jRRose))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCAnbaugebiet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLAnbaugebiet)
                    .addComponent(jLAuswaehlenAnbaugebiet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLRegion)
                    .addComponent(jCRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLRebsorte)
                    .addComponent(jCRebsorte1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLComboboxDoppelt))
                .addGap(18, 18, 18)
                .addComponent(jCRebsorte2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCRebsorte3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLAlkoholgehalt)
                    .addComponent(jCAlkoholgehalt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLLagerfaehigkeit)
                    .addComponent(jTLagerfaehigkeit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigLagerfaehigkeit)
                    .addComponent(jLHilfeLagerfaehigkeit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCFlaschengroesse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLFlaschengroesse))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLFlaschenpreis)
                    .addComponent(jTFlaschenpreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigFlaschenpreis)
                    .addComponent(jLEuro))
                .addGap(18, 18, 18)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAbbrechen)
                    .addComponent(jBSpeichern))
                .addGap(291, 291, 291))
        );

        jLUngueltigBestellNr.getAccessibleContext().setAccessibleParent(jLUngueltigBestellNr);

        jTPWeinaufnahme.addTab("Aufnahmeformular", jPFormular);

        javax.swing.GroupLayout jPDiagrammLayout = new javax.swing.GroupLayout(jPDiagramm);
        jPDiagramm.setLayout(jPDiagrammLayout);
        jPDiagrammLayout.setHorizontalGroup(
            jPDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPDiagrammLayout.setVerticalGroup(
            jPDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jTPWeinaufnahme.addTab("tab2", jPDiagramm);

        javax.swing.GroupLayout jIFweinAufnehmenLayout = new javax.swing.GroupLayout(jIFweinAufnehmen.getContentPane());
        jIFweinAufnehmen.getContentPane().setLayout(jIFweinAufnehmenLayout);
        jIFweinAufnehmenLayout.setHorizontalGroup(
            jIFweinAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFweinAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTPWeinaufnahme, javax.swing.GroupLayout.DEFAULT_SIZE, 712, Short.MAX_VALUE)
                .addContainerGap())
        );
        jIFweinAufnehmenLayout.setVerticalGroup(
            jIFweinAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFweinAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTPWeinaufnahme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        desktopPane.add(jIFweinAufnehmen);
        jIFweinAufnehmen.setBounds(100, 10, 550, 600);

        dateiMenue.setMnemonic('d');
        dateiMenue.setText("Datei");

        beendenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        beendenMenuItem.setMnemonic('b');
        beendenMenuItem.setText("Beenden");
        beendenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beendenMenuItemActionPerformed(evt);
            }
        });
        dateiMenue.add(beendenMenuItem);

        menuBar.add(dateiMenue);

        bearbeitenMenue.setMnemonic('b');
        bearbeitenMenue.setText("Bearbeiten");
        menuBar.add(bearbeitenMenue);

        kundenMenue.setMnemonic('k');
        kundenMenue.setText("Kunden");
        kundenMenue.setToolTipText("");

        kundenAufnehmenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        kundenAufnehmenMenuItem.setMnemonic('a');
        kundenAufnehmenMenuItem.setText("Aufnehmen");
        kundenMenue.add(kundenAufnehmenMenuItem);

        kundenAendernMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        kundenAendernMenuItem.setMnemonic('\u00e4');
        kundenAendernMenuItem.setText("Ändern");
        kundenMenue.add(kundenAendernMenuItem);

        kundenLoeschenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        kundenLoeschenMenuItem.setMnemonic('l');
        kundenLoeschenMenuItem.setText("Löschen");
        kundenMenue.add(kundenLoeschenMenuItem);

        menuBar.add(kundenMenue);

        weinMenue.setMnemonic('w');
        weinMenue.setText("Wein");

        weinAufnehmenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK));
        weinAufnehmenMenuItem.setMnemonic('a');
        weinAufnehmenMenuItem.setText("Aufnehmen");
        weinAufnehmenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weinAufnehmenMenuItemActionPerformed(evt);
            }
        });
        weinMenue.add(weinAufnehmenMenuItem);

        weinAendernMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        weinAendernMenuItem.setMnemonic('\u00e4');
        weinAendernMenuItem.setText("Ändern");
        weinMenue.add(weinAendernMenuItem);

        weinLoeschenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK));
        weinLoeschenMenuItem.setMnemonic('l');
        weinLoeschenMenuItem.setText("Löschen");
        weinMenue.add(weinLoeschenMenuItem);

        menuBar.add(weinMenue);

        hilfeMenue.setMnemonic('i');
        hilfeMenue.setText("?");

        hilfeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        hilfeMenuItem.setMnemonic('i');
        hilfeMenuItem.setText("Info");
        hilfeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hilfeMenuItemActionPerformed(evt);
            }
        });
        hilfeMenue.add(hilfeMenuItem);

        menuBar.add(hilfeMenue);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void beendenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_beendenMenuItemActionPerformed
        beenden();
    }//GEN-LAST:event_beendenMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        beenden();
    }//GEN-LAST:event_formWindowClosing

    private void hilfeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hilfeMenuItemActionPerformed
        JOptionPane.showMessageDialog(null, INFO_MESSAGE, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_hilfeMenuItemActionPerformed

    private void weinAufnehmenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_weinAufnehmenMenuItemActionPerformed
        jIFweinAufnehmen.setVisible(true);
    }//GEN-LAST:event_weinAufnehmenMenuItemActionPerformed

    private void jTBestellnummerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTBestellnummerFocusGained
        jTBestellnummer.selectAll();
    }//GEN-LAST:event_jTBestellnummerFocusGained

    private void jTBestellnummerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTBestellnummerFocusLost
        jTBestellnummer.select(0, 0);
    }//GEN-LAST:event_jTBestellnummerFocusLost

    private void jBAbbrechenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAbbrechenActionPerformed
        schliesseInternalFrame();
    }//GEN-LAST:event_jBAbbrechenActionPerformed

    private void jBSpeichernActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSpeichernActionPerformed
        boolean fehlerGefunden = false;
        String farbe;
        String[] rebsorten = new String[3];
        resetVisibility();
        
        if (!bestellNrVerifier.verify(jTBestellnummer)) {
            jLUngueltigBestellNr.setVisible(true);
            jLHilfeBestellNr.setVisible(true);
            if (!fehlerGefunden) {
                jTBestellnummer.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!jahrVerifier.verify(jTJahrgang)) {
            jLUngueltigJahrgang.setVisible(true);
            jLHilfeJahrgang.setVisible(true);
            if (!fehlerGefunden) {
                jTJahrgang.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jTName.getText().equals("")) {
            jLAusfuellenName.setVisible(true);
            if (!fehlerGefunden) {
                jTName.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jCAnbaugebiet.getSelectedIndex() == 0) {
            jLAuswaehlenAnbaugebiet.setVisible(true);
            if (!fehlerGefunden) {
                jCAnbaugebiet.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!lagerdauerVerifier.verify(jTLagerfaehigkeit)) {
            jLUngueltigLagerfaehigkeit.setVisible(true);
            jLHilfeLagerfaehigkeit.setVisible(true);
            if (!fehlerGefunden) {
                jTLagerfaehigkeit.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jTFlaschenpreis.getText().toString().isEmpty()) {
            jLUngueltigFlaschenpreis.setVisible(true);
            if (!fehlerGefunden) {
                jLFlaschenpreis.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (preisUngueltig) {
            jLUngueltigFlaschenpreis.setVisible(true);
        }
        if (comboboxDoppelt()) {
            jLComboboxDoppelt.setVisible(true);
        }
        if (fehlerGefunden) {
            JOptionPane.showMessageDialog(null, ERROR_MESSAGE, ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        } else {

            if (jRWeiss.isSelected()) {
                farbe = "weiss";
            } else if (jRRot.isSelected()) {
                farbe = "rot";
            } else {
                farbe = "rose";
            }
            if (jCRebsorte1.getSelectedIndex() != 0) {
                rebsorten[0] = jCRebsorte1.getSelectedItem().toString();
            }
            if (jCRebsorte2.getSelectedIndex() != 0) {
                rebsorten[1] = jCRebsorte2.getSelectedItem().toString();
            }
            if (jCRebsorte3.getSelectedIndex() != 0) {
                rebsorten[2] = jCRebsorte3.getSelectedItem().toString();
            }
            NumberFormat nf = NumberFormat.getInstance();
            double preis = 0;
            try {
                preis = nf.parse(jTFlaschenpreis.getText()).doubleValue();
            } catch (ParseException e) {
                
            }
            Wein neuerWein = new Wein(jTBestellnummer.getText(), Integer.parseInt(jTJahrgang.getText()),
                    jTName.getText(), farbe, rebsorten, jCAnbaugebiet.getSelectedItem().toString(),
                    jCAlkoholgehalt.getSelectedItem().toString(), 
                    Integer.parseInt(jTLagerfaehigkeit.getText()),
                    jCFlaschengroesse.getSelectedItem().toString(),
                    preis);
            System.out.println(neuerWein.toString());
            resetInternalFrame();
        }
    }//GEN-LAST:event_jBSpeichernActionPerformed

    private void jCAnbaugebietItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCAnbaugebietItemStateChanged
        if (!firstRun) {
            String anbaugebiet = jCAnbaugebiet.getSelectedItem().toString();
//            fuelleJCRebsorte1(anbaugebiet);
//            fuelleJCRebsorte2(anbaugebiet);
//            fuelleJCRebsorte3(anbaugebiet);
            fuelleJCRegion(anbaugebiet);
        }
    }//GEN-LAST:event_jCAnbaugebietItemStateChanged

    private void jLHilfeBestellNrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeBestellNrMouseClicked
        JOptionPane.showMessageDialog(null, BESTELLNR_MESSAGE, BESTELLNR_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeBestellNrMouseClicked

    private void jLHilfeJahrgangMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeJahrgangMousePressed
        JOptionPane.showMessageDialog(null, JAHR_MESSAGE, JAHR_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeJahrgangMousePressed

    private void jTJahrgangFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTJahrgangFocusGained
        jTJahrgang.selectAll();
    }//GEN-LAST:event_jTJahrgangFocusGained

    private void jTJahrgangFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTJahrgangFocusLost
        jTJahrgang.select(0, 0);
    }//GEN-LAST:event_jTJahrgangFocusLost

    private void jTNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTNameFocusGained
        jTName.selectAll();
    }//GEN-LAST:event_jTNameFocusGained

    private void jTNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTNameFocusLost
        jTName.select(0, 0);
    }//GEN-LAST:event_jTNameFocusLost

    private void jLHilfeLagerfaehigkeitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeLagerfaehigkeitMouseClicked
        JOptionPane.showMessageDialog(null, LAGERDAUER_MESSAGE, LAGERDAUER_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeLagerfaehigkeitMouseClicked

    private void jTLagerfaehigkeitFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTLagerfaehigkeitFocusGained
        jTLagerfaehigkeit.selectAll();
    }//GEN-LAST:event_jTLagerfaehigkeitFocusGained

    private void jTFlaschenpreisFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFlaschenpreisFocusLost
        formatierePreis();
        jTFlaschenpreis.select(0, 0);
    }//GEN-LAST:event_jTFlaschenpreisFocusLost

    private void jTFlaschenpreisFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFlaschenpreisFocusGained
        jTFlaschenpreis.selectAll();
    }//GEN-LAST:event_jTFlaschenpreisFocusGained

    private void jIFweinAufnehmenInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_jIFweinAufnehmenInternalFrameClosing
        schliesseInternalFrame();
    }//GEN-LAST:event_jIFweinAufnehmenInternalFrameClosing

    private void jCAnbaugebietActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCAnbaugebietActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCAnbaugebietActionPerformed

    private void jCAlkoholgehaltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCAlkoholgehaltActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCAlkoholgehaltActionPerformed

    private void jCFlaschengroesseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCFlaschengroesseActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCFlaschengroesseActionPerformed

    private void jCRebsorte1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRebsorte1ActionPerformed
        if (jCRebsorte1.getSelectedIndex() != 0 && jCRebsorte1.isEnabled()) {
            jCRebsorte2.setEnabled(true);
        } else {
            jCRebsorte2.setEnabled(false);
            jCRebsorte3.setEnabled(false);
            jCRebsorte2.setSelectedIndex(0);
            jCRebsorte3.setSelectedIndex(0);
        }
    }//GEN-LAST:event_jCRebsorte1ActionPerformed

    private void jCRebsorte2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRebsorte2ActionPerformed
        if (jCRebsorte2.getSelectedIndex() != 0 && jCRebsorte1.isEnabled()) {
            jCRebsorte3.setEnabled(true);
        } else {
            jCRebsorte3.setEnabled(false);
            jCRebsorte3.setSelectedIndex(0);
        }
    }//GEN-LAST:event_jCRebsorte2ActionPerformed

    /**
     * Prüft ob die ausgewählten Einträge der Comboboxen doppelt sind.
     *
     * @return boolean
     */
    private boolean comboboxDoppelt() {

        return jCRebsorte2.getSelectedIndex() != 0 && jCRebsorte1.getSelectedIndex() == jCRebsorte2.getSelectedIndex()
                || jCRebsorte3.getSelectedIndex() != 0 && jCRebsorte1.getSelectedIndex() == jCRebsorte3.getSelectedIndex()
                || jCRebsorte3.getSelectedIndex() != 0 && jCRebsorte2.getSelectedIndex() == jCRebsorte3.getSelectedIndex();
    }

    /**
     * Setzt die Fokusreihenfolge fest.
     *
     * @return ArrayList<JComponent> - Fokusreihenfolge
     */
    private ArrayList<JComponent> getOrder() {
        ArrayList<JComponent> order = new ArrayList<>();
        order.add(jTBestellnummer);
//        order.add(jLHilfeBestellNr);
        order.add(jTJahrgang);
//        order.add(jLHilfeJahrgang);
        order.add(jTName);
        order.add(jRWeiss);
        order.add(jRRot);
        order.add(jRRose);
        order.add(jCAnbaugebiet);
        order.add(jCRebsorte1);
        order.add(jCRebsorte2);
        order.add(jCRebsorte3);
        order.add(jCAlkoholgehalt);
        order.add(jTLagerfaehigkeit);
//        order.add(jLHilfeLagerfaehigkeit);
        order.add(jCFlaschengroesse);
        order.add(jTFlaschenpreis);
        order.add(jBSpeichern);
        order.add(jBAbbrechen);
        return order;
    }

    /**
     * InternalFrame wird resettet und unsichtbar gemacht.
     */
    private void schliesseInternalFrame() {
        if (formularVeraendert()) {
            int abbrechen = JOptionPane.showConfirmDialog(null, ABORT_MESSAGE,
                    ABORT_TITLE, JOptionPane.YES_NO_OPTION);
            if (abbrechen == 0) {
                jIFweinAufnehmen.setVisible(false);
                resetInternalFrame();
                resetVisibility();
            }
        } else {
            jIFweinAufnehmen.setVisible(false);
            resetInternalFrame();
            resetVisibility();
        }
    }

    /**
     * Prüft ob das Formular bearbeitet wurde.
     *
     * @return boolean
     */
    private boolean formularVeraendert() {
        boolean veraendert = false;

        for (JComponent c : PFLICHTFELDER) {
            if (!veraendert && c instanceof JTextField) {
                veraendert = !((JTextField) c).getText().isEmpty();
            } else if (!veraendert && c instanceof JComboBox) {
                veraendert = comboBoxChanged;
            } else if (!veraendert && c instanceof JRadioButton) {
                veraendert = !((JRadioButton) c).isSelected();
            }
        }

        return veraendert;
    }

    /**
     * Resettet das InternalFrame.
     */
    private void resetInternalFrame() {
        jTBestellnummer.setText("");
        jTJahrgang.setText("");
        jTName.setText("");
        jRWeiss.setSelected(true);
        jCAnbaugebiet.setSelectedIndex(0);
//        jCRebsorte1.removeAllItems();
//        jCRebsorte1.addItem("Anbaugebiet Auswählen");
//        jCRebsorte2.removeAllItems();
//        jCRebsorte2.addItem("Anbaugebiet Auswählen");
//        jCRebsorte3.removeAllItems();
//        jCRebsorte3.addItem("Anbaugebiet Auswählen");
        jCRebsorte1.setSelectedIndex(0);
        jCRebsorte2.setSelectedIndex(0);
        jCRebsorte3.setSelectedIndex(0);
        jCAlkoholgehalt.setSelectedIndex(6);
        jTLagerfaehigkeit.setText("");
        jCFlaschengroesse.setSelectedIndex(5);
        jTFlaschenpreis.setText("");
        comboBoxChanged = false;
    }

    /**
     * Formatiert das Textfield in deutscher Euro schreibweise.
     */
    private void formatierePreis() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        preisUngueltig = false;
        try {
            jTFlaschenpreis.setText(nf.format(nf.parse(jTFlaschenpreis.getText()).doubleValue()));
        } catch (ParseException e) {
//            jLUngueltigFlaschenpreis.setVisible(true);
            preisUngueltig = true;
        }
    }

    /**
     * Macht alle Fehlerhinweise unsichtbar.
     */
    private void resetVisibility() {
        jLUngueltigBestellNr.setVisible(false);
        jLUngueltigJahrgang.setVisible(false);
        jLHilfeBestellNr.setVisible(false);
        jLHilfeJahrgang.setVisible(false);
        jLAusfuellenName.setVisible(false);
        jLAuswaehlenAnbaugebiet.setVisible(false);
        jLHilfeLagerfaehigkeit.setVisible(false);
        jLUngueltigLagerfaehigkeit.setVisible(false);
        jLUngueltigFlaschenpreis.setVisible(false);
        jLComboboxDoppelt.setVisible(false);
    }

    /**
     * Fragt nach ob das Programm wirklich beendet werden sollen und beendet ggf
     * das Programm.
     */
    private void beenden() {
        int beenden = JOptionPane.showConfirmDialog(null, CLOSE_MASSAGE, CLOSE_TITLE, JOptionPane.YES_NO_OPTION);
        if (beenden == 0) {
            System.exit(0);
        }
    }

    /**
     * Füllt eine Combobox mit Ländern.
     *
     * @param laender Strin[]
     */
    private void fuelleLaender(String[] laender) {
        jCAnbaugebiet.addItem("Bitte Land auswählen");
        for (int i = 0; i < laender.length; i++) {
            jCAnbaugebiet.addItem(laender[i]);
        }
    }

    /**
     * Füllt ein Array mit Ländern.
     */
    private void fuelleRebLaender() {
        boolean hinzugefuegt = true;
        hinzugefuegt = REB_LAENDER.add(ARGENTINIENREG);
        hinzugefuegt = REB_LAENDER.add(AUSTRALIENREG);
        hinzugefuegt = REB_LAENDER.add(CHILEREG);
        hinzugefuegt = REB_LAENDER.add(DEUTSCHLANDREG);
        hinzugefuegt = REB_LAENDER.add(FRANKREICHREG);
        hinzugefuegt = REB_LAENDER.add(GRIECHENLANDREG);
        hinzugefuegt = REB_LAENDER.add(ITALIENREG);
        hinzugefuegt = REB_LAENDER.add(KROATIENREG);
        hinzugefuegt = REB_LAENDER.add(MAZEDONIENREG);
        hinzugefuegt = REB_LAENDER.add(MEXIKOREG);
        hinzugefuegt = REB_LAENDER.add(NEUSEELANDREG);
        hinzugefuegt = REB_LAENDER.add(OESTERREICH);
        hinzugefuegt = REB_LAENDER.add(PORTUGALREG);
        hinzugefuegt = REB_LAENDER.add(RUMÄNIENREG);
        hinzugefuegt = REB_LAENDER.add(SCHWEIZREG);
        hinzugefuegt = REB_LAENDER.add(SPANIENREG);
        hinzugefuegt = REB_LAENDER.add(SUEDAFRIEKAREG);
        hinzugefuegt = REB_LAENDER.add(TSCHECHIENREG);
        hinzugefuegt = REB_LAENDER.add(UNGARNREG);
        hinzugefuegt = REB_LAENDER.add(USAREG);
    }
    
    private void fuelleJCRegion(String land) {
        String[] rebsorten = LAND_UND_REGION.get(land);
        jCRegion.removeAllItems();
        jCRegion.addItem("Bitte auswählen");
        if (rebsorten != null) {
            for (int i = 0; i < rebsorten.length; i++) {
                jCRegion.addItem(rebsorten[i]);
            }
        }
    }

    /**
     * Füllt eine Combobox mit Resborten.
     *
     * @param land
     */
    private void fuelleJCRebsorten(String[] rebsorten) {
        jCRebsorte1.removeAllItems();
//        jCRebsorte1.addItem("Bitte auswählen");
        if (rebsorten != null) {
            for (int i = 0; i < rebsorten.length; i++) {
                jCRebsorte1.addItem(rebsorten[i]);
                jCRebsorte2.addItem(rebsorten[i]);
                jCRebsorte3.addItem(rebsorten[i]);
            }
        }
    }


    /**
     * Legt in einer HashMap die Beziehung zwischen Rebsorten und Ländern fest.
     *
     * @param beziehung
     */
    private void fuelleLandUndRegionBeziehung(ArrayList<String[]> beziehung) {
        for (int i = 0; i < beziehung.size(); i++) {
            LAND_UND_REGION.put(LAENDER[i], beziehung.get(i));
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Weinverwaltung().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu bearbeitenMenue;
    private javax.swing.JMenuItem beendenMenuItem;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenu dateiMenue;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenuItem hilfeMenuItem;
    private javax.swing.JMenu hilfeMenue;
    private javax.swing.JButton jBAbbrechen;
    private javax.swing.JButton jBSpeichern;
    private javax.swing.JComboBox jCAlkoholgehalt;
    private javax.swing.JComboBox jCAnbaugebiet;
    private javax.swing.JComboBox jCFlaschengroesse;
    private javax.swing.JComboBox jCRebsorte1;
    private javax.swing.JComboBox jCRebsorte2;
    private javax.swing.JComboBox jCRebsorte3;
    private javax.swing.JComboBox jCRegion;
    private javax.swing.JInternalFrame jIFweinAufnehmen;
    private javax.swing.JLabel jLAlkoholgehalt;
    private javax.swing.JLabel jLAnbaugebiet;
    private javax.swing.JLabel jLAusfuellenName;
    private javax.swing.JLabel jLAuswaehlenAnbaugebiet;
    private javax.swing.JLabel jLBestellnummer;
    private javax.swing.JLabel jLComboboxDoppelt;
    private javax.swing.JLabel jLEuro;
    private javax.swing.JLabel jLFarbe;
    private javax.swing.JLabel jLFlaschengroesse;
    private javax.swing.JLabel jLFlaschenpreis;
    private javax.swing.JLabel jLHilfeBestellNr;
    private javax.swing.JLabel jLHilfeJahrgang;
    private javax.swing.JLabel jLHilfeLagerfaehigkeit;
    private javax.swing.JLabel jLJahrgang;
    private javax.swing.JLabel jLLagerfaehigkeit;
    private javax.swing.JLabel jLName;
    private javax.swing.JLabel jLRebsorte;
    private javax.swing.JLabel jLRegion;
    private javax.swing.JLabel jLUngueltigBestellNr;
    private javax.swing.JLabel jLUngueltigFlaschenpreis;
    private javax.swing.JLabel jLUngueltigJahrgang;
    private javax.swing.JLabel jLUngueltigLagerfaehigkeit;
    private javax.swing.JPanel jPDiagramm;
    private javax.swing.JPanel jPFormular;
    private javax.swing.JRadioButton jRRose;
    private javax.swing.JRadioButton jRRot;
    private javax.swing.JRadioButton jRWeiss;
    private javax.swing.JTextField jTBestellnummer;
    private javax.swing.JTextField jTFlaschenpreis;
    private javax.swing.JTextField jTJahrgang;
    private javax.swing.JTextField jTLagerfaehigkeit;
    private javax.swing.JTextField jTName;
    private javax.swing.JTabbedPane jTPWeinaufnahme;
    private javax.swing.JMenuItem kundenAendernMenuItem;
    private javax.swing.JMenuItem kundenAufnehmenMenuItem;
    private javax.swing.JMenuItem kundenLoeschenMenuItem;
    private javax.swing.JMenu kundenMenue;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem weinAendernMenuItem;
    private javax.swing.JMenuItem weinAufnehmenMenuItem;
    private javax.swing.JMenuItem weinLoeschenMenuItem;
    private javax.swing.JMenu weinMenue;
    // End of variables declaration//GEN-END:variables

}
