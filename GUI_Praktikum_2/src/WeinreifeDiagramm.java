
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.GregorianCalendar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Diese Klasse erzeugt ein Diagramm.
 *
 * @author dennis
 */
public class WeinreifeDiagramm extends JPanel {

    /**
     * Die Lagerdauer des Weins.
     */
    int lagerdauer;
    /**
     * Der Jahrgang des Weins.
     */
    int jahrgang;
    /**
     * Konstanten für die Farben.
     */
    private Color farbeUnreif = Color.GRAY;
    private Color[] farbeSteigerung = {Color.GRAY, Color.GREEN};
    private Color farbeOptimal = Color.GREEN;
    private Color farbeUeberlagert = Color.YELLOW;
    private Color farbeSchrift = Color.BLACK;
    private Color farbeUmrandung = Color.BLACK;

    private final int aktuellesJahr;

    private float groesseEinesJahres;
    
    /**
     * Variablen für die X-Koordinaten.
     */
    private float xKoordinateUnreif;
    private float xKoordinateSteigerung;
    private float xKoordinateOptimal;
    private float xKoordinateUeberlagert;
    private float xKoordinateAktuellesJahr;

    private float xProzent;
    private float yProzent;

    /**
     * Variablen der gezeichnete Flächen.
     */
    private Rectangle2D flaecheUnreif;
    private Rectangle2D flaecheSteigerung;
    private Rectangle2D flaecheOptimal;
    private Rectangle2D flaecheUeberlagert;
    
    /**
     * Wird der Wert gespeichert ob die Legende sichtbar ist oder nicht.
     */
    private boolean mitLegende = true;

    /**
     * Ein neuer Datentyp worin die Bereiche wo der Mauszeiger sein kann
     * aufgelistet sind.
     */
    private enum Bereiche {

        UNREIF, STEIGERUNG, OPTIMAL, UEBERLAGERT, AUSSERHALB;
    }
    
    /**
     * Merkt sich wo der Mauszeiger ist.
     */
    private Bereiche bereich = Bereiche.AUSSERHALB;

    /**
     * Fügt ein MouseMotionListener und MouseListener hinzu, zudem wird das
     * aktuelle Jahr ermittelt.
     */
    public WeinreifeDiagramm() {
        super();
        this.addMouseMotionListener(new MouseOver());
        this.addMouseListener(new Mausklick());
        aktuellesJahr
                = new GregorianCalendar().get(GregorianCalendar.YEAR);
    }

    /**
     * Zeichnet das Diagramm.
     *
     * @param g Grafikkontext
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Dimension d = this.getSize();
        this.koordinatenBerechnen();

        /*
         * Unreif Bereich zeichnen.
         */
        g2d.setPaint(farbeUnreif);
        g2d.fill(flaecheUnreif);

        /*
         * Steigerungsbereich zeichnen.
         */
        g2d.setPaint(new GradientPaint(xKoordinateSteigerung * xProzent,
                0f,
                farbeSteigerung[0],
                xKoordinateOptimal * xProzent,
                0f,
                farbeSteigerung[1]));
        g2d.fill(flaecheSteigerung);

        /*
         * Optimalen Bereich zeichnen.
         */
        g2d.setPaint(farbeOptimal);

        g2d.fill(flaecheOptimal);

        /*
         * Ueberlagerten Bereich zeichnen.
         */
        g2d.setPaint(farbeUeberlagert);

        g2d.fill(flaecheUeberlagert);

        /*
         * Rahmen zeichnen.
         */
        g2d.setPaint(farbeUmrandung);

        g2d.draw(
                new Rectangle2D.Double(10 * xProzent,
                        10 * yProzent,
                        80 * xProzent,
                        20 * yProzent));

        /*
         * Trenner einzeichnen.
         */
        g2d.setPaint(farbeUmrandung);

        g2d.draw(
                new Line2D.Double(
                        xKoordinateSteigerung * xProzent,
                        10 * yProzent,
                        xKoordinateSteigerung * xProzent,
                        30 * yProzent));
        g2d.draw(
                new Line2D.Double(
                        xKoordinateOptimal * xProzent,
                        10 * yProzent,
                        xKoordinateOptimal * xProzent,
                        30 * yProzent));
        g2d.draw(
                new Line2D.Double(
                        xKoordinateUeberlagert * xProzent,
                        10 * yProzent,
                        xKoordinateUeberlagert * xProzent,
                        30 * yProzent));

        /*
         * Beschriftungen einzeichnen.
         */
        g2d.setPaint(Color.BLACK);

        // Unreif
        g2d.drawString(
                "" + this.jahrgang,
                xKoordinateUnreif * xProzent,
                35 * yProzent);

        // Steigert sich noch
        g2d.drawString(
                "" + (this.jahrgang + Math.round(this.lagerdauer / 8.0)),
                xKoordinateSteigerung * xProzent,
                35 * yProzent);

        // Optimal
        /*
         * Wenn Lagerdauer 2 beträgt muss die Lagerdauer für die Anzeige 
         * umgerechnet werden.
         */
        int lagerdauerTemp;
        if (lagerdauer == 2 || lagerdauer
                == 4) {
            lagerdauerTemp = lagerdauer + 1;
        } else {
            lagerdauerTemp = lagerdauer;
        }

        g2d.drawString(
                "" + (this.jahrgang + Math.round(lagerdauerTemp / 2.0) - 1),
                xKoordinateOptimal * xProzent,
                35 * yProzent);

        // Ueberlagert
        g2d.drawString(
                "" + (this.jahrgang + this.lagerdauer),
                xKoordinateUeberlagert * xProzent,
                35 * yProzent);

        /*
         * Aktuelles Jahr einzeichnen.
         */
        if (aktuellesJahr >= jahrgang
                && aktuellesJahr <= (jahrgang + lagerdauer)) {
            g2d.setColor(Color.RED);
            g2d.draw(new Rectangle2D.Double(xKoordinateAktuellesJahr * xProzent,
                    9.9 * yProzent,
                    groesseEinesJahres * xProzent,
                    20.3 * yProzent));
            // aktuelles Jahr schreiben
            g2d.setPaint(Color.RED);
            g2d.drawString(
                    "" + aktuellesJahr,
                    xKoordinateAktuellesJahr * xProzent,
                    35 * yProzent);
        }
        if (mitLegende) {
            /*
             * Legende Bezeichnungen
             */
            g2d.setPaint(farbeSchrift);

            g2d.drawString(
                    "Legende", 10 * xProzent, 47 * yProzent);
            g2d.drawString(
                    "Zu früh", 15 * xProzent, 57 * yProzent);
            g2d.drawString(
                    "Steigert sich noch", 15 * xProzent, 67 * yProzent);
            g2d.drawString(
                    "Optimaler Trinkzeitpunkt",
                    15 * xProzent, 77 * yProzent);
            g2d.drawString(
                    "Überlagert", 15 * xProzent, 87 * yProzent);

            /*
             * Legende zeichnen
             */
            g2d.scale(d.getWidth() / 100, d.getHeight() / 100);

            g2d.setColor(farbeUnreif);

            g2d.fill(
                    new Rectangle2D.Double(10, 52, 3, 5));

            g2d.setPaint(
                    new GradientPaint(
                            10f, 0f, farbeSteigerung[0], 13f, 0f, 
                            farbeSteigerung[1]));
            g2d.fill(
                    new Rectangle2D.Double(10, 62, 3, 5));

            g2d.setColor(farbeOptimal);

            g2d.fill(
                    new Rectangle2D.Double(10, 72, 3, 5));

            g2d.setColor(farbeUeberlagert);

            g2d.fill(
                    new Rectangle2D.Double(10, 82, 3, 5));

            /*
             * Schwarze umrandungen
             */
            //Strichstärke setzen
            g2d.setStroke(
                    new BasicStroke(0f));
            g2d.setColor(farbeUmrandung);

            g2d.draw(
                    new Rectangle2D.Double(9.9, 51.9, 3, 5));
            g2d.draw(
                    new Rectangle2D.Double(9.9, 61.9, 3, 5));
            g2d.draw(
                    new Rectangle2D.Double(9.9, 71.9, 3, 5));
            g2d.draw(
                    new Rectangle2D.Double(9.9, 81.9, 3, 5));
        }
    }

    
    /**
     * Berechnet alle benötigten X-Koordinaten und berechnet die Flächen.
     */
    private void koordinatenBerechnen() {
        Dimension d = this.getSize();
        groesseEinesJahres = 80f / (this.lagerdauer + 1);
        xKoordinateUnreif = 10;
        xKoordinateSteigerung
                = 10 + groesseEinesJahres * Math.round(lagerdauer / 8.0f);
        xKoordinateOptimal
                = 10 + groesseEinesJahres
                * (Math.round(lagerdauer / 2.0f) - lagerdauer % 2);
        xKoordinateUeberlagert
                = 10 + groesseEinesJahres * this.lagerdauer;

        xProzent = (float) (d.getWidth() / 100);
        yProzent = (float) (d.getHeight() / 100);

        xKoordinateAktuellesJahr
                = 10 + (aktuellesJahr - jahrgang) * groesseEinesJahres;

        flaecheUnreif = new Rectangle2D.Double(
                xKoordinateUnreif * xProzent,
                10 * yProzent,
                (xKoordinateSteigerung - xKoordinateUnreif) * xProzent,
                20 * yProzent);

        flaecheSteigerung = new Rectangle2D.Double(
                (xKoordinateSteigerung * xProzent) + 1,
                (10 * yProzent) + 1,
                (xKoordinateOptimal - xKoordinateSteigerung) * xProzent,
                20 * yProzent);

        flaecheOptimal = new Rectangle2D.Double(
                xKoordinateOptimal * xProzent,
                10 * yProzent,
                (xKoordinateUeberlagert - xKoordinateOptimal) * xProzent,
                20 * yProzent);

        flaecheUeberlagert = new Rectangle2D.Double(
                xKoordinateUeberlagert * xProzent,
                10 * yProzent,
                groesseEinesJahres * xProzent,
                20 * yProzent);
    }

    /**
     * Diese Methode prüft und setzt den Jahrgang des Weins.
     *
     * @param jahrgang <= aktuelles Jahr && > 1899
     * @throws IllegalArgumentException wenn der Wert nicht gültig ist
     */
    public void setzeJahrgang(int jahrgang) throws IllegalArgumentException {

        if (jahrgang <= aktuellesJahr && jahrgang > 1899) {
            this.jahrgang = jahrgang;
        } else {
            throw new IllegalArgumentException("Jahrgang hat einen"
                    + "ungültigen Wert!");
        }
    }

    /**
     * Die Methode prüft und setzt die Lagerdauer des Weins.
     *
     * @param lagerdauer min. 1 Jahr max 25 Jahre
     * @throws IllegalArgumentException wenn der Wert nicht gültig ist
     */
    public void setzeLagerdauer(int lagerdauer)
            throws IllegalArgumentException {
        if (lagerdauer >= 1 && lagerdauer <= 25) {
            this.lagerdauer = lagerdauer;
        } else {
            throw new IllegalArgumentException("Lagerdauer hat einen"
                    + "ungültigen Wertebereich!");
        }
    }

    /**
     * Sorgt für die MouseOver Effekte.
     */
    class MouseOver extends MouseMotionAdapter {
        
        /**
         * Färbt die verschiedenen bereiche im Balkendiagramm ein.
         * 
         * @param e MouseEvent 
         */
        @Override
        public void mouseMoved(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();

            if ((y < 10 * yProzent
                    || x < 10 * xProzent
                    || x > 90 * xProzent
                    || y > 30 * yProzent)
                    && bereich != Bereiche.AUSSERHALB) {
                bereich = Bereiche.AUSSERHALB;
                farbeUnreif = Color.GRAY;
                farbeSteigerung = new Color[]{Color.GRAY, Color.GREEN};
                farbeOptimal = Color.GREEN;
                farbeUeberlagert = Color.YELLOW;
                repaint();
            } else if (flaecheUnreif.contains(x, y)
                    && bereich != bereich.UNREIF) {
                bereich = Bereiche.UNREIF;
                farbeUnreif = Color.GRAY.darker();
                farbeSteigerung = new Color[]{Color.GRAY, Color.GREEN};
                farbeOptimal = Color.GREEN;
                farbeUeberlagert = Color.YELLOW;
                repaint();
            } else if (flaecheSteigerung.contains(x, y)
                    && bereich != bereich.STEIGERUNG) {
                bereich = Bereiche.STEIGERUNG;
                farbeUnreif = Color.GRAY;
                farbeSteigerung = new Color[]{Color.GRAY.darker(), 
                    Color.GREEN.darker()};
                farbeOptimal = Color.GREEN;
                farbeUeberlagert = Color.YELLOW;
                repaint();
            } else if (flaecheOptimal.contains(x, y)
                    && bereich != bereich.OPTIMAL) {
                bereich = Bereiche.OPTIMAL;
                farbeUnreif = Color.GRAY;
                farbeSteigerung = new Color[]{Color.GRAY, Color.GREEN};
                farbeOptimal = Color.GREEN.darker();
                farbeUeberlagert = Color.YELLOW;
                repaint();
            } else if (flaecheUeberlagert.contains(x, y)
                    && bereich != bereich.UEBERLAGERT) {
                bereich = Bereiche.UEBERLAGERT;
                farbeUnreif = Color.GRAY;
                farbeSteigerung = new Color[]{Color.GRAY, Color.GREEN};
                farbeOptimal = Color.GREEN;
                farbeUeberlagert = Color.YELLOW.darker();
                repaint();
            }
        }
    }

    /**
     * Zuständlich für den Mausklick.
     */
    class Mausklick extends MouseAdapter {

        /**
         * Erzeugt eine Konsolenausgabe mit den Jahresinformationswerten und
         * blendet mit gedrückter Shift-Taste die Legende ein oder aus.
         * 
         * Shift + Linke-Maustaste = Legene ausgeblendet
         * Shift + Rechte-Maustaste = Legende eingeblendet
         * 
         * @param e MouseEvent
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();

            if (e.isShiftDown() && SwingUtilities.isLeftMouseButton(e)) {
                mitLegende = false;
                repaint();
            } else if (e.isShiftDown() 
                    && SwingUtilities.isRightMouseButton(e)) {
                mitLegende = true;
                repaint();
            } else if (flaecheUnreif.contains(x, y)) {
                int jahre = Math.round(lagerdauer / 8f);

                if (jahre > 1) {
                    System.out.println("In den Jahren " + jahrgang + " – "
                            + (jahrgang + (int) (lagerdauer / 8.0f))
                            + " ist der Wein unreif.");
                } else {
                    System.out.println("Im Jahr " + jahrgang
                            + " ist der Wein zu früh zum trinken.");
                }
            } else if (flaecheSteigerung.contains(x, y)) {
                int jahre = Math.round(lagerdauer / 2f) 
                        - Math.round(lagerdauer / 8f) - lagerdauer % 2;

                if (jahre > 1) {
                    System.out.println("In den Jahren " 
                            + (jahrgang + Math.round(lagerdauer / 8.0f)) + " – "
                            + (jahrgang + (lagerdauer / 2) - 1)
                            + " ist der Wein steigerungsfähig.");
                } else {
                    System.out.println("Der Wein ist im Jahr "
                            + (jahrgang + Math.round(lagerdauer / 8.0f))
                            + " noch steigerungsfähig.");
                }

            } else if (flaecheOptimal.contains(x, y)) {
                int jahre = Math.round(lagerdauer / 2f);
                if (jahre > 1) {
                    System.out.println("In den Jahren " 
                            + (jahrgang + (lagerdauer / 2)) + " – "
                            + (jahrgang + lagerdauer - 1)
                            + " hat der Wein sein geschmackliches Optimum.");
                } else {
                    System.out.println("Der Wein hat im Jahr "
                            + (jahrgang + (lagerdauer / 2))
                            + " sein geschmackliches Optimum.");
                }
            } else if (flaecheUeberlagert.contains(x, y)) {
                System.out.println("Ab dem Jahr " + (jahrgang + lagerdauer)
                        + " is der Wein überlagert.");
            }
        }
    }
}
