
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.swing.InputVerifier;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dennis
 */
public class Weinverwaltung extends javax.swing.JFrame {

    /**
     * Konstanten für den MessageDialog
     */
    private final String TEXT_TITLE = "Keine Zahl";
    private final String TEXT_MESSAGE = "Bitte geben Sie eine Zahl im deutschen Format \nmit Tausenderpunkten (z.B. 2.345,66) ein.";

    private final String CLOSE_MASSAGE = "Möchten Sie das Programm wirklich beenden?";

    private final String CLOSE_TITLE = "Programm Beenden?";

    private final String ABORT_MESSAGE = "Möchten Sie die Weinaufnahme wirklich "
            + "abbrechen?";

    private final String ABORT_TITLE = "Weinaufnahme Beenden?";

    private final String KUNDE_ABORT_MESSAGE = "Möchten Sie die Kundenaufnahme wirklich "
            + "abbrechen?";

    private final String KUNDE_ABORT_TITLE = "Kundenaufnahme Beenden?";

    private final String INFO_MESSAGE = "The cake ist a lie! \n Version 0.01";

    private final String INFO_TITLE = "The cake is a lie!";

    private final String LAGERDAUER_MESSAGE = "Die Lagerdauer muss über den aktuellen Jahr liegen \n"
            + "und darf nicht länger als 25 Jahre haltbar sein.";

    private final String LAGERDAUER_TITLE = "Ungültige Lagerdauer";

    private final String BESTELLNR_MESSAGE = "Bitte geben Sie eine gültige"
            + "Bestellnummer ein.\n"
            + "Das Format der Bestellnummer muss wie folgt aussehen:"
            + "\nAA99 oder aa99 (2 Buchstaben 2 Zahlen)";
    private final String BESTELLNR_TITLE = "Ungültige Bestellnummer";

    private final String JAHR_MESSAGE = "Bitte geben Sie ein gültigen Jahrgang ein!"
            + "\nDer Jahrgang darf 25 vor Jahre bis 25 nach dem aktuellen Jahr liegen.";

    private final String JAHR_TITLE = "Ungültiges Jahr!";

    private final String KUNDENNR_MESSAGE = "Die Kundennummer muss eine 5-"
            + "stellige Zahl sein!";

    private final String KUNDENNR_TITLE = "Ungültige Kundennummer!";

    private final String NAME_MESSAGE = "Der Name darf nur aus Buchstaben, ',"
            + " - und Leerzeichen bestehen.";

    private final String NAME_TITLE = "Ungültiger Name!";

    private final String PLZ_MESSAGE = "Die Postleitzahl muss eine 5-"
            + "stellige Zahl sein!";

    private final String PLZ_TITLE = "Ungültige Postleitzahl!";

    private final String ERROR_MESSAGE = "Es wurde(n) ein oder mehrere "
            + "ungültige(r) Wert(e) gefunden!";

    private final String ERROR_TITLE = "Ungültige(r) Wert(e) gefunden!";

    private final String TRENNER = ";";

    private final Color LEER = new Color(255, 138, 128);

    private final BestellnummerVerifier bestellNrVerifier;

    private final JahrgangVerifier jahrVerifier;

    private final LagerdauerVerifier lagerdauerVerifier;

    private final NamenVerifier namenVerifier;

    private final KundennummerVerifier kundennummerVerifier;

    private final String[] ARGENTINIENREG = new String[]{
        "Jujuy", "Salta",
        "Catamarca", "La Rioja", "San Juan", "Mendoza", "Valle De Uco",
        "San Rafael", "Rio Negro"};
    private final String[] AUSTRALIENREG = new String[]{"Hunter Valley",
        "Swan River Valley", "Barossa Valley"};
    private final String[] CHILEREG = new String[]{"Valle del Elqui",
        "Valle de Casablanca", "Valle de San Antonio", "Valle del Maipo",
        "Valle de Rapel", "Valle de Curicó", "Valle del Maule", "Valle del Itata",
        "Valle del Bío-Bío", "Valle del Malleco"};
    private final String[] DEUTSCHLANDREG = new String[]{"Ahrtal", "Baden", "Bayern",
        "Main", "Moseltal", "Nordfriesland-Holstein", "Saar", "Ruwer",
        "Mitteldeutschland", "Nahegau", "Pfalz", "Regensburg", "Rheingau",
        "Rhein", "Saarland-Mosel", "Sachsen", "Schwaben", "Starkenburg",
        "Taubertal"};
    private final String[] FRANKREICHREG = new String[]{"Margaux", "Pauillac",
        "Saint-Julien", "Saint-Estèphe", "Listrac", "Chablis", "Côte de Nuits",
        "Côte de Beaune", "Côte Chalonnaise", "Mâconnais", "Côtes du Marmandais",
        "Floc de Gascogne", "Gaillac", "Irouléguy", "Jurançon", "Côtes du Rhône",
        "Côte du Rhône Villages", "Châteauneuf-du-Pape", "Condrieu", "Cornas"};
    private final String[] GRIECHENLANDREG = new String[]{"Aminteon", "Goumenissa",
        "Naoussa", "Cotes de Meliton", "Anchialos", "Rapsani", "Zitsa",
        "Mantinia", "Nemea", "Patras", "Robola von Kefallonia", "Archánes",
        "Daphnes", "Peza", "Sitia", "Paros", "Rhodos", "Santorini",
        "Kantza", "Samos"};
    private final String[] ITALIENREG = new String[]{"Aostatal", "Piemont",
        "Ligurien", "Lombardei", "Trentino", "Südtirol", "Venetien",
        "Emilia-Romagna", "Marken", "Toskana", "Umbrien", "Latium", "Abruzzen",
        "Molise", "Apulien", "Kampanien", "Kalabrien", "Basilikata", "Sizilien",
        "Sardinien"};
    private final String[] KROATIENREG = new String[]{"Srijemsko vinogorje",
        "Erdutsko vinogorje", "Baranjsko vinogorje", "Đakovačko vinogorje",
        "Slavonskobrodsko vinogorje", "Novogradiško vinogorje",
        "Požeško-pleterničko vinogorje", "Kutjevačko vinogorje",
        "Daruvarsko vinogorje", "Pakračko vinogorje", "Feričanačko vinogorje",
        "Orahovičko-slatinsko vinogorje", "Virovitičko vinogorje",
        "Volodersko-Ivanićgradsko vinogorje", "Čazmansko-Garešničko vinogorje",
        "Dugoselsko-Vrbovečko vinogorje", "Kalničko vinogorje",
        "Koprivničko-Đurđevačko vinogorje",
        "Bjelovarsko-Grubišnopoljsko vinogorje", "Zelinsko vinogorje"};
    private final String[] MAZEDONIENREG = new String[]{"Gevevelija-Valandovo",
        "Kochani-Vinica", "Ovce Pole", "Skopje", "Strumica-Radoviste", "Tikveš",
        "Titov Veles", "Veles", "Kocani", "Kratovo", "Kumanovo", "Pijanecki",
        "Bitola", "Ohrid", "Tetovo", "Kicevo", "Prilep", "Prespa"};
    private final String[] MEXIKOREG = new String[]{"Aguascalientes", "Coahuila",
        "Sonora", "Durango", "Zacatecas", "Querétaro"};
    private final String[] NEUSEELANDREG = new String[]{"Northland", "Auckland",
        "Gisborne", "Hawke’s Bay", "Marlborough", "Nelson", "Canterbury",
        "Central Otago"};
    private final String[] OESTERREICH = new String[]{"Carnuntum", "Kamptal",
        "Kremstal", "Thermenregion", "Traisental", "Wachau", "Wagram",
        "Weinviertel", "Mittelburgenland", "Neusiedler See", "Südburgenland",
        "Wien", "Mittelburgenland", "Südoststeiermark", "Südsteiermark",
        "Weststeiermark"};
    private final String[] PORTUGALREG = new String[]{"Tavora-Varosa", "Barraida",
        "Dão", "Alto Douro", "Beira Interior", "Ribatejo", "Lourinha", "Obidos",
        "Alenquer", "Torres Vedras", "Arruda", "Bucelas", "Palmela", "Sétubal",
        "Alentejo", "Lagos", "Portimão", "Lagoa", "Tavira", "Madeira",
        "Pico Collares"};
    private final String[] RUMÄNIENREG = new String[]{"Moldova", "Walachei",
        "Dobrogea", "Banat", "Crișana"};
    private final String[] SCHWEIZREG = new String[]{"Ostschweiz", "Graubünden",
        "Drei-Seen-Land", "Waadt", "Genf", "Wallis", "Tessin", "Aargau"};
    private final String[] SPANIENREG = new String[]{"Andalusien", "Aragón",
        "Cataluña", "Extremadura", "Galicia", "Castilla-La Mancha",
        "Vinos de Madrid", "Murcia", "Navarra", "Ribera del Duero",
        "del Bierzo", "Rioja", "Utiel-Requena", "Valdepeñas", "Valencia",
        "Yecla", "Mallorca", "Ibiza", "Menorca", "Formentera"};
    private final String[] SUEDAFRIEKAREG = new String[]{"Western Cape",
        "Breede Rivier Valley", "Klein Karoo", "Boberg", "Olifants River",
        "Cape Agulhas"};
    private final String[] TSCHECHIENREG = new String[]{"Brno", "Bzenec", "Mikulov",
        "Mutěnice", "Velké Pavlovice", "Znojmo", "Strážnice", "Kyjov",
        "Uherské Hradiště", "Podluží", "Valtice", "Prag", "Čáslav", "Mělník",
        "Roudnice nad Labem", "Velké Žernoseky"};
    private final String[] UNGARNREG = new String[]{"Tokaj-Hegyalja", "Plattensee",
        "Baranya"};
    private final String[] USAREG = new String[]{"Arizona", "Idaho", "Kalifornien",
        "New Mexico", "New York", "North Carolina", "Ohio", "Oregon", "Texas",
        "Virginia", "Washington"};
    private final String[] LAENDER = new String[]{
        "Argentinien", "Australien", "Chile", "Deutschland", "Frankreich",
        "Griechenland", "Italien", "Kroatien", "Mazedonien", "Mexiko",
        "Neuseeland", "Österreich", "Portugal", "Rumänien", "Schweiz",
        "Spanien", "Südafrika", "Tschechien", "Ungarn", "USA"};
    private final String[] REBSORTEN = new String[]{
        "", "Cabernet Sauvignon", "Brun Fourca", "Blauer Arbst",
        "Spätburgunder"};

    private final String NAME_EINGEBEN_MESSAGE = "Bitte geben Sie den Vor- und"
            + " Nachnamen in Kundendaten ein!";

    private final String NAME_EINGEBEN_TITLE = "Kein Name vorhanden!";

    private boolean buttonBottle = false;
    private boolean buttonLiter = false;
    private int bestellNr = 00;

    private int kundenNr = 0;

    private final PreisVerifier zahlenVerifier = new PreisVerifier();

    private boolean firstRun = true;

    private boolean comboBoxChanged = false;

    private final ArrayList<String[]> REB_LAENDER
            = new ArrayList<>();

    private final HashMap<String, String[]> LAND_UND_REGION
            = new HashMap<>();

    private final ArrayList<JComponent> PFLICHTFELDER_WEIN = new ArrayList<>();
    private final ArrayList<JComponent> PFLICHTFELDER_KUNDE = new ArrayList<>();

    private ArrayList<JTextField> leereFelder = new ArrayList<>();

    private KundenDaten kundenDaten = null;
    private WeinDaten weinDaten = null;

    private File aktuelleDatei = null;

    private final String SPEICHERN_MESSAGE = "Es sind noch Daten nicht "
            + "gespeichert worden! \nMöchten sie diese vorher sichern? \n"
            + "Wenn Sie dies nicht tuen, gehen alle veränderten Daten"
            + "verloren.";

    private final String SPEICHERN_TITLE = "Nicht gespeicherte Daten!";

    private final String KEINE_DATEI_GEFUNDEN_MESSAGE = "Es wurde keine Datei "
            + "gefunden!";

    private final String KEINE_DATEI_GEFUNDEN_TITLE = "Keine Datei gefunden!";

    private final String FEHLER_LESEN_MESSAGE = "Es ist ein Fehler beim Öffnen"
            + " der Datei passiert!";

    private final String FEHLER_LESEN_TITLE = "Fehler beim lesen!";

    private final String NICHTS_EINGELESEN_MESSAGE = "Die ausgewählte Datei"
            + " enthielt keine Informationen!"
            + "\n Es Wurde nichts eingelesen!";

    private final String ALLES_OK_MESSAGE = "Es wurde alles erfolgreich eingelesen!";

    private final String ALLES_OK_TITLE = "Erfolgreich eingelesen!";

    private final String FEHELER_BEIM_LESEN_MESSAGE = "Es ist/sind ein oder"
            + " mehrere Fehler beim einlesen aufgetreten!";

    private final String FEHLER_BEIM_LESEN_TITLE = "Fehler beim einlesen!";

    private final String NICHTS_EINGELESEN_TITLE = "Nichts eingelesen!";

    private final String FEHLER_SPEICHERN_MESSAGE = "Es ist ein Fehler beim"
            + " speichern passiert!";

    private final String FEHLER_SPEICHERN_TITLE = "Fehler beim Speichern!";

    private final String SPEICHERN_ERFOLGREICH_MESSAGE = "Es wurde erfolgreich"
            + " gespeichert!";

    private final String SPEICHERN_ERFOLGREICH_TITLE = "Speichern erfolgreich!";

    private final String DIALOG_BESCHREIBUNG = "Wein- und Kundendaten (*.wkd)";
    private final String DIALOG_ENDUNG = "wkd";

    private final FileNameExtensionFilter filter = new FileNameExtensionFilter(DIALOG_BESCHREIBUNG, DIALOG_ENDUNG);

    private JFileChooser dateiAuswahl = new JFileChooser();

    private class HausnummerDocument extends PlainDocument {

        private final String REGEX_STR = "[0-9]|[a-zA-Z]";
        private final String REGEX_TEXT = "[0-9]+[a-zA-Z]?";
        private final String DELIM = ";";

        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            boolean gueltig = true;
            boolean buchstabe = false;
            String text = jTHausnummer.getText();

            for (int i = 0; i < text.length() && !buchstabe; i++) {
                buchstabe = ("" + text.charAt(i)).matches("[a-zA-Z]");
            }

            if (str.matches(REGEX_STR)
                    && (buchstabe
                    || (jTHausnummer.getText().equals("") && str.matches("[a-zA-Z]")))
                    || str.equals(DELIM)) {
                Toolkit.getDefaultToolkit().beep();
                gueltig = false;
            }

            if (gueltig) {
                super.insertString(offs, str, a);
            }
        }

    }

    private class JahrgangVerifier extends InputVerifier {

        private final String REGEX = "[0-9]{4}";

        @Override
        public boolean verify(JComponent component) {
            boolean gueltig = false;
            String eingabe = ((JTextField) component).getText();

            if (eingabe.matches(REGEX)) {
                int aktuellesJahr
                        = new GregorianCalendar().get(GregorianCalendar.YEAR);
                int jahr = Integer.parseInt(eingabe);
                gueltig = jahr >= (aktuellesJahr - 25) && jahr <= aktuellesJahr;
            }

            return gueltig;
        }
    }

    private class LagerdauerVerifier extends InputVerifier {

        private final String REGEX = "[0-9]{4}";

        @Override
        public boolean verify(JComponent component) {
            boolean istGueltig = false;
            String inhalt = ((JTextField) component).getText();
            if (inhalt.matches(REGEX)) {
                if (!jTJahrgang.getText().equals("")) {
                    int lagerdauer = Integer.parseInt(inhalt);
                    int jahrgang = Integer.parseInt(jTJahrgang.getText());
                    int aktuellesJahr
                            = new GregorianCalendar().get(GregorianCalendar.YEAR);
                    istGueltig = (aktuellesJahr - 25) <= lagerdauer
                            && (aktuellesJahr + 25) >= lagerdauer
                            && jahrgang <= lagerdauer;
                }
            }
            return istGueltig;
        }

    }

    private class FocusOrder extends FocusTraversalPolicy {

        ArrayList<JComponent> order;

        public FocusOrder(ArrayList<JComponent> order) {
            this.order = order;
        }

        @Override
        public Component getComponentAfter(Container aContainer, Component aComponent) {
            int index = order.indexOf(aComponent);
            if (!order.get((index + 1) % order.size()).isEnabled()) {
                boolean enabled = false;
                while (!enabled) {
                    index = (index + 1) % order.size();
                    enabled = order.get(index).isEnabled();
                }
            } else {
                index = (order.indexOf(aComponent) + 1) % order.size();
            }
            return order.get(index);
        }

        @Override
        public Component getComponentBefore(Container aContainer, Component aComponent) {
            int index = order.indexOf(aComponent);
            if (!order.get((index - 1) % order.size()).isEnabled()) {
                boolean enabled = false;
                while (!enabled) {
                    index = (index - 1) % order.size();
                    enabled = order.get(index).isEnabled();
                }
            } else {
                index = (order.indexOf(aComponent) - 1) % order.size();
            }
            return order.get(index);
        }

        @Override
        public Component getFirstComponent(Container aContainer) {
            return order.get(0);
        }

        @Override
        public Component getLastComponent(Container aContainer) {
            return order.get(order.size() - 1);
        }

        @Override
        public Component getDefaultComponent(Container aContainer) {
            return order.get(0);
        }

    }

    /**
     * Creates new form Weinverwaltung
     */
    public Weinverwaltung() {
        fuelleRebLaender();
        fuelleLandUndRegionBeziehung(REB_LAENDER);
        initComponents();
        jTName.setDocument(new DefaultDocument());
        jTLagerfaehigkeit.setDocument(new ZahlenDocument());
        jTFlaschenpreis.setInputVerifier(new PreisVerifier());
        jTFlaschenpreis.setDocument(new PreisDocument());
        jTLiterpreis.setInputVerifier(new PreisVerifier());
        jTLiterpreis.setDocument(new PreisDocument());
        jTKundenNr.setDocument(new PLZDocument());
        jTTelefonnummer.setDocument(new TelefonDocument());
        jTPLZ.setDocument(new PLZDocument());
        jTKontoNr.setDocument(new KontonummerDocument());
        jTBankleitzahl.setDocument(new BankleitzahlDocument());
        jTKreditinstitut.setDocument(new BuchstabenDocument());
        jTKontoinhaber.setDocument(new BuchstabenDocument());
        jTHausnummer.setDocument(new HausnummerDocument());
        jTVorname.setDocument(new BuchstabenDocument());
        jTNachname.setDocument(new BuchstabenDocument());
        jTStrasse.setDocument(new DefaultDocument());
        jTOrt.setDocument(new DefaultDocument());

        fuelleLaender(LAENDER);

        namenVerifier = new NamenVerifier();
        kundennummerVerifier = new KundennummerVerifier();
        bestellNrVerifier = new BestellnummerVerifier();
        jahrVerifier = new JahrgangVerifier();
        lagerdauerVerifier = new LagerdauerVerifier();
        jTBestellnummer.setDocument(new BestellnummerDocument());
        jTJahrgang.setDocument(new JahrDocument());
        resetVisibilityWein();
        resetVisibilityKunde();

        // Pflichtfelder Wein füllen
        PFLICHTFELDER_WEIN.add(jTName);
        PFLICHTFELDER_WEIN.add(jRWeiss);
        PFLICHTFELDER_WEIN.add(jCAnbaugebiet);
        PFLICHTFELDER_WEIN.add(jCRegion);
        PFLICHTFELDER_WEIN.add(jCAlkoholgehalt);
        PFLICHTFELDER_WEIN.add(jTLagerfaehigkeit);
        PFLICHTFELDER_WEIN.add(jCFlaschengroesse);

        // Pflichtfelder Kunde füllen
        PFLICHTFELDER_KUNDE.add(jTKundenNr);
        PFLICHTFELDER_KUNDE.add(jRHerr);
        PFLICHTFELDER_KUNDE.add(jTVorname);
        PFLICHTFELDER_KUNDE.add(jTNachname);
        PFLICHTFELDER_KUNDE.add(jTTelefonnummer);
        PFLICHTFELDER_KUNDE.add(jTStrasse);
        PFLICHTFELDER_KUNDE.add(jTHausnummer);
        PFLICHTFELDER_KUNDE.add(jTPLZ);
        PFLICHTFELDER_KUNDE.add(jTOrt);
        PFLICHTFELDER_KUNDE.add(jTKontoinhaber);
        PFLICHTFELDER_KUNDE.add(jTKontoNr);
        PFLICHTFELDER_KUNDE.add(jTBankleitzahl);
        PFLICHTFELDER_KUNDE.add(jTKreditinstitut);

        fuelleJCRebsorten(REBSORTEN);

        // Vorbelegen
        resetInternalFrameWein();
        resetInternalFrameKunde();

        firstRun = false;

        dateiAuswahl.addChoosableFileFilter(filter);
        dateiAuswahl.setAcceptAllFileFilterUsed(true);
        dateiAuswahl.setFileFilter(filter);

        speichernMenuItem.setEnabled(false);
        speichernUnterMenuItem.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bGFarben = new javax.swing.ButtonGroup();
        bGAnrede = new javax.swing.ButtonGroup();
        desktopPane = new javax.swing.JDesktopPane();
        jIFweinAufnehmen = new javax.swing.JInternalFrame();
        jTPWeinaufnahme = new javax.swing.JTabbedPane();
        jPFormular = new javax.swing.JPanel();
        jLBestellnummer = new javax.swing.JLabel();
        jTBestellnummer = new javax.swing.JTextField();
        jLJahrgang = new javax.swing.JLabel();
        jTJahrgang = new javax.swing.JTextField();
        jLName = new javax.swing.JLabel();
        jTName = new javax.swing.JTextField();
        jLFarbe = new javax.swing.JLabel();
        jRWeiss = new javax.swing.JRadioButton();
        jRRot = new javax.swing.JRadioButton();
        jRRose = new javax.swing.JRadioButton();
        jLRebsorte = new javax.swing.JLabel();
        jLAnbaugebiet = new javax.swing.JLabel();
        jCAnbaugebiet = new javax.swing.JComboBox();
        jLAlkoholgehalt = new javax.swing.JLabel();
        jCAlkoholgehalt = new javax.swing.JComboBox();
        jLLagerfaehigkeit = new javax.swing.JLabel();
        jTLagerfaehigkeit = new javax.swing.JTextField();
        jLFlaschengroesse = new javax.swing.JLabel();
        jCFlaschengroesse = new javax.swing.JComboBox();
        jLFlaschenpreis = new javax.swing.JLabel();
        jTFlaschenpreis = new javax.swing.JTextField();
        jBAbbrechen = new javax.swing.JButton();
        jBSpeichern = new javax.swing.JButton();
        jLUngueltigBestellNr = new javax.swing.JLabel();
        jLUngueltigJahrgang = new javax.swing.JLabel();
        jCRebsorte1 = new javax.swing.JComboBox();
        jCRebsorte2 = new javax.swing.JComboBox();
        jCRebsorte3 = new javax.swing.JComboBox();
        jLHilfeBestellNr = new javax.swing.JLabel();
        jLHilfeJahrgang = new javax.swing.JLabel();
        jLAusfuellenName = new javax.swing.JLabel();
        jLAuswaehlenAnbaugebiet = new javax.swing.JLabel();
        jLUngueltigLagerfaehigkeit = new javax.swing.JLabel();
        jLHilfeLagerfaehigkeit = new javax.swing.JLabel();
        jLUngueltigFlaschenpreis = new javax.swing.JLabel();
        jLEuro = new javax.swing.JLabel();
        jLComboboxDoppelt = new javax.swing.JLabel();
        jLRegion = new javax.swing.JLabel();
        jCRegion = new javax.swing.JComboBox();
        jLRegionAuswaehlen = new javax.swing.JLabel();
        jBLiterpreisInFlaschenpreis = new javax.swing.JButton();
        jLPreisProLiter = new javax.swing.JLabel();
        jTLiterpreis = new javax.swing.JTextField();
        jLEuro2 = new javax.swing.JLabel();
        jBFlaschenpreisInLiterpreis = new javax.swing.JButton();
        jLUngueltigLiterpreis = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPDiagramm = new javax.swing.JPanel();
        jCLagerdauer = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        weinreifeDiagramm = new WeinreifeDiagramm();
        weinreifeLegende = new WeinreifeLegende();
        jIFKundeAufnehmen = new javax.swing.JInternalFrame();
        jLKundennummer = new javax.swing.JLabel();
        jLAnrede = new javax.swing.JLabel();
        jLVorname = new javax.swing.JLabel();
        jLNachname = new javax.swing.JLabel();
        jLStrasse = new javax.swing.JLabel();
        jLHausnummer = new javax.swing.JLabel();
        jLPLZ = new javax.swing.JLabel();
        jLOrt = new javax.swing.JLabel();
        jLTelefonNr = new javax.swing.JLabel();
        jLKontoinhaber = new javax.swing.JLabel();
        jLKontoNr = new javax.swing.JLabel();
        jLBankleitzahl = new javax.swing.JLabel();
        jLKreditinstitut = new javax.swing.JLabel();
        jTKundenNr = new javax.swing.JTextField();
        jRHerr = new javax.swing.JRadioButton();
        jRFrau = new javax.swing.JRadioButton();
        jTVorname = new javax.swing.JTextField();
        jTNachname = new javax.swing.JTextField();
        jTStrasse = new javax.swing.JTextField();
        jTHausnummer = new javax.swing.JTextField();
        jTPLZ = new javax.swing.JTextField();
        jTOrt = new javax.swing.JTextField();
        jTTelefonnummer = new javax.swing.JTextField();
        jTKontoinhaber = new javax.swing.JTextField();
        jTKontoNr = new javax.swing.JTextField();
        jTBankleitzahl = new javax.swing.JTextField();
        jTKreditinstitut = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLAnschrift = new javax.swing.JLabel();
        jBAbbrechenKunde = new javax.swing.JButton();
        jBSpeichernKunde = new javax.swing.JButton();
        jLBankverbindung = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLKundendaten = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLUngueltigKundenNr = new javax.swing.JLabel();
        jLHilfeKundenNr = new javax.swing.JLabel();
        jLUngueltigPLZ = new javax.swing.JLabel();
        jLHilfePLZ = new javax.swing.JLabel();
        jLUngueltigNachname = new javax.swing.JLabel();
        jLUngueltigVorname = new javax.swing.JLabel();
        jLHilfeVorname = new javax.swing.JLabel();
        jLHilfeNachname = new javax.swing.JLabel();
        jLUngueltigKontoinhaber = new javax.swing.JLabel();
        jLHilfeKontoinhaber = new javax.swing.JLabel();
        jLAusfuellen = new javax.swing.JLabel();
        jCUebernehmen = new javax.swing.JCheckBox();
        menuBar = new javax.swing.JMenuBar();
        dateiMenue = new javax.swing.JMenu();
        oeffnenMenuItem = new javax.swing.JMenuItem();
        speichernMenuItem = new javax.swing.JMenuItem();
        speichernUnterMenuItem = new javax.swing.JMenuItem();
        beendenMenuItem = new javax.swing.JMenuItem();
        bearbeitenMenue = new javax.swing.JMenu();
        kundenMenue = new javax.swing.JMenu();
        kundenAufnehmenMenuItem = new javax.swing.JMenuItem();
        kundenAendernMenuItem = new javax.swing.JMenuItem();
        kundenLoeschenMenuItem = new javax.swing.JMenuItem();
        weinMenue = new javax.swing.JMenu();
        weinAufnehmenMenuItem = new javax.swing.JMenuItem();
        weinAendernMenuItem = new javax.swing.JMenuItem();
        weinLoeschenMenuItem = new javax.swing.JMenuItem();
        hilfeMenue = new javax.swing.JMenu();
        hilfeMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Weinverwaltung");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jIFweinAufnehmen.setClosable(true);
        jIFweinAufnehmen.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jIFweinAufnehmen.setIconifiable(true);
        jIFweinAufnehmen.setMaximizable(true);
        jIFweinAufnehmen.setResizable(true);
        jIFweinAufnehmen.setTitle("Wein aufnehmen");
        jIFweinAufnehmen.setMinimumSize(new java.awt.Dimension(143, 100));
        jIFweinAufnehmen.setNormalBounds(new java.awt.Rectangle(100, 10, 500, 500));
        jIFweinAufnehmen.setPreferredSize(new java.awt.Dimension(550, 750));
        jIFweinAufnehmen.setVisible(false);
        jIFweinAufnehmen.addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                jIFweinAufnehmenInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jTPWeinaufnahme.setPreferredSize(new java.awt.Dimension(800, 600));

        jPFormular.setFocusTraversalPolicy(new FocusOrder(getOrder()));
        jPFormular.setFocusTraversalPolicyProvider(true);

        jLBestellnummer.setText("Bestellnummer*");

        jTBestellnummer.setMargin(new java.awt.Insets(0, 0, 0, 10));
        jTBestellnummer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTBestellnummerFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTBestellnummerFocusLost(evt);
            }
        });

        jLJahrgang.setText("Jahrgang*");

        jTJahrgang.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTJahrgangFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTJahrgangFocusLost(evt);
            }
        });

        jLName.setText("Name*");

        jTName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTNameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTNameFocusLost(evt);
            }
        });

        jLFarbe.setText("Farbe*");

        bGFarben.add(jRWeiss);
        jRWeiss.setSelected(true);
        jRWeiss.setText("weiß");

        bGFarben.add(jRRot);
        jRRot.setText("rot");

        bGFarben.add(jRRose);
        jRRose.setText("rose");

        jLRebsorte.setText("Rebsorte");

        jLAnbaugebiet.setText("Anbaugebiet*");

        jCAnbaugebiet.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCAnbaugebietItemStateChanged(evt);
            }
        });
        jCAnbaugebiet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCAnbaugebietActionPerformed(evt);
            }
        });
        jCAnbaugebiet.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCAnbaugebietFocusLost(evt);
            }
        });

        jLAlkoholgehalt.setText("Alkoholgehalt*");

        jCAlkoholgehalt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0 Vol%", "7,5 Vol%", "8 Vol%", "8,5 Vol%", "9 Vol%", "9,5 Vol%", "10 Vol%", "10,5 Vol%", "11 Vol%", "11,5 Vol%", "12 Vol%", "12,5 Vol%", "13 Vol%" }));
        jCAlkoholgehalt.setSelectedIndex(6);
        jCAlkoholgehalt.setSelectedItem(6);
        jCAlkoholgehalt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCAlkoholgehaltActionPerformed(evt);
            }
        });

        jLLagerfaehigkeit.setText("Lagerfähigkeit*");

        jTLagerfaehigkeit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTLagerfaehigkeitFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTLagerfaehigkeitFocusLost(evt);
            }
        });

        jLFlaschengroesse.setText("Flaschengröße*");

        jCFlaschengroesse.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0,187 l", "0,25 l", "0,375 l", "0,5 l", "0,62 l", "0,7 l", "0,75 l", "0,8 l", "1 l", "1,5 l" }));
        jCFlaschengroesse.setSelectedIndex(5);
        jCFlaschengroesse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCFlaschengroesseActionPerformed(evt);
            }
        });

        jLFlaschenpreis.setText("Flaschenpreis*");

        jTFlaschenpreis.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTFlaschenpreis.setText("0");
        jTFlaschenpreis.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTFlaschenpreisFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTFlaschenpreisFocusLost(evt);
            }
        });
        jTFlaschenpreis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTFlaschenpreisKeyPressed(evt);
            }
        });

        jBAbbrechen.setText("Abbrechen");
        jBAbbrechen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAbbrechenActionPerformed(evt);
            }
        });

        jBSpeichern.setText("Speichern");
        jBSpeichern.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSpeichernActionPerformed(evt);
            }
        });

        jLUngueltigBestellNr.setForeground(new java.awt.Color(255, 3, 0));
        jLUngueltigBestellNr.setText("ungültig!");

        jLUngueltigJahrgang.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigJahrgang.setText("ungültig!");

        jCRebsorte1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRebsorte1ActionPerformed(evt);
            }
        });

        jCRebsorte2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte2.setEnabled(false);
        jCRebsorte2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRebsorte2ActionPerformed(evt);
            }
        });

        jCRebsorte3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));
        jCRebsorte3.setEnabled(false);
        jCRebsorte3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRebsorte3ActionPerformed(evt);
            }
        });

        jLHilfeBestellNr.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeBestellNr.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeBestellNr.setText("?");
        jLHilfeBestellNr.setToolTipText("Hilfe");
        jLHilfeBestellNr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeBestellNrMouseClicked(evt);
            }
        });

        jLHilfeJahrgang.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeJahrgang.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeJahrgang.setText("?");
        jLHilfeJahrgang.setToolTipText("Hilfe");
        jLHilfeJahrgang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLHilfeJahrgangMousePressed(evt);
            }
        });

        jLAusfuellenName.setForeground(new java.awt.Color(255, 0, 0));
        jLAusfuellenName.setText("bitte ausfüllen!");

        jLAuswaehlenAnbaugebiet.setForeground(new java.awt.Color(255, 0, 0));
        jLAuswaehlenAnbaugebiet.setText("bitte auswählen!");

        jLUngueltigLagerfaehigkeit.setForeground(new java.awt.Color(250, 0, 0));
        jLUngueltigLagerfaehigkeit.setText("ungültig!");

        jLHilfeLagerfaehigkeit.setFont(new java.awt.Font("Cantarell", 1, 15)); // NOI18N
        jLHilfeLagerfaehigkeit.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeLagerfaehigkeit.setText("?");
        jLHilfeLagerfaehigkeit.setToolTipText("Hilfe");
        jLHilfeLagerfaehigkeit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeLagerfaehigkeitMouseClicked(evt);
            }
        });

        jLUngueltigFlaschenpreis.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigFlaschenpreis.setText("ungültig!");

        jLEuro.setText("€");

        jLComboboxDoppelt.setForeground(new java.awt.Color(255, 0, 0));
        jLComboboxDoppelt.setText("doppelte Einträge!");

        jLRegion.setText("Region*");

        jCRegion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bitte Anbaugebiet auswählen" }));
        jCRegion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCRegionItemStateChanged(evt);
            }
        });
        jCRegion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCRegionActionPerformed(evt);
            }
        });
        jCRegion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCRegionFocusLost(evt);
            }
        });

        jLRegionAuswaehlen.setForeground(new java.awt.Color(255, 0, 0));
        jLRegionAuswaehlen.setText("bitte auswählen!");

        jBLiterpreisInFlaschenpreis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/monotone_arrow_up_60.png"))); // NOI18N
        jBLiterpreisInFlaschenpreis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLiterpreisInFlaschenpreisActionPerformed(evt);
            }
        });

        jLPreisProLiter.setText("Preis pro l");

        jTLiterpreis.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTLiterpreis.setText("0");
        jTLiterpreis.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTLiterpreisFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTLiterpreisFocusLost(evt);
            }
        });
        jTLiterpreis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTLiterpreisKeyPressed(evt);
            }
        });

        jLEuro2.setText("€");

        jBFlaschenpreisInLiterpreis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/monotone_arrow_down_60.png"))); // NOI18N
        jBFlaschenpreisInLiterpreis.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jBFlaschenpreisInLiterpreis.setMaximumSize(new java.awt.Dimension(74, 72));
        jBFlaschenpreisInLiterpreis.setMinimumSize(new java.awt.Dimension(74, 72));
        jBFlaschenpreisInLiterpreis.setPreferredSize(new java.awt.Dimension(74, 72));
        jBFlaschenpreisInLiterpreis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBFlaschenpreisInLiterpreisActionPerformed(evt);
            }
        });

        jLUngueltigLiterpreis.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigLiterpreis.setText("ungültig!");

        jLabel1.setText("Mit * markierte Felder sind Pflichtfelder!");

        javax.swing.GroupLayout jPFormularLayout = new javax.swing.GroupLayout(jPFormular);
        jPFormular.setLayout(jPFormularLayout);
        jPFormularLayout.setHorizontalGroup(
            jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularLayout.createSequentialGroup()
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPFormularLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLAnbaugebiet)
                            .addGroup(jPFormularLayout.createSequentialGroup()
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLFlaschengroesse)
                                    .addComponent(jLFlaschenpreis))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCFlaschengroesse, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jBAbbrechen)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jBSpeichern))
                                            .addComponent(jTFlaschenpreis, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                                            .addComponent(jTLiterpreis, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jLEuro)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLUngueltigFlaschenpreis))
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jLEuro2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLUngueltigLiterpreis))))))
                            .addGroup(jPFormularLayout.createSequentialGroup()
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLAlkoholgehalt)
                                    .addComponent(jLLagerfaehigkeit))
                                .addGap(18, 18, 18)
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jTLagerfaehigkeit, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLUngueltigLagerfaehigkeit)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLHilfeLagerfaehigkeit))
                                    .addComponent(jCAlkoholgehalt, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPFormularLayout.createSequentialGroup()
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLJahrgang)
                                    .addComponent(jLName)
                                    .addComponent(jLFarbe)
                                    .addComponent(jLBestellnummer)
                                    .addComponent(jLRegion)
                                    .addComponent(jLRebsorte))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCRebsorte2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCRebsorte3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jCRebsorte1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLComboboxDoppelt))
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTName, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTJahrgang, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTBestellnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jRWeiss, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(22, 22, 22)
                                                .addComponent(jRRot)
                                                .addGap(18, 18, 18)
                                                .addComponent(jRRose))
                                            .addComponent(jCAnbaugebiet, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jLUngueltigJahrgang)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfeJahrgang))
                                            .addGroup(jPFormularLayout.createSequentialGroup()
                                                .addComponent(jLUngueltigBestellNr)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfeBestellNr))
                                            .addComponent(jLAusfuellenName)
                                            .addComponent(jLAuswaehlenAnbaugebiet)))
                                    .addGroup(jPFormularLayout.createSequentialGroup()
                                        .addComponent(jCRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLRegionAuswaehlen))))
                            .addComponent(jLPreisProLiter)))
                    .addGroup(jPFormularLayout.createSequentialGroup()
                        .addGap(184, 184, 184)
                        .addComponent(jBFlaschenpreisInLiterpreis, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBLiterpreisInFlaschenpreis, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPFormularLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPFormularLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jCAlkoholgehalt, jCAnbaugebiet, jCFlaschengroesse, jTBestellnummer, jTFlaschenpreis, jTJahrgang, jTLagerfaehigkeit, jTName});

        jPFormularLayout.setVerticalGroup(
            jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLBestellnummer)
                    .addComponent(jTBestellnummer)
                    .addComponent(jLUngueltigBestellNr)
                    .addComponent(jLHilfeBestellNr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTJahrgang)
                    .addComponent(jLJahrgang)
                    .addComponent(jLUngueltigJahrgang)
                    .addComponent(jLHilfeJahrgang))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTName)
                    .addComponent(jLName)
                    .addComponent(jLAusfuellenName))
                .addGap(8, 8, 8)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRWeiss)
                    .addComponent(jLFarbe)
                    .addComponent(jRRot)
                    .addComponent(jRRose))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCAnbaugebiet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLAnbaugebiet)
                    .addComponent(jLAuswaehlenAnbaugebiet))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLRegion)
                    .addComponent(jCRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLRegionAuswaehlen))
                .addGap(18, 18, 18)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLRebsorte)
                    .addComponent(jCRebsorte1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLComboboxDoppelt))
                .addGap(18, 18, 18)
                .addComponent(jCRebsorte2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCRebsorte3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLAlkoholgehalt)
                    .addComponent(jCAlkoholgehalt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLLagerfaehigkeit)
                    .addComponent(jTLagerfaehigkeit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigLagerfaehigkeit)
                    .addComponent(jLHilfeLagerfaehigkeit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCFlaschengroesse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLFlaschengroesse))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLFlaschenpreis)
                    .addComponent(jTFlaschenpreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigFlaschenpreis)
                    .addComponent(jLEuro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBFlaschenpreisInLiterpreis, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBLiterpreisInFlaschenpreis, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTLiterpreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLPreisProLiter)
                    .addComponent(jLEuro2)
                    .addComponent(jLUngueltigLiterpreis))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPFormularLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAbbrechen)
                    .addComponent(jBSpeichern))
                .addGap(706, 706, 706))
        );

        jLUngueltigBestellNr.getAccessibleContext().setAccessibleParent(jLUngueltigBestellNr);

        jTPWeinaufnahme.addTab("Aufnahmeformular", jPFormular);

        jCLagerdauer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" }));
        jCLagerdauer.setSelectedIndex(weinreifeDiagramm.lagerdauer-1);
        jCLagerdauer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCLagerdauerActionPerformed(evt);
            }
        });

        weinreifeDiagramm.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                weinreifeDiagrammPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout weinreifeDiagrammLayout = new javax.swing.GroupLayout(weinreifeDiagramm);
        weinreifeDiagramm.setLayout(weinreifeDiagrammLayout);
        weinreifeDiagrammLayout.setHorizontalGroup(
            weinreifeDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        weinreifeDiagrammLayout.setVerticalGroup(
            weinreifeDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 197, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout weinreifeLegendeLayout = new javax.swing.GroupLayout(weinreifeLegende);
        weinreifeLegende.setLayout(weinreifeLegendeLayout);
        weinreifeLegendeLayout.setHorizontalGroup(
            weinreifeLegendeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        weinreifeLegendeLayout.setVerticalGroup(
            weinreifeLegendeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(weinreifeLegende, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(weinreifeDiagramm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(weinreifeDiagramm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(weinreifeLegende, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPDiagrammLayout = new javax.swing.GroupLayout(jPDiagramm);
        jPDiagramm.setLayout(jPDiagrammLayout);
        jPDiagrammLayout.setHorizontalGroup(
            jPDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jCLagerdauer, 0, 580, Short.MAX_VALUE)
        );
        jPDiagrammLayout.setVerticalGroup(
            jPDiagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPDiagrammLayout.createSequentialGroup()
                .addComponent(jCLagerdauer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 858, Short.MAX_VALUE))
        );

        jTPWeinaufnahme.addTab("Weindiagramm", jPDiagramm);

        javax.swing.GroupLayout jIFweinAufnehmenLayout = new javax.swing.GroupLayout(jIFweinAufnehmen.getContentPane());
        jIFweinAufnehmen.getContentPane().setLayout(jIFweinAufnehmenLayout);
        jIFweinAufnehmenLayout.setHorizontalGroup(
            jIFweinAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFweinAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTPWeinaufnahme, javax.swing.GroupLayout.DEFAULT_SIZE, 718, Short.MAX_VALUE)
                .addContainerGap())
        );
        jIFweinAufnehmenLayout.setVerticalGroup(
            jIFweinAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFweinAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTPWeinaufnahme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        desktopPane.add(jIFweinAufnehmen);
        jIFweinAufnehmen.setBounds(100, 10, 550, 750);

        jIFKundeAufnehmen.setClosable(true);
        jIFKundeAufnehmen.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        jIFKundeAufnehmen.setMaximizable(true);
        jIFKundeAufnehmen.setResizable(true);
        jIFKundeAufnehmen.setTitle("Kunde Aufnehmen");
        jIFKundeAufnehmen.setPreferredSize(new java.awt.Dimension(650, 725));
        jIFKundeAufnehmen.setVisible(false);
        jIFKundeAufnehmen.addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                jIFKundeAufnehmenInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jLKundennummer.setText("Kundennummer");

        jLAnrede.setText("Anrede");

        jLVorname.setText("Vorname");

        jLNachname.setText("Nachname");

        jLStrasse.setText("Straße");

        jLHausnummer.setText("Hausnummer");

        jLPLZ.setText("PLZ");

        jLOrt.setText("Ort");

        jLTelefonNr.setText("Telefonnummer");

        jLKontoinhaber.setText("Kontoinhaber");

        jLKontoNr.setText("Kontonummer");

        jLBankleitzahl.setText("Bankleitzahl");

        jLKreditinstitut.setText("Kreditinstitut");

        jTKundenNr.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTKundenNr.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTKundenNrFocusLost(evt);
            }
        });

        bGAnrede.add(jRHerr);
        jRHerr.setSelected(true);
        jRHerr.setText("Herr");

        bGAnrede.add(jRFrau);
        jRFrau.setText("Frau");

        jTVorname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTVornameFocusLost(evt);
            }
        });

        jTNachname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTNachnameFocusLost(evt);
            }
        });

        jTStrasse.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTStrasseFocusLost(evt);
            }
        });

        jTHausnummer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTHausnummerFocusLost(evt);
            }
        });

        jTPLZ.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTPLZ.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTPLZFocusLost(evt);
            }
        });

        jTOrt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTOrtFocusLost(evt);
            }
        });

        jTTelefonnummer.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTTelefonnummer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTTelefonnummerFocusLost(evt);
            }
        });

        jTKontoinhaber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTKontoinhaberFocusLost(evt);
            }
        });

        jTKontoNr.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTKontoNr.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTKontoNrFocusLost(evt);
            }
        });

        jTBankleitzahl.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTBankleitzahl.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTBankleitzahlFocusLost(evt);
            }
        });

        jTKreditinstitut.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTKreditinstitutFocusLost(evt);
            }
        });

        jLAnschrift.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLAnschrift.setText("Anschrift");

        jBAbbrechenKunde.setText("Abbrechen");
        jBAbbrechenKunde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAbbrechenKundeActionPerformed(evt);
            }
        });

        jBSpeichernKunde.setText("Speichern");
        jBSpeichernKunde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSpeichernKundeActionPerformed(evt);
            }
        });

        jLBankverbindung.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLBankverbindung.setText("Bankverbindung");

        jLKundendaten.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLKundendaten.setText("Kundendaten");

        jLUngueltigKundenNr.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigKundenNr.setText("ungültig!");

        jLHilfeKundenNr.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLHilfeKundenNr.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeKundenNr.setText("?");
        jLHilfeKundenNr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeKundenNrMouseClicked(evt);
            }
        });

        jLUngueltigPLZ.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigPLZ.setText("ungültig!");

        jLHilfePLZ.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLHilfePLZ.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfePLZ.setText("?");
        jLHilfePLZ.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfePLZMouseClicked(evt);
            }
        });

        jLUngueltigNachname.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigNachname.setText("ungültig!");

        jLUngueltigVorname.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigVorname.setText("ungültig!");

        jLHilfeVorname.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLHilfeVorname.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeVorname.setText("?");
        jLHilfeVorname.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeVornameMouseClicked(evt);
            }
        });

        jLHilfeNachname.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLHilfeNachname.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeNachname.setText("?");
        jLHilfeNachname.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeNachnameMouseClicked(evt);
            }
        });

        jLUngueltigKontoinhaber.setForeground(new java.awt.Color(255, 0, 0));
        jLUngueltigKontoinhaber.setText("ungültig!");

        jLHilfeKontoinhaber.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLHilfeKontoinhaber.setForeground(new java.awt.Color(0, 0, 255));
        jLHilfeKontoinhaber.setText("?");
        jLHilfeKontoinhaber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLHilfeKontoinhaberMouseClicked(evt);
            }
        });

        jLAusfuellen.setForeground(new java.awt.Color(255, 0, 0));
        jLAusfuellen.setText("Bitte die rot markierten Fehlder ausfüllen!");

        jCUebernehmen.setText("Name übernehmen?");
        jCUebernehmen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCUebernehmenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jIFKundeAufnehmenLayout = new javax.swing.GroupLayout(jIFKundeAufnehmen.getContentPane());
        jIFKundeAufnehmen.getContentPane().setLayout(jIFKundeAufnehmenLayout);
        jIFKundeAufnehmenLayout.setHorizontalGroup(
            jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                        .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLAnschrift)
                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLKontoinhaber)
                                    .addComponent(jLKontoNr)
                                    .addComponent(jLBankleitzahl)
                                    .addComponent(jLKreditinstitut))
                                .addGap(29, 29, 29)
                                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                        .addComponent(jTKontoinhaber, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jCUebernehmen)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLUngueltigKontoinhaber)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLHilfeKontoinhaber))
                                    .addComponent(jTBankleitzahl, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTKreditinstitut, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTKontoNr, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                .addComponent(jBAbbrechenKunde)
                                .addGap(211, 211, 211)
                                .addComponent(jBSpeichernKunde))
                            .addComponent(jLBankverbindung))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jIFKundeAufnehmenLayout.createSequentialGroup()
                        .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jIFKundeAufnehmenLayout.createSequentialGroup()
                                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLKundennummer, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLKundendaten, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLAusfuellen, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jIFKundeAufnehmenLayout.createSequentialGroup()
                                        .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLHausnummer)
                                            .addComponent(jLPLZ)
                                            .addComponent(jLStrasse)
                                            .addComponent(jLOrt))
                                        .addGap(35, 35, 35)
                                        .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTOrt, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                                .addComponent(jTPLZ, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLUngueltigPLZ)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfePLZ))
                                            .addComponent(jTHausnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLVorname, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLNachname, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jIFKundeAufnehmenLayout.createSequentialGroup()
                                        .addComponent(jLTelefonNr)
                                        .addGap(19, 19, 19)
                                        .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                                .addComponent(jTNachname)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLUngueltigNachname)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfeNachname))
                                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                                .addComponent(jTVorname)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLUngueltigVorname)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfeVorname))
                                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                                .addComponent(jRHerr)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jRFrau))
                                            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                                                .addComponent(jTKundenNr, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLUngueltigKundenNr)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLHilfeKundenNr))
                                            .addComponent(jTTelefonnummer, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLAnrede, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );

        jIFKundeAufnehmenLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jTKundenNr, jTNachname, jTVorname});

        jIFKundeAufnehmenLayout.setVerticalGroup(
            jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jIFKundeAufnehmenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLAusfuellen)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLKundendaten)
                .addGap(5, 5, 5)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLKundennummer)
                    .addComponent(jTKundenNr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigKundenNr)
                    .addComponent(jLHilfeKundenNr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLAnrede)
                    .addComponent(jRHerr)
                    .addComponent(jRFrau))
                .addGap(13, 13, 13)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLVorname)
                    .addComponent(jTVorname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigVorname)
                    .addComponent(jLHilfeVorname))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLNachname)
                    .addComponent(jTNachname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigNachname)
                    .addComponent(jLHilfeNachname))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTTelefonnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLTelefonNr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jLAnschrift)
                .addGap(18, 18, 18)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLStrasse))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTHausnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLHausnummer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTPLZ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigPLZ)
                    .addComponent(jLHilfePLZ)
                    .addComponent(jLPLZ))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTOrt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLOrt))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLBankverbindung)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTKontoinhaber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUngueltigKontoinhaber)
                    .addComponent(jLHilfeKontoinhaber)
                    .addComponent(jLKontoinhaber)
                    .addComponent(jCUebernehmen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTKontoNr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLKontoNr))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTBankleitzahl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLBankleitzahl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTKreditinstitut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLKreditinstitut))
                .addGap(18, 18, 18)
                .addGroup(jIFKundeAufnehmenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAbbrechenKunde)
                    .addComponent(jBSpeichernKunde))
                .addContainerGap(113, Short.MAX_VALUE))
        );

        desktopPane.add(jIFKundeAufnehmen);
        jIFKundeAufnehmen.setBounds(20, 20, 650, 725);

        dateiMenue.setMnemonic('d');
        dateiMenue.setText("Datei");

        oeffnenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        oeffnenMenuItem.setMnemonic('f');
        oeffnenMenuItem.setText("Öffnen");
        oeffnenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oeffnenMenuItemActionPerformed(evt);
            }
        });
        dateiMenue.add(oeffnenMenuItem);

        speichernMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        speichernMenuItem.setMnemonic('s');
        speichernMenuItem.setText("Speichern");
        speichernMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                speichernMenuItemActionPerformed(evt);
            }
        });
        dateiMenue.add(speichernMenuItem);

        speichernUnterMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        speichernUnterMenuItem.setMnemonic('u');
        speichernUnterMenuItem.setText("Speichern unter...");
        speichernUnterMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                speichernUnterMenuItemActionPerformed(evt);
            }
        });
        dateiMenue.add(speichernUnterMenuItem);

        beendenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        beendenMenuItem.setMnemonic('b');
        beendenMenuItem.setText("Beenden");
        beendenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beendenMenuItemActionPerformed(evt);
            }
        });
        dateiMenue.add(beendenMenuItem);

        menuBar.add(dateiMenue);

        bearbeitenMenue.setMnemonic('b');
        bearbeitenMenue.setText("Bearbeiten");
        menuBar.add(bearbeitenMenue);

        kundenMenue.setMnemonic('k');
        kundenMenue.setText("Kunden");
        kundenMenue.setToolTipText("");

        kundenAufnehmenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        kundenAufnehmenMenuItem.setMnemonic('a');
        kundenAufnehmenMenuItem.setText("Aufnehmen");
        kundenAufnehmenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kundenAufnehmenMenuItemActionPerformed(evt);
            }
        });
        kundenMenue.add(kundenAufnehmenMenuItem);

        kundenAendernMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        kundenAendernMenuItem.setMnemonic('\u00e4');
        kundenAendernMenuItem.setText("Ändern");
        kundenMenue.add(kundenAendernMenuItem);

        kundenLoeschenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        kundenLoeschenMenuItem.setMnemonic('l');
        kundenLoeschenMenuItem.setText("Löschen");
        kundenMenue.add(kundenLoeschenMenuItem);

        menuBar.add(kundenMenue);

        weinMenue.setMnemonic('w');
        weinMenue.setText("Wein");

        weinAufnehmenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK));
        weinAufnehmenMenuItem.setMnemonic('a');
        weinAufnehmenMenuItem.setText("Aufnehmen");
        weinAufnehmenMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weinAufnehmenMenuItemActionPerformed(evt);
            }
        });
        weinMenue.add(weinAufnehmenMenuItem);

        weinAendernMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        weinAendernMenuItem.setMnemonic('\u00e4');
        weinAendernMenuItem.setText("Ändern");
        weinMenue.add(weinAendernMenuItem);

        weinLoeschenMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK));
        weinLoeschenMenuItem.setMnemonic('l');
        weinLoeschenMenuItem.setText("Löschen");
        weinMenue.add(weinLoeschenMenuItem);

        menuBar.add(weinMenue);

        hilfeMenue.setMnemonic('i');
        hilfeMenue.setText("?");

        hilfeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        hilfeMenuItem.setMnemonic('i');
        hilfeMenuItem.setText("Info");
        hilfeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hilfeMenuItemActionPerformed(evt);
            }
        });
        hilfeMenue.add(hilfeMenuItem);

        menuBar.add(hilfeMenue);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 779, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void beendenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_beendenMenuItemActionPerformed
        beenden();
    }//GEN-LAST:event_beendenMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        beenden();
    }//GEN-LAST:event_formWindowClosing

    private void hilfeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hilfeMenuItemActionPerformed
        JOptionPane.showMessageDialog(null, INFO_MESSAGE, INFO_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_hilfeMenuItemActionPerformed

    private void weinAufnehmenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_weinAufnehmenMenuItemActionPerformed
        jIFweinAufnehmen.setVisible(true);
    }//GEN-LAST:event_weinAufnehmenMenuItemActionPerformed

    private void jTBestellnummerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTBestellnummerFocusGained
        jTBestellnummer.selectAll();
    }//GEN-LAST:event_jTBestellnummerFocusGained

    private void jTBestellnummerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTBestellnummerFocusLost
        jTBestellnummer.select(0, 0);
        if (!bestellNrVerifier.verify(jTBestellnummer)) {
            jLUngueltigBestellNr.setVisible(true);
            jLHilfeBestellNr.setVisible(true);
//            if (!fehlerGefunden) {
//                jTBestellnummer.requestFocusInWindow();
//            }
//            fehlerGefunden = true;
        } else if (jLUngueltigBestellNr.isVisible() && jLHilfeBestellNr.isVisible()) {
            jLUngueltigBestellNr.setVisible(false);
            jLHilfeBestellNr.setVisible(false);
        }
    }//GEN-LAST:event_jTBestellnummerFocusLost

    private void jBAbbrechenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAbbrechenActionPerformed
        schliesseWeinInternalFrame();
    }//GEN-LAST:event_jBAbbrechenActionPerformed

    private void jBSpeichernActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSpeichernActionPerformed
        boolean fehlerGefunden = false;
        String farbe;
        String[] rebsorten = new String[3];
//        resetVisibility();

        if (!bestellNrVerifier.verify(jTBestellnummer)) {
            jLUngueltigBestellNr.setVisible(true);
            jLHilfeBestellNr.setVisible(true);
            if (!fehlerGefunden) {
                jTBestellnummer.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!jahrVerifier.verify(jTJahrgang)) {
            jLUngueltigJahrgang.setVisible(true);
            jLHilfeJahrgang.setVisible(true);
            if (!fehlerGefunden) {
                jTJahrgang.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jTName.getText().equals("")) {
            jLAusfuellenName.setVisible(true);
            if (!fehlerGefunden) {
                jTName.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jCAnbaugebiet.getSelectedIndex() == 0) {
            jLAuswaehlenAnbaugebiet.setVisible(true);
            if (!fehlerGefunden) {
                jCAnbaugebiet.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (jCRegion.getSelectedIndex() == 0) {
            jLRegionAuswaehlen.setVisible(true);
            if (!fehlerGefunden) {
                jLRegionAuswaehlen.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!lagerdauerVerifier.verify(jTLagerfaehigkeit)) {
            jLUngueltigLagerfaehigkeit.setVisible(true);
            jLHilfeLagerfaehigkeit.setVisible(true);
            if (!fehlerGefunden) {
                jTLagerfaehigkeit.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!preisGueltig(jTFlaschenpreis)) {
            jLUngueltigFlaschenpreis.setVisible(true);
            if (!fehlerGefunden) {
                jTFlaschenpreis.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (!preisGueltig(jTLiterpreis)) {
            jLUngueltigLiterpreis.setVisible(true);
            if (!fehlerGefunden) {
                jTLiterpreis.requestFocusInWindow();
            }
            fehlerGefunden = true;
        }
        if (comboboxDoppelt()) {
            jLComboboxDoppelt.setVisible(true);
        }
        if (fehlerGefunden) {
            JOptionPane.showMessageDialog(null, ERROR_MESSAGE, ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        } else {

            if (jRWeiss.isSelected()) {
                farbe = "weiss";
            } else if (jRRot.isSelected()) {
                farbe = "rot";
            } else {
                farbe = "rose";
            }
            if (jCRebsorte1.getSelectedIndex() != 0) {
                rebsorten[0] = jCRebsorte1.getSelectedItem().toString();
            }
            if (jCRebsorte2.getSelectedIndex() != 0) {
                rebsorten[1] = jCRebsorte2.getSelectedItem().toString();
            }
            if (jCRebsorte3.getSelectedIndex() != 0) {
                rebsorten[2] = jCRebsorte3.getSelectedItem().toString();
            }
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            double preis = 0;
            double flaschengroesse = 0;
            double alkoholgehalt = 0;
            try {
                preis = nf.parse(jTFlaschenpreis.getText()).doubleValue();
                flaschengroesse = nf.parse(jCFlaschengroesse.getItemAt(jCFlaschengroesse.getSelectedIndex()).toString()).doubleValue();
                alkoholgehalt = nf.parse(jCAlkoholgehalt.getItemAt(jCAlkoholgehalt.getSelectedIndex()).toString()).doubleValue();
            } catch (ParseException e) {

            }

            Wein neuerWein = new Wein(jTBestellnummer.getText(), Integer.parseInt(jTJahrgang.getText()),
                    jTName.getText(), farbe, rebsorten, jCAnbaugebiet.getSelectedItem().toString(),
                    alkoholgehalt,
                    Integer.parseInt(jTLagerfaehigkeit.getText()),
                    flaschengroesse,
                    preis);
            weinreifeDiagramm.setzeLagerdauer(neuerWein.getLagerdauer());
            weinreifeDiagramm.setzeJahrgang(neuerWein.getJahrgang());
            jCLagerdauer.setSelectedIndex(neuerWein.getLagerdauer() - 1);
            if (weinDaten == null) {
                weinDaten = new WeinDaten();
            }
            weinDaten.weinDatenHinzufuegn(neuerWein);
            System.out.println(neuerWein.toString());
            bestellNr++;
            resetInternalFrameWein();
            if (weinDaten != null && !speichernMenuItem.isEnabled() && !speichernUnterMenuItem.isEnabled()) {
                speichernMenuItem.setEnabled(true);
                speichernUnterMenuItem.setEnabled(true);
            } else if (weinDaten == null && speichernMenuItem.isEnabled() && speichernUnterMenuItem.isEnabled()) {
                speichernMenuItem.setEnabled(false);
                speichernUnterMenuItem.setEnabled(false);
            }
        }
    }//GEN-LAST:event_jBSpeichernActionPerformed

    private void jCAnbaugebietItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCAnbaugebietItemStateChanged
        if (!firstRun) {
            String anbaugebiet = jCAnbaugebiet.getSelectedItem().toString();
            fuelleJCRegion(anbaugebiet);
            jLRegionAuswaehlen.setVisible(false);
            if (jCAnbaugebiet.getSelectedIndex() == 0) {
                jLAuswaehlenAnbaugebiet.setVisible(true);
            } else if (jLAuswaehlenAnbaugebiet.isVisible()) {
                jLAuswaehlenAnbaugebiet.setVisible(false);
            }
        }
    }//GEN-LAST:event_jCAnbaugebietItemStateChanged

    private void jLHilfeBestellNrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeBestellNrMouseClicked
        JOptionPane.showMessageDialog(null, BESTELLNR_MESSAGE, BESTELLNR_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeBestellNrMouseClicked

    private void jLHilfeJahrgangMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeJahrgangMousePressed
        JOptionPane.showMessageDialog(null, JAHR_MESSAGE, JAHR_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeJahrgangMousePressed

    private void jTJahrgangFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTJahrgangFocusGained
        jTJahrgang.selectAll();
    }//GEN-LAST:event_jTJahrgangFocusGained

    private void jTJahrgangFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTJahrgangFocusLost
        jTJahrgang.select(0, 0);
        if (jahrVerifier.verify(jTJahrgang)) {
            weinreifeDiagramm.setzeJahrgang(Integer.parseInt(jTJahrgang.getText()));
            if (jLUngueltigJahrgang.isVisible() && jLHilfeJahrgang.isVisible()) {
                jLUngueltigJahrgang.setVisible(false);
                jLHilfeJahrgang.setVisible(false);
            }
        } else {
            jLUngueltigJahrgang.setVisible(true);
            jLHilfeJahrgang.setVisible(true);
        }
        if (jTJahrgang.getText().equals("")) {
            weinreifeDiagramm.setzeJahrgang(0);
        } else if (weinreifeDiagramm.lagerdauer != 0) {
            jTLagerfaehigkeit.setText("" + (Integer.parseInt(jTJahrgang.getText())
                    + weinreifeDiagramm.lagerdauer));
        }
    }//GEN-LAST:event_jTJahrgangFocusLost

    private void jTNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTNameFocusGained
        jTName.selectAll();
    }//GEN-LAST:event_jTNameFocusGained

    private void jTNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTNameFocusLost
        jTName.select(0, 0);
        if (jTName.getText().equals("")) {
            jLAusfuellenName.setVisible(true);
        } else if (jLAusfuellenName.isVisible()) {
            jLAusfuellenName.setVisible(false);
        }
    }//GEN-LAST:event_jTNameFocusLost

    private void jLHilfeLagerfaehigkeitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeLagerfaehigkeitMouseClicked
        JOptionPane.showMessageDialog(null, LAGERDAUER_MESSAGE, LAGERDAUER_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeLagerfaehigkeitMouseClicked

    private void jTLagerfaehigkeitFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTLagerfaehigkeitFocusGained
        jTLagerfaehigkeit.selectAll();
    }//GEN-LAST:event_jTLagerfaehigkeitFocusGained

    private void jTFlaschenpreisFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFlaschenpreisFocusLost
        boolean gueltig = preisGueltig(jTFlaschenpreis);
        jTFlaschenpreis.select(0, 0);
        if (!gueltig) {
            jLUngueltigFlaschenpreis.setVisible(true);
        } else if (jLUngueltigFlaschenpreis.isVisible()) {
            jLUngueltigFlaschenpreis.setVisible(false);
        }
//        if (jTFlaschenpreis.getText().toString().isEmpty()) {
//            jLUngueltigFlaschenpreis.setVisible(true);
//        } else if (jLUngueltigFlaschenpreis.isVisible()) {
//            jLUngueltigFlaschenpreis.setVisible(false);
//        }
    }//GEN-LAST:event_jTFlaschenpreisFocusLost

    private void jTFlaschenpreisFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTFlaschenpreisFocusGained
        jTFlaschenpreis.selectAll();
    }//GEN-LAST:event_jTFlaschenpreisFocusGained

    private void jIFweinAufnehmenInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_jIFweinAufnehmenInternalFrameClosing
        schliesseWeinInternalFrame();
    }//GEN-LAST:event_jIFweinAufnehmenInternalFrameClosing

    private void jCAnbaugebietActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCAnbaugebietActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCAnbaugebietActionPerformed

    private void jCAlkoholgehaltActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCAlkoholgehaltActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCAlkoholgehaltActionPerformed

    private void jCFlaschengroesseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCFlaschengroesseActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
            buttonLiter = true;
            calculateToLiter();
//            jTFlaschenpreis.requestFocusInWindow();
//            jTFlaschenpreis.selectAll();
        }
    }//GEN-LAST:event_jCFlaschengroesseActionPerformed

    private void jCRebsorte1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRebsorte1ActionPerformed
        if (!firstRun) {
            if (jCRebsorte1.getSelectedIndex() != 0 && jCRebsorte1.isEnabled()) {
                jCRebsorte2.setEnabled(true);
            } else {
                jCRebsorte2.setEnabled(false);
                jCRebsorte3.setEnabled(false);
                jCRebsorte2.setSelectedIndex(0);
                jCRebsorte3.setSelectedIndex(0);
            }
        }
    }//GEN-LAST:event_jCRebsorte1ActionPerformed

    private void jCRebsorte2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRebsorte2ActionPerformed
        if (!firstRun) {
            if (jCRebsorte2.getSelectedIndex() != 0 && jCRebsorte1.isEnabled()) {
                if (jCRebsorte2.getSelectedIndex() == jCRebsorte1.getSelectedIndex()) {
                    jLComboboxDoppelt.setVisible(true);
                } else {
                    jLComboboxDoppelt.setVisible(false);
                }
                jCRebsorte3.setEnabled(true);
            } else {
                jCRebsorte3.setEnabled(false);
                jCRebsorte3.setSelectedIndex(0);
            }
        }
    }//GEN-LAST:event_jCRebsorte2ActionPerformed

    private void jCLagerdauerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCLagerdauerActionPerformed
        weinreifeDiagramm.lagerdauer = jCLagerdauer.getSelectedIndex() + 1;
        if (!jTJahrgang.getText().equals("")) {
            if (jCLagerdauer.getSelectedIndex() > -1) {
                jTLagerfaehigkeit.setText("" + (weinreifeDiagramm.lagerdauer + Integer.parseInt(jTJahrgang.getText())));
                if (jLUngueltigLagerfaehigkeit.isVisible() && jLHilfeLagerfaehigkeit.isVisible()) {
                    jLUngueltigLagerfaehigkeit.setVisible(false);
                    jLHilfeLagerfaehigkeit.setVisible(false);
                }
            }
        }
        repaint();
    }//GEN-LAST:event_jCLagerdauerActionPerformed

    private void weinreifeDiagrammPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_weinreifeDiagrammPropertyChange
        if (evt.getPropertyName().equals("JCLagerdauer")) {
            jCLagerdauer.setSelectedIndex((Integer) evt.getNewValue() - 1);
        }
    }//GEN-LAST:event_weinreifeDiagrammPropertyChange

    private void jCRegionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRegionActionPerformed
        if (!firstRun) {
            comboBoxChanged = true;
        }
    }//GEN-LAST:event_jCRegionActionPerformed

    private void jCRegionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCRegionItemStateChanged
        if (!firstRun) {
            if (jCRegion.getSelectedIndex() < 1) {
                jLRegionAuswaehlen.setVisible(true);
            } else if (jLRegionAuswaehlen.isVisible()) {
                jLRegionAuswaehlen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jCRegionItemStateChanged

    private void jCRebsorte3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCRebsorte3ActionPerformed
        if (!firstRun && jCRebsorte3.getSelectedIndex() != 0) {
            if (jCRebsorte3.getSelectedIndex() == jCRebsorte1.getSelectedIndex()
                    || jCRebsorte3.getSelectedIndex() == jCRebsorte2.getSelectedIndex()) {
                jLComboboxDoppelt.setVisible(true);
            } else {
                jLComboboxDoppelt.setVisible(false);
            }

        }
    }//GEN-LAST:event_jCRebsorte3ActionPerformed

    private void jTLagerfaehigkeitFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTLagerfaehigkeitFocusLost

        if (!jTJahrgang.getText().equals("") && !jTLagerfaehigkeit.getText().equals("")) {
            int lagerdauer = Integer.parseInt(jTLagerfaehigkeit.getText())
                    - Integer.parseInt(jTJahrgang.getText());
            if (lagerdauer < 1 || lagerdauer > 25) {
                jLHilfeLagerfaehigkeit.setVisible(true);
                jLUngueltigLagerfaehigkeit.setVisible(true);
                jCLagerdauer.setSelectedIndex(-1);
            } else {
                weinreifeDiagramm.setzeLagerdauer(lagerdauer);
                jCLagerdauer.setSelectedIndex(lagerdauer - 1);
                if (jLUngueltigLagerfaehigkeit.isVisible() && jLHilfeLagerfaehigkeit.isVisible()) {
                    jLUngueltigLagerfaehigkeit.setVisible(false);
                    jLHilfeLagerfaehigkeit.setVisible(false);
                }
            }
        } else {
            jLHilfeLagerfaehigkeit.setVisible(true);
            jLUngueltigLagerfaehigkeit.setVisible(true);
        }
    }//GEN-LAST:event_jTLagerfaehigkeitFocusLost

    private void jCAnbaugebietFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCAnbaugebietFocusLost
        if (!firstRun) {
            if (jCAnbaugebiet.getSelectedIndex() == 0) {
                jLAuswaehlenAnbaugebiet.setVisible(true);
            } else if (jLAuswaehlenAnbaugebiet.isVisible()) {
                jLAuswaehlenAnbaugebiet.setVisible(false);
            }
        }
    }//GEN-LAST:event_jCAnbaugebietFocusLost

    private void jCRegionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCRegionFocusLost
        if (!firstRun) {
            if (jCRegion.getSelectedIndex() == 0) {
                jLRegionAuswaehlen.setVisible(true);
            } else if (jLRegionAuswaehlen.isVisible()) {
                jLRegionAuswaehlen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jCRegionFocusLost

    private void jBFlaschenpreisInLiterpreisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBFlaschenpreisInLiterpreisActionPerformed
        buttonLiter = true;
        calculateToLiter();
    }//GEN-LAST:event_jBFlaschenpreisInLiterpreisActionPerformed

    private void jBLiterpreisInFlaschenpreisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLiterpreisInFlaschenpreisActionPerformed
        buttonBottle = true;
        calculateToBottle();
    }//GEN-LAST:event_jBLiterpreisInFlaschenpreisActionPerformed

    private void jTLiterpreisFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTLiterpreisFocusGained
        jTLiterpreis.selectAll();
    }//GEN-LAST:event_jTLiterpreisFocusGained

    private void jTLiterpreisFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTLiterpreisFocusLost
//        jTLiterpreis.select(0, 0);
//
//        String regexKomma = "(\\d*,?\\d*)|(\\d{0,3}(\\.\\d{3})*,?\\d*)";
//
//        if (!jTLiterpreis.getText().matches(regexKomma)) {
//            JOptionPane.showMessageDialog(null, TEXT_MESSAGE, TEXT_TITLE, JOptionPane.WARNING_MESSAGE);
//            jTLiterpreis.setText("");
//            jTLiterpreis.requestFocusInWindow();
//        }

        boolean gueltig = preisGueltig(jTLiterpreis);
        jTLiterpreis.select(0, 0);
        if (!gueltig) {
            jLUngueltigLiterpreis.setVisible(true);
        } else if (jLUngueltigFlaschenpreis.isVisible()) {
            jLUngueltigLiterpreis.setVisible(false);
        }
//        if (jTLiterpreis.getText().toString().isEmpty()) {
//            jLUngueltigLiterpreis.setVisible(true);
//        } else if (jLUngueltigFlaschenpreis.isVisible()) {
//            jLUngueltigLiterpreis.setVisible(false);
//        }

    }//GEN-LAST:event_jTLiterpreisFocusLost

    private void jTLiterpreisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTLiterpreisKeyPressed
        if (evt.getExtendedKeyCode() == KeyEvent.VK_ENTER && zahlenVerifier.shouldYieldFocus(jTLiterpreis)) {
            buttonBottle = true;
            calculateToBottle();
        }
    }//GEN-LAST:event_jTLiterpreisKeyPressed

    private void jTFlaschenpreisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFlaschenpreisKeyPressed
        if (evt.getExtendedKeyCode() == KeyEvent.VK_ENTER && zahlenVerifier.shouldYieldFocus(jTFlaschenpreis)) {
            buttonBottle = true;
            calculateToLiter();
        }
    }//GEN-LAST:event_jTFlaschenpreisKeyPressed

    private void kundenAufnehmenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kundenAufnehmenMenuItemActionPerformed
        jIFKundeAufnehmen.setVisible(true);
    }//GEN-LAST:event_kundenAufnehmenMenuItemActionPerformed

    private void jTKundenNrFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTKundenNrFocusLost
        if (!kundennummerVerifier.verify(jTKundenNr)) {
            jLUngueltigKundenNr.setVisible(true);
            jLHilfeKundenNr.setVisible(true);
        } else if (jLUngueltigKundenNr.isVisible() && jLHilfeKundenNr.isVisible()) {
            jLUngueltigKundenNr.setVisible(false);
            jLHilfeKundenNr.setVisible(false);
        }
        if (kundennummerVerifier.verify(jTKundenNr) && jTKundenNr.getBackground().equals(LEER)) {
            jTKundenNr.setBackground(Color.WHITE);
            leereFelder.remove(jTKundenNr);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTKundenNrFocusLost

    private void jLHilfeKundenNrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeKundenNrMouseClicked
        JOptionPane.showMessageDialog(null, KUNDENNR_MESSAGE, KUNDENNR_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeKundenNrMouseClicked

    private void jBAbbrechenKundeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAbbrechenKundeActionPerformed
        schliesseKundenInternalFrame();
    }//GEN-LAST:event_jBAbbrechenKundeActionPerformed

    private void jLHilfePLZMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfePLZMouseClicked
        JOptionPane.showMessageDialog(null, PLZ_MESSAGE, PLZ_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfePLZMouseClicked

    private void jTPLZFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTPLZFocusLost
        if (!kundennummerVerifier.verify(jTPLZ)) {
            jLUngueltigPLZ.setVisible(true);
            jLHilfePLZ.setVisible(true);
        } else if (jLUngueltigPLZ.isVisible() && jLHilfePLZ.isVisible()) {
            jLUngueltigPLZ.setVisible(false);
            jLHilfePLZ.setVisible(false);
        }
        if (kundennummerVerifier.verify(jTPLZ) && jTPLZ.getBackground().equals(LEER)) {
            jTPLZ.setBackground(Color.WHITE);
            leereFelder.remove(jTPLZ);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTPLZFocusLost

    private void jTVornameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTVornameFocusLost
        if (!namenVerifier.verify(jTVorname)) {
            jLUngueltigVorname.setVisible(true);
            jLHilfeVorname.setVisible(true);
        } else if (jLUngueltigVorname.isVisible() && jLHilfeVorname.isVisible()) {
            jLUngueltigVorname.setVisible(false);
            jLHilfeVorname.setVisible(false);
        }
        if (namenVerifier.verify(jTVorname) && jTVorname.getBackground().equals(LEER)) {
            jTVorname.setBackground(Color.WHITE);
            leereFelder.remove(jTVorname);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTVornameFocusLost

    private void jTNachnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTNachnameFocusLost
        if (!namenVerifier.verify(jTNachname)) {
            jLUngueltigNachname.setVisible(true);
            jLHilfeNachname.setVisible(true);
        } else if (jLUngueltigNachname.isVisible()) {
            jLUngueltigNachname.setVisible(false);
            jLHilfeNachname.setVisible(false);
        }
        if (namenVerifier.verify(jTNachname) && jTNachname.getBackground().equals(LEER)) {
            jTNachname.setBackground(Color.WHITE);
            leereFelder.remove(jTNachname);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTNachnameFocusLost

    private void jLHilfeVornameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeVornameMouseClicked
        JOptionPane.showMessageDialog(null, NAME_MESSAGE, NAME_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeVornameMouseClicked

    private void jLHilfeNachnameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeNachnameMouseClicked
        JOptionPane.showMessageDialog(null, NAME_MESSAGE, NAME_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeNachnameMouseClicked

    private void jLHilfeKontoinhaberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLHilfeKontoinhaberMouseClicked
        JOptionPane.showMessageDialog(null, NAME_MESSAGE, NAME_TITLE, JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLHilfeKontoinhaberMouseClicked

    private void jTKontoinhaberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTKontoinhaberFocusLost
        if (!namenVerifier.verify(jTKontoinhaber)) {
            jLUngueltigKontoinhaber.setVisible(true);
            jLHilfeKontoinhaber.setVisible(true);
        } else if (jLUngueltigKontoinhaber.isVisible()) {
            jLUngueltigKontoinhaber.setVisible(false);
            jLHilfeKontoinhaber.setVisible(false);
        }
        if (namenVerifier.verify(jTKontoinhaber) && jTKontoinhaber.getBackground().equals(LEER)) {
            jTKontoinhaber.setBackground(Color.WHITE);
            leereFelder.remove(jTKontoinhaber);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTKontoinhaberFocusLost

    private void jBSpeichernKundeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSpeichernKundeActionPerformed
        felderLeer();
        if (leereFelder.isEmpty()) {
            String anrede;
            if (jRHerr.isSelected()) {
                anrede = jRHerr.getText();
            } else {
                anrede = jRFrau.getText();
            }
            Kunde kunde = new Kunde(jTKundenNr.getText(), anrede,
                    jTVorname.getText(), jTNachname.getText(),
                    jTStrasse.getText(), jTHausnummer.getText(),
                    jTPLZ.getText(), jTOrt.getText(), jTTelefonnummer.getText(),
                    jTKontoinhaber.getText(), jTKontoNr.getText(),
                    jTBankleitzahl.getText(), jTKreditinstitut.getText());
            if (kundenDaten == null) {
                kundenDaten = new KundenDaten();
            }
            kundenDaten.kundeHinzufuegen(kunde);
            System.out.println(kunde.toString());
            kundenNr++;
            resetInternalFrameKunde();
            if (kundenDaten != null && !speichernMenuItem.isEnabled() && !speichernUnterMenuItem.isEnabled()) {
                speichernMenuItem.setEnabled(true);
                speichernUnterMenuItem.setEnabled(true);
            } else if (kundenDaten == null && speichernMenuItem.isEnabled() && speichernUnterMenuItem.isEnabled()) {
                speichernMenuItem.setEnabled(false);
                speichernUnterMenuItem.setEnabled(false);
            }
        }
    }//GEN-LAST:event_jBSpeichernKundeActionPerformed

    private void jTTelefonnummerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTTelefonnummerFocusLost
        if (!jTTelefonnummer.getText().equals("") && jTTelefonnummer.getBackground().equals(LEER)) {
            jTTelefonnummer.setBackground(Color.WHITE);
            leereFelder.remove(jTTelefonnummer);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTTelefonnummerFocusLost

    private void jTStrasseFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTStrasseFocusLost
        if (!jTStrasse.getText().equals("") && jTStrasse.getBackground().equals(LEER)) {
            jTStrasse.setBackground(Color.WHITE);
            leereFelder.remove(jTStrasse);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTStrasseFocusLost

    private void jTHausnummerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTHausnummerFocusLost
        if (!jTHausnummer.getText().equals("") && jTHausnummer.getBackground().equals(LEER)) {
            jTHausnummer.setBackground(Color.WHITE);
            leereFelder.remove(jTHausnummer);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTHausnummerFocusLost

    private void jTOrtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTOrtFocusLost
        if (!jTOrt.getText().equals("") && jTOrt.getBackground().equals(LEER)) {
            jTOrt.setBackground(Color.WHITE);
            leereFelder.remove(jTOrt);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTOrtFocusLost

    private void jTKontoNrFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTKontoNrFocusLost
        if (!jTKontoNr.getText().equals("") && jTKontoNr.getBackground().equals(LEER)) {
            jTKontoNr.setBackground(Color.WHITE);
            leereFelder.remove(jTKontoNr);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }

        }
    }//GEN-LAST:event_jTKontoNrFocusLost

    private void jTBankleitzahlFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTBankleitzahlFocusLost
        if (!jTBankleitzahl.getText().equals("") && jTBankleitzahl.getBackground().equals(LEER)) {
            jTBankleitzahl.setBackground(Color.WHITE);
            leereFelder.remove(jTBankleitzahl);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTBankleitzahlFocusLost

    private void jTKreditinstitutFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTKreditinstitutFocusLost
        if (!jTKreditinstitut.getText().equals("") && jTKreditinstitut.getBackground().equals(LEER)) {
            jTKreditinstitut.setBackground(Color.WHITE);
            leereFelder.remove(jTKreditinstitut);
            if (leereFelder.isEmpty()) {
                jLAusfuellen.setVisible(false);
            }
        }
    }//GEN-LAST:event_jTKreditinstitutFocusLost

    private void speichernMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_speichernMenuItemActionPerformed
        if (aktuelleDatei != null) {
            speichern(aktuelleDatei);
        } else {
            speichernUnterMenuItemActionPerformed(evt);
        }
    }//GEN-LAST:event_speichernMenuItemActionPerformed

    private void oeffnenMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oeffnenMenuItemActionPerformed
        boolean dateiAusgewaehlt = true;
        File gewaehlt = null;
        Kunde kunde = null;
        Wein wein = null;

        int antwort = dateiAuswahl.showOpenDialog(this);
        if (antwort == JFileChooser.APPROVE_OPTION) {
            gewaehlt = dateiAuswahl.getSelectedFile();
            System.out.println("Ausgewählt: " + gewaehlt.getName());
        } else {
            dateiAusgewaehlt = false;
            System.out.println("Ausgewählt: nichts");
        }

        if (dateiAusgewaehlt) {
            boolean abbrechen = false;
            if (weinFormularVeraendert() || kundenFormularVeraendert()) {
                int speichern = JOptionPane.showConfirmDialog(this, SPEICHERN_MESSAGE, SPEICHERN_TITLE, JOptionPane.YES_NO_CANCEL_OPTION);
                if (speichern == JOptionPane.YES_OPTION) {
                    speichern(aktuelleDatei);
                } else {
                    abbrechen = speichern == JOptionPane.CANCEL_OPTION;
                }
            }

            if (!abbrechen) {
                boolean fehler = false;
                aktuelleDatei = null;
                weinDaten = new WeinDaten();
                kundenDaten = new KundenDaten();
                int anzahlWerte = 0;
                int anzahlFehler = 0;
                String zeile;
                try {
                    BufferedReader eingabe = new BufferedReader(new FileReader(gewaehlt));
                    while (eingabe.ready()) {
                        zeile = eingabe.readLine();
                        String[] daten = zeile.split(TRENNER);

                        if (daten[0].equals("Kunde")) {
                            daten = zeile.split(TRENNER);
                            try {
                                kundenDaten.kundeHinzufuegen(Kunde.valueOf(daten));
                                anzahlWerte++;
                            } catch (IllegalArgumentException ke) {
                                anzahlFehler++;
                                System.out.println(ke.getMessage());
                            }
                            kundenNr = Integer.parseInt(kundenDaten
                                    .getKundenDaten().get(kundenDaten
                                            .getKundenDaten().size() - 1)
                                    .getKundenNummer()) + 1;
                        }
                        if (daten[0].equals("Wein")) {
                            try {
                                weinDaten.weinDatenHinzufuegn(Wein
                                        .valueOf(daten));
                                anzahlWerte++;
                            } catch (IllegalArgumentException we) {
                                anzahlFehler++;
                                System.out.println(we.getMessage());
                            }
                        }
                        bestellNr = Integer.parseInt(weinDaten.getWeinDaten()
                                .get(weinDaten.getWeinDaten().size() - 1)
                                .getBestellnummer().substring(2, 3)) + 1;

                    }
                    eingabe.close();
                    resetInternalFrameKunde();
                    resetInternalFrameWein();
                    if ((weinDaten != null || kundenDaten != null)
                            && !speichernMenuItem.isEnabled()
                            && !speichernUnterMenuItem.isEnabled()) {
                        speichernMenuItem.setEnabled(true);
                        speichernUnterMenuItem.setEnabled(true);
                    } else if (weinDaten == null && kundenDaten == null
                            && speichernMenuItem.isEnabled()
                            && speichernUnterMenuItem.isEnabled()) {
                        speichernMenuItem.setEnabled(false);
                        speichernUnterMenuItem.setEnabled(false);
                    }
                } catch (FileNotFoundException fe) {
                    JOptionPane.showMessageDialog(this, KEINE_DATEI_GEFUNDEN_MESSAGE,
                            KEINE_DATEI_GEFUNDEN_TITLE, JOptionPane.ERROR_MESSAGE);
                    fehler = true;
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(this, FEHLER_LESEN_MESSAGE,
                            FEHLER_LESEN_TITLE, JOptionPane.ERROR_MESSAGE);
                    fehler = true;
                }

                if (!fehler) {
                    if (anzahlWerte == 0) {
                        JOptionPane.showMessageDialog(this, NICHTS_EINGELESEN_MESSAGE,
                                NICHTS_EINGELESEN_TITLE, JOptionPane.INFORMATION_MESSAGE);
                    } else if (anzahlFehler == 0) {
                        JOptionPane.showMessageDialog(this, ALLES_OK_MESSAGE, ALLES_OK_TITLE,
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(this, FEHELER_BEIM_LESEN_MESSAGE,
                                FEHLER_BEIM_LESEN_TITLE, JOptionPane.ERROR_MESSAGE);
                    }
                    aktuelleDatei = gewaehlt;
                    System.out.println(weinDaten.toString());
                    System.out.println(kundenDaten.toString());
                }
            }
        }

    }//GEN-LAST:event_oeffnenMenuItemActionPerformed

    private void speichernUnterMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_speichernUnterMenuItemActionPerformed
        int antwort = dateiAuswahl.showSaveDialog(this);
        if (antwort == JFileChooser.APPROVE_OPTION) {
            speichern(dateiAuswahl.getSelectedFile());
        }
    }//GEN-LAST:event_speichernUnterMenuItemActionPerformed

    private void jCUebernehmenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCUebernehmenActionPerformed
        if (jCUebernehmen.isSelected()) {
            if (!jTVorname.getText().equals("") && !jTNachname.getText().equals("")) {
                jTKontoinhaber.setEnabled(false);
                jTKontoinhaber.setText(jTVorname.getText() + " " + jTNachname.getText());
                if (jLUngueltigKontoinhaber.isEnabled() && jLHilfeKontoinhaber.isEnabled()) {
                    jLUngueltigKontoinhaber.setVisible(false);
                    jLHilfeKontoinhaber.setVisible(false);
                }
            } else {
                jCUebernehmen.setSelected(false);
                JOptionPane.showMessageDialog(null, NAME_EINGEBEN_MESSAGE, NAME_EINGEBEN_TITLE, JOptionPane.INFORMATION_MESSAGE);
                if (jTVorname.getText().equals("")) {
                    jTVorname.requestFocusInWindow();
                } else if (jTNachname.getText().equals("")) {
                    jTNachname.requestFocusInWindow();
                }
            }
        } else {
            jTKontoinhaber.setText("");
            jTKontoinhaber.setEnabled(true);
            jTKontoinhaber.requestFocusInWindow();
        }
    }//GEN-LAST:event_jCUebernehmenActionPerformed

    private void jIFKundeAufnehmenInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_jIFKundeAufnehmenInternalFrameClosing
        schliesseKundenInternalFrame();
    }//GEN-LAST:event_jIFKundeAufnehmenInternalFrameClosing

    /**
     * Es wird eine Eingabe in einem Literpreis pro Flasche umgerechnet und in
     * einem Textfield ausgegeben.
     *
     * Dazu wird die Eingabe formatiert und an einem Eingabeverifizierer
     * weitergeleitet, der prüft ob die Eingabe gültig ist.
     */
    private void calculateToLiter() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        String stringPreis = jTFlaschenpreis.getText().toString();
        try {
            double flaschengroesse = nf.parse(jCFlaschengroesse.getItemAt(
                    jCFlaschengroesse.getSelectedIndex()).toString()).doubleValue();
            double preis = nf.parse(stringPreis).doubleValue();

            // Literpreis wird berechnet.
            jTLiterpreis.setText(nf.format(preis / flaschengroesse));
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, TEXT_MESSAGE, TEXT_TITLE,
                    JOptionPane.WARNING_MESSAGE);
            jTFlaschenpreis.setText("");
            jTFlaschenpreis.requestFocusInWindow();
        }
        buttonLiter = false;
    }

    /**
     * Es wird eine Eingabe in einen Flaschenpeis pro Liter umgerechnet und in
     * einem Textfield ausgegeben.
     *
     * Dazu wird die Eingabe formatiert und an einem Eingabeverifizierer
     * weitergeleitet, der prüft ob die Eingabe gültig ist.
     */
    private void calculateToBottle() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);

        String stringPreis = jTLiterpreis.getText().toString();
        try {
            double flaschengroesse = nf.parse(jCFlaschengroesse.getItemAt(
                    jCFlaschengroesse.getSelectedIndex()).toString()).doubleValue();
            double preis = nf.parse(stringPreis).doubleValue();

            // Literpreis wird berechnet.
            jTFlaschenpreis.setText(nf.format(preis * flaschengroesse));
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, TEXT_MESSAGE, TEXT_TITLE,
                    JOptionPane.WARNING_MESSAGE);
            jTLiterpreis.setText("");
            jTLiterpreis.requestFocusInWindow();
        }
        buttonBottle = false;
    }

    /**
     * Prüft ob die ausgewählten Einträge der Comboboxen doppelt sind.
     *
     * @return boolean
     */
    private boolean comboboxDoppelt() {

        return jCRebsorte2.getSelectedIndex() != 0 && jCRebsorte1.getSelectedIndex() == jCRebsorte2.getSelectedIndex()
                || jCRebsorte3.getSelectedIndex() != 0 && jCRebsorte1.getSelectedIndex() == jCRebsorte3.getSelectedIndex()
                || jCRebsorte3.getSelectedIndex() != 0 && jCRebsorte2.getSelectedIndex() == jCRebsorte3.getSelectedIndex();
    }

    /**
     * Setzt die Fokusreihenfolge fest.
     *
     * @return ArrayList<JComponent> - Fokusreihenfolge
     */
    private ArrayList<JComponent> getOrder() {
        ArrayList<JComponent> order = new ArrayList<>();
        order.add(jTBestellnummer);
//        order.add(jLHilfeBestellNr);
        order.add(jTJahrgang);
//        order.add(jLHilfeJahrgang);
        order.add(jTName);
        order.add(jRWeiss);
        order.add(jRRot);
        order.add(jRRose);
        order.add(jCAnbaugebiet);
        order.add(jCRegion);
        order.add(jCRebsorte1);
        order.add(jCRebsorte2);
        order.add(jCRebsorte3);
        order.add(jCAlkoholgehalt);
        order.add(jTLagerfaehigkeit);
//        order.add(jLHilfeLagerfaehigkeit);
        order.add(jCFlaschengroesse);
        order.add(jTFlaschenpreis);
        order.add(jBFlaschenpreisInLiterpreis);
        order.add(jTLiterpreis);
        order.add(jBLiterpreisInFlaschenpreis);
        order.add(jBSpeichern);
        order.add(jBAbbrechen);
        return order;
    }

    /**
     * InternalFrame wird resetet und unsichtbar gemacht.
     */
    private void schliesseWeinInternalFrame() {
        if (weinFormularVeraendert()) {
            int abbrechen = JOptionPane.showConfirmDialog(null, ABORT_MESSAGE,
                    ABORT_TITLE, JOptionPane.YES_NO_OPTION);

            if (abbrechen == 0) {
                jIFweinAufnehmen.setVisible(false);
                resetInternalFrameWein();
                resetVisibilityWein();
            }
        } else {
            jIFweinAufnehmen.setVisible(false);
            resetInternalFrameWein();
            resetVisibilityWein();
        }
    }

    /**
     * Prüft ob das Formular bearbeitet wurde.
     *
     * @return boolean
     */
    private boolean weinFormularVeraendert() {
        boolean veraendert = false;

        for (JComponent c : PFLICHTFELDER_WEIN) {
            if (!veraendert && c instanceof JTextField) {
                veraendert = !((JTextField) c).getText().isEmpty();
            } else if (!veraendert && c instanceof JComboBox) {
                veraendert = comboBoxChanged;
            } else if (!veraendert && c instanceof JRadioButton) {
                veraendert = !((JRadioButton) c).isSelected();
            }
        }

        return veraendert;
    }

    /**
     * Resettet das InternalFrame.
     */
    private void resetInternalFrameWein() {
        jTJahrgang.setText("");
        jTName.setText("");
        jRWeiss.setSelected(true);
        jCAnbaugebiet.setSelectedIndex(0);
        jCRebsorte1.setSelectedIndex(0);
        jCRebsorte2.setSelectedIndex(0);
        jCRebsorte3.setSelectedIndex(0);
        jCAlkoholgehalt.setSelectedIndex(6);
        jTLagerfaehigkeit.setText("");
        jCFlaschengroesse.setSelectedIndex(5);
        jTFlaschenpreis.setText("0,00");
        jTLiterpreis.setText("0,00");
        comboBoxChanged = false;
        resetVisibilityWein();
        weinreifeDiagramm.reset();
        jTBestellnummer.setText("aa0" + bestellNr);
        int aktuellesJahr = new GregorianCalendar().get(GregorianCalendar.YEAR);
        jTJahrgang.setText("" + aktuellesJahr);
        weinreifeDiagramm.setzeJahrgang(aktuellesJahr);
        jTBestellnummer.setText("aa0" + bestellNr);
    }

    /**
     * Formatiert das Textfield in deutscher Euro schreibweise.
     */
    private boolean preisGueltig(JTextField textfield) {
        boolean preisGueltig = true;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        try {
            textfield.setText(nf.format(nf.parse(textfield.getText()).doubleValue()));
        } catch (ParseException e) {
            preisGueltig = false;
        }
        return preisGueltig;
    }

    /**
     * Macht alle Fehlerhinweise unsichtbar.
     */
    private void resetVisibilityWein() {
        jLUngueltigBestellNr.setVisible(false);
        jLUngueltigJahrgang.setVisible(false);
        jLHilfeBestellNr.setVisible(false);
        jLHilfeJahrgang.setVisible(false);
        jLAusfuellenName.setVisible(false);
        jLAuswaehlenAnbaugebiet.setVisible(false);
        jLHilfeLagerfaehigkeit.setVisible(false);
        jLUngueltigLagerfaehigkeit.setVisible(false);
        jLUngueltigFlaschenpreis.setVisible(false);
        jLComboboxDoppelt.setVisible(false);
        jLRegionAuswaehlen.setVisible(false);
        jLUngueltigLiterpreis.setVisible(false);
    }

    /**
     * Fragt nach ob das Programm wirklich beendet werden sollen und beendet ggf
     * das Programm.
     */
    private void beenden() {
        int beenden = JOptionPane.showConfirmDialog(null, CLOSE_MASSAGE, CLOSE_TITLE, JOptionPane.YES_NO_OPTION);
        if (beenden == 0) {
            System.exit(0);
        }
    }

    /**
     * Füllt eine Combobox mit Ländern.
     *
     * @param laender Strin[]
     */
    private void fuelleLaender(String[] laender) {
        jCAnbaugebiet.addItem("Bitte Land auswählen");
        for (int i = 0; i < laender.length; i++) {
            jCAnbaugebiet.addItem(laender[i]);
        }
    }

    /**
     * Füllt ein Array mit Ländern.
     */
    private void fuelleRebLaender() {
        boolean hinzugefuegt = true;
        hinzugefuegt = REB_LAENDER.add(ARGENTINIENREG);
        hinzugefuegt = REB_LAENDER.add(AUSTRALIENREG);
        hinzugefuegt = REB_LAENDER.add(CHILEREG);
        hinzugefuegt = REB_LAENDER.add(DEUTSCHLANDREG);
        hinzugefuegt = REB_LAENDER.add(FRANKREICHREG);
        hinzugefuegt = REB_LAENDER.add(GRIECHENLANDREG);
        hinzugefuegt = REB_LAENDER.add(ITALIENREG);
        hinzugefuegt = REB_LAENDER.add(KROATIENREG);
        hinzugefuegt = REB_LAENDER.add(MAZEDONIENREG);
        hinzugefuegt = REB_LAENDER.add(MEXIKOREG);
        hinzugefuegt = REB_LAENDER.add(NEUSEELANDREG);
        hinzugefuegt = REB_LAENDER.add(OESTERREICH);
        hinzugefuegt = REB_LAENDER.add(PORTUGALREG);
        hinzugefuegt = REB_LAENDER.add(RUMÄNIENREG);
        hinzugefuegt = REB_LAENDER.add(SCHWEIZREG);
        hinzugefuegt = REB_LAENDER.add(SPANIENREG);
        hinzugefuegt = REB_LAENDER.add(SUEDAFRIEKAREG);
        hinzugefuegt = REB_LAENDER.add(TSCHECHIENREG);
        hinzugefuegt = REB_LAENDER.add(UNGARNREG);
        hinzugefuegt = REB_LAENDER.add(USAREG);
    }

    private void fuelleJCRegion(String land) {
        String[] rebsorten = LAND_UND_REGION.get(land);
        jCRegion.removeAllItems();
        jCRegion.addItem("Bitte auswählen");
        if (rebsorten != null) {
            for (int i = 0; i < rebsorten.length; i++) {
                jCRegion.addItem(rebsorten[i]);
            }
        }
    }

    /**
     * Füllt eine Combobox mit Resborten.
     *
     * @param land
     */
    private void fuelleJCRebsorten(String[] rebsorten) {
        jCRebsorte1.removeAllItems();
        jCRebsorte3.removeAllItems();
        jCRebsorte2.removeAllItems();
//        jCRebsorte1.addItem("Bitte auswählen");
        if (rebsorten != null) {
            for (int i = 0; i < rebsorten.length; i++) {
                jCRebsorte1.addItem(rebsorten[i]);
                jCRebsorte2.addItem(rebsorten[i]);
                jCRebsorte3.addItem(rebsorten[i]);
            }
        }
    }

    /**
     * Legt in einer HashMap die Beziehung zwischen Rebsorten und Ländern fest.
     *
     * @param beziehung
     */
    private void fuelleLandUndRegionBeziehung(ArrayList<String[]> beziehung) {
        for (int i = 0; i < beziehung.size(); i++) {
            LAND_UND_REGION.put(LAENDER[i], beziehung.get(i));
        }
    }

    // Kunde ------------------------------------------------------------------
    private boolean kundenFormularVeraendert() {
        boolean veraendert = false;

        for (JComponent c : PFLICHTFELDER_KUNDE) {
            if (!veraendert && c instanceof JTextField) {
                if (((JTextField) c).equals(jTKundenNr)) {
                    veraendert = !((JTextField) c).getText().equals("0000" + kundenNr);
                } else {
                    veraendert = !((JTextField) c).getText().isEmpty();
                }
            } else if (!veraendert && c instanceof JRadioButton) {
                veraendert = !((JRadioButton) c).isSelected();
            }
        }

        return veraendert;
    }

    private void schliesseKundenInternalFrame() {
        if (kundenFormularVeraendert()) {
            int abbrechen = JOptionPane.showConfirmDialog(null, KUNDE_ABORT_MESSAGE,
                    KUNDE_ABORT_TITLE, JOptionPane.YES_NO_OPTION);

            if (abbrechen == 0) {
                jIFKundeAufnehmen.setVisible(false);
                resetInternalFrameKunde();
                resetVisibilityKunde();
            }
        } else {
            jIFKundeAufnehmen.setVisible(false);
            resetInternalFrameKunde();
            resetVisibilityKunde();
        }
    }

    private void felderLeer() {
        leereFelder = new ArrayList<>();
        for (JComponent c : PFLICHTFELDER_KUNDE) {
            if (c instanceof JTextField) {
                if (((JTextField) c).getText().equals("")) {
                    ((JTextField) c).setBackground(LEER);
                    leereFelder.add((JTextField) c);
                    if (leereFelder.size() == 1) {
                        c.requestFocusInWindow();
                    }
                    if (!jLAusfuellen.isVisible()) {
                        jLAusfuellen.setVisible(true);
                    }
                }
            }
        }
    }

    private void resetVisibilityKunde() {
        jLUngueltigKundenNr.setVisible(false);
        jLHilfeKundenNr.setVisible(false);
        jLUngueltigPLZ.setVisible(false);
        jLHilfePLZ.setVisible(false);
        jLUngueltigVorname.setVisible(false);
        jLUngueltigNachname.setVisible(false);
        jLHilfeNachname.setVisible(false);
        jLHilfeVorname.setVisible(false);
        jLUngueltigKontoinhaber.setVisible(false);
        jLHilfeKontoinhaber.setVisible(false);
        jLAusfuellen.setVisible(false);

        if (!leereFelder.isEmpty()) {
            for (JTextField jT : leereFelder) {
                jT.setBackground(Color.WHITE);
            }
            leereFelder = new ArrayList<>();
            jLAusfuellen.setVisible(false);
        }
    }

    private void resetInternalFrameKunde() {
        jTKundenNr.setText("0000" + kundenNr);
        jRHerr.setSelected(true);
        jTVorname.setText("");
        jTNachname.setText("");
        jTTelefonnummer.setText("");
        jTStrasse.setText("");
        jTHausnummer.setText("");
        jTPLZ.setText("");
        jTOrt.setText("");
        jTKontoinhaber.setText("");
        jTKontoNr.setText("");
        jTBankleitzahl.setText("");
        jTKreditinstitut.setText("");
        jCUebernehmen.setSelected(false);
        jTKontoinhaber.setEnabled(true);
    }

    private void speichern(File f) {
        File fileEingabe = f;
        if (!fileEingabe.getName().toLowerCase().endsWith(".wkd")) {
            fileEingabe = new File(f.getPath() + ".wkd");
        }
        try {
            BufferedWriter ausgabe = new BufferedWriter(new FileWriter(fileEingabe));
            if (weinDaten != null) {
                for (int i = 0; i < weinDaten.getWeinDaten().size(); i++) {
                    System.out.println("Wein;" + weinDaten.getWeinDaten().get(i).toString());
                    ausgabe.write("Wein;" + weinDaten.getWeinDaten().get(i).toString());
                    ausgabe.newLine();
                }
            }
            ausgabe.newLine();
            if (kundenDaten != null) {
                for (int i = 0; i < kundenDaten.getKundenDaten().size(); i++) {
                    System.out.println("Kunde;" + kundenDaten.getKundenDaten().get(i).toString());
                    ausgabe.write("Kunde;" + kundenDaten.getKundenDaten().get(i).toString());
                    ausgabe.newLine();
                }
            }
            ausgabe.close();
            JOptionPane.showMessageDialog(this, SPEICHERN_ERFOLGREICH_MESSAGE, SPEICHERN_ERFOLGREICH_TITLE, JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, FEHLER_SPEICHERN_MESSAGE, FEHLER_SPEICHERN_TITLE, JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Weinverwaltung.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Weinverwaltung().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bGAnrede;
    private javax.swing.ButtonGroup bGFarben;
    private javax.swing.JMenu bearbeitenMenue;
    private javax.swing.JMenuItem beendenMenuItem;
    private javax.swing.JMenu dateiMenue;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenuItem hilfeMenuItem;
    private javax.swing.JMenu hilfeMenue;
    private javax.swing.JButton jBAbbrechen;
    private javax.swing.JButton jBAbbrechenKunde;
    private javax.swing.JButton jBFlaschenpreisInLiterpreis;
    private javax.swing.JButton jBLiterpreisInFlaschenpreis;
    private javax.swing.JButton jBSpeichern;
    private javax.swing.JButton jBSpeichernKunde;
    private javax.swing.JComboBox jCAlkoholgehalt;
    private javax.swing.JComboBox jCAnbaugebiet;
    private javax.swing.JComboBox jCFlaschengroesse;
    private static javax.swing.JComboBox jCLagerdauer;
    private javax.swing.JComboBox jCRebsorte1;
    private javax.swing.JComboBox jCRebsorte2;
    private javax.swing.JComboBox jCRebsorte3;
    private javax.swing.JComboBox jCRegion;
    private javax.swing.JCheckBox jCUebernehmen;
    private javax.swing.JInternalFrame jIFKundeAufnehmen;
    private javax.swing.JInternalFrame jIFweinAufnehmen;
    private javax.swing.JLabel jLAlkoholgehalt;
    private javax.swing.JLabel jLAnbaugebiet;
    private javax.swing.JLabel jLAnrede;
    private javax.swing.JLabel jLAnschrift;
    private javax.swing.JLabel jLAusfuellen;
    private javax.swing.JLabel jLAusfuellenName;
    private javax.swing.JLabel jLAuswaehlenAnbaugebiet;
    private javax.swing.JLabel jLBankleitzahl;
    private javax.swing.JLabel jLBankverbindung;
    private javax.swing.JLabel jLBestellnummer;
    private javax.swing.JLabel jLComboboxDoppelt;
    private javax.swing.JLabel jLEuro;
    private javax.swing.JLabel jLEuro2;
    private javax.swing.JLabel jLFarbe;
    private javax.swing.JLabel jLFlaschengroesse;
    private javax.swing.JLabel jLFlaschenpreis;
    private javax.swing.JLabel jLHausnummer;
    private javax.swing.JLabel jLHilfeBestellNr;
    private javax.swing.JLabel jLHilfeJahrgang;
    private javax.swing.JLabel jLHilfeKontoinhaber;
    private javax.swing.JLabel jLHilfeKundenNr;
    private javax.swing.JLabel jLHilfeLagerfaehigkeit;
    private javax.swing.JLabel jLHilfeNachname;
    private javax.swing.JLabel jLHilfePLZ;
    private javax.swing.JLabel jLHilfeVorname;
    private javax.swing.JLabel jLJahrgang;
    private javax.swing.JLabel jLKontoNr;
    private javax.swing.JLabel jLKontoinhaber;
    private javax.swing.JLabel jLKreditinstitut;
    private javax.swing.JLabel jLKundendaten;
    private javax.swing.JLabel jLKundennummer;
    private javax.swing.JLabel jLLagerfaehigkeit;
    private javax.swing.JLabel jLNachname;
    private javax.swing.JLabel jLName;
    private javax.swing.JLabel jLOrt;
    private javax.swing.JLabel jLPLZ;
    private javax.swing.JLabel jLPreisProLiter;
    private javax.swing.JLabel jLRebsorte;
    private javax.swing.JLabel jLRegion;
    private javax.swing.JLabel jLRegionAuswaehlen;
    private javax.swing.JLabel jLStrasse;
    private javax.swing.JLabel jLTelefonNr;
    private javax.swing.JLabel jLUngueltigBestellNr;
    private javax.swing.JLabel jLUngueltigFlaschenpreis;
    private javax.swing.JLabel jLUngueltigJahrgang;
    private javax.swing.JLabel jLUngueltigKontoinhaber;
    private javax.swing.JLabel jLUngueltigKundenNr;
    private javax.swing.JLabel jLUngueltigLagerfaehigkeit;
    private javax.swing.JLabel jLUngueltigLiterpreis;
    private javax.swing.JLabel jLUngueltigNachname;
    private javax.swing.JLabel jLUngueltigPLZ;
    private javax.swing.JLabel jLUngueltigVorname;
    private javax.swing.JLabel jLVorname;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPDiagramm;
    private javax.swing.JPanel jPFormular;
    public javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRFrau;
    private javax.swing.JRadioButton jRHerr;
    private javax.swing.JRadioButton jRRose;
    private javax.swing.JRadioButton jRRot;
    private javax.swing.JRadioButton jRWeiss;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextField jTBankleitzahl;
    private javax.swing.JTextField jTBestellnummer;
    private javax.swing.JTextField jTFlaschenpreis;
    private javax.swing.JTextField jTHausnummer;
    private javax.swing.JTextField jTJahrgang;
    private javax.swing.JTextField jTKontoNr;
    private javax.swing.JTextField jTKontoinhaber;
    private javax.swing.JTextField jTKreditinstitut;
    private javax.swing.JTextField jTKundenNr;
    private javax.swing.JTextField jTLagerfaehigkeit;
    private javax.swing.JTextField jTLiterpreis;
    private javax.swing.JTextField jTNachname;
    private javax.swing.JTextField jTName;
    private javax.swing.JTextField jTOrt;
    private javax.swing.JTextField jTPLZ;
    private javax.swing.JTabbedPane jTPWeinaufnahme;
    private javax.swing.JTextField jTStrasse;
    private javax.swing.JTextField jTTelefonnummer;
    private javax.swing.JTextField jTVorname;
    private javax.swing.JMenuItem kundenAendernMenuItem;
    private javax.swing.JMenuItem kundenAufnehmenMenuItem;
    private javax.swing.JMenuItem kundenLoeschenMenuItem;
    private javax.swing.JMenu kundenMenue;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem oeffnenMenuItem;
    private javax.swing.JMenuItem speichernMenuItem;
    private javax.swing.JMenuItem speichernUnterMenuItem;
    private javax.swing.JMenuItem weinAendernMenuItem;
    private javax.swing.JMenuItem weinAufnehmenMenuItem;
    private javax.swing.JMenuItem weinLoeschenMenuItem;
    private javax.swing.JMenu weinMenue;
    private WeinreifeDiagramm weinreifeDiagramm;
    private WeinreifeLegende weinreifeLegende;
    // End of variables declaration//GEN-END:variables

}
