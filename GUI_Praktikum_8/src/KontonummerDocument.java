
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dennis
 */
public class KontonummerDocument extends PlainDocument {

    private final String REGEX = "[0-9]";
    private final int MAX_LAENGE = 10;
    
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        boolean gueltig = true;
        for (int i = 0; i < str.length(); i++) {
            if (!("" + str.charAt(i)).matches(REGEX) || offs > MAX_LAENGE-1) {
               gueltig = false;
               Toolkit.getDefaultToolkit().beep();
            }
        }
        
        if(gueltig) {
            super.insertString(offs, str, a);
        }
    }
    
}