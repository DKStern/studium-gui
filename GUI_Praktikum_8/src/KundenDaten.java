
import java.util.ArrayList;

/**
 *
 * @author dennis
 */
public class KundenDaten {
    private ArrayList<Kunde> kundenDaten;
    
    public KundenDaten() {
        kundenDaten = new ArrayList<>();
    }
    
    public boolean kundeHinzufuegen(Kunde kunde) {
        return kundenDaten.add(kunde);
    }
    
    public ArrayList<Kunde> getKundenDaten() {
        return kundenDaten;
    }
    
    @Override
    public String toString() {
        String inhalt = "";
        
        for (int i = 0; i < kundenDaten.size(); i++) {
            inhalt += "Kunde;" + kundenDaten.get(i).toString() + "\n";
        }
        
        return inhalt;
    }
}
