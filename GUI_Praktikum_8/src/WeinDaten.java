
import java.util.ArrayList;

/**
 *
 * @author dennis
 */
public class WeinDaten {
    private ArrayList<Wein> weinDaten;
    
    public WeinDaten() {
        weinDaten = new ArrayList<>();
    }
    
    public boolean weinDatenHinzufuegn(Wein wein) {
        return weinDaten.add(wein);
    }
    
    public ArrayList<Wein> getWeinDaten() {
        return weinDaten;
    }
    
    @Override
    public String toString() {
        String inhalt = "";
        
        for (int i = 0; i < weinDaten.size(); i++) {
            inhalt += "Wein;" + weinDaten.get(i).toString() + "\n";
        }
        
        return inhalt;
    }
}
