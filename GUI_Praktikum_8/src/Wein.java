
import java.util.Objects;


/**
 *
 * @author dennis
 */
public class Wein {

    private String bestellnummer;
    private int jahrgang;
    private String name;
    private String farbe;
    private String[] rebsorten;
    private String anbaugebiet;
    private double alkoholgehalt;
    private int lagerfaehigkeit;
    private double flaschengroesse;
    private double flaschenpreis;
    private static String trenner = ";";
    private static int anzahlAttribute = 10;
    
    private static String anzahlZuWenig = "Die Zeichenfolge enthält weniger"
            + " als " + anzahlAttribute + " Attribute! -> wird übersprungen";

     private static final String BESTELLNUMMER = "([A-Z]{2}|[a-z]{2})[0-9]{2}";
     private static final String JAHRGANG = "[0-9]{4}";
     
    
    public Wein(String bestellnummer, int jahrgang, String name, String farbe,
            String[] rebsorten, String anbaugebiet, double alkoholgehalt,
            int lagerfaehigkeit, double flaschengroesse, double flaschenpreis) {
        this.bestellnummer = bestellnummer;
        this.jahrgang = jahrgang;
        this.name = name;
        this.farbe = farbe;
        this.rebsorten = rebsorten;
        this.anbaugebiet = anbaugebiet;
        this.alkoholgehalt = alkoholgehalt;
        this.lagerfaehigkeit = lagerfaehigkeit;
        this.flaschengroesse = flaschengroesse;
        this.flaschenpreis = flaschenpreis;
    }

    @Override
    public String toString() {
        String wein;

        wein = bestellnummer + trenner
                + jahrgang + trenner
                + name + trenner
                + farbe + trenner
                + getRebsorten() + trenner
                + anbaugebiet + trenner
                + alkoholgehalt + trenner
                + lagerfaehigkeit + trenner
                + flaschengroesse + trenner
                + flaschenpreis;

        return wein;
    }
    
    public String getBestellnummer() {
        return bestellnummer;
    }
    
    public int getLagerdauer() {
        return lagerfaehigkeit - jahrgang;
    }
    
    public int getJahrgang() {
        return jahrgang;
    }

    public String getRebsorten() {
        String reb = "";
        if(rebsorten[0] != null) {
            reb = rebsorten[0];
        } else if (rebsorten[0] == null) {
            reb = "keine";
        } 
        
        for (int i = 1; i < rebsorten.length && rebsorten[i] != null; i++) {
            reb += ", " + rebsorten[i];
        }

        return reb;
    }
    
    public static Wein valueOf(String[] str) throws IllegalArgumentException {
        Wein wein = null;
//        String[] str = str.split(trenner);
        
        if (str.length < anzahlAttribute) {
            throw new IllegalArgumentException(anzahlZuWenig);
        }
        
        for (int i = 0; i < str.length; i++) {
            str[i] = str[i].trim();
        }
        
        String[] eingResborten = str[5].split(",");
        
        if (!str[1].matches(BESTELLNUMMER)) {
            System.out.println(str[1]);
            throw new IllegalArgumentException("Ungueltige Bestellnummer!");
        }
        
        try {
            wein = new Wein(str[1], Integer.parseInt(str[2]),str[3], str[4], eingResborten, str[6], 
                    Double.parseDouble(str[7]), Integer.parseInt(str[8]), Double.parseDouble(str[9]), Double.parseDouble(str[10]));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Ungueltiger Wert!");
        }
        
        return wein;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.bestellnummer);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Wein other = (Wein) obj;
        if (!Objects.equals(this.bestellnummer, other.bestellnummer)) {
            return false;
        }
        return true;
    }
    
}
