
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dennis
 */
public class BuchstabenDocument extends PlainDocument {

    private final String REGEX = "[a-zA-Z]|-|'| |ä|Ä|ü|Ü|ö|Ö";
    private final String DELIM = ";";

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        boolean gueltig = true;
        for (int i = 0; i < str.length() && gueltig; i++) {
            if (!("" + str.charAt(i)).matches(REGEX) || ("" + str.charAt(i)).equals(DELIM)) {
                Toolkit.getDefaultToolkit().beep();
                gueltig = false;
            }
        }

        if (gueltig) {
            super.insertString(offs, str, a);
        }
    }

}
