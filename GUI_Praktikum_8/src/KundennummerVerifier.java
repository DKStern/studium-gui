
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author dennis
 */
public class KundennummerVerifier extends InputVerifier {

    private final String REGEX = "[0-9]{5}";

    @Override
    public boolean verify(JComponent input) {
        String eingabe = ((JTextField) input).getText();
        return eingabe.matches(REGEX);
    }
}
