
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dennis
 */
public class TelefonDocument extends PlainDocument {
    
    private final String REGEX = "[0-9]|\\/|\\(|\\)|\\+|";

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        boolean gueltig = true;
        
        for (int i = 0; i < str.length(); i++) {
            if (!("" + str.charAt(i)).matches(REGEX)) {
                Toolkit.getDefaultToolkit().beep();
                gueltig = false;
            }
        }
        if (gueltig) {
            super.insertString(offs, str, a);
        }
        
    }
    
    
    
}
