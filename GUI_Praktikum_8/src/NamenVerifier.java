
import java.util.StringTokenizer;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author dennis
 */
public class NamenVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent input) {
        String eingabe = ((JTextField)input).getText();
        boolean gueltig = !eingabe.equals("");
        StringTokenizer st = new StringTokenizer(eingabe, "-' ");
        while (st.hasMoreTokens() && gueltig) {
            String token = st.nextToken();
            for (int i = 0; i < token.length(); i++) {
                gueltig = Character.isLetter(token.charAt(i));
            }
        }
        
        return gueltig;
    }
    
}
