
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dennis
 */
public class PLZDocument extends PlainDocument {

    private final String REGEX = "[0-9]*";
    private final int MAX_LAENGE = 5;

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        boolean gueltig = true;
        
        if (!str.matches(REGEX) || offs > MAX_LAENGE-1) {
            Toolkit.getDefaultToolkit().beep();
            gueltig = false;
        }

        if (gueltig) {
            super.insertString(offs, str, a);
        }
    }

}
