
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dennis
 */
public class JahrDocument extends PlainDocument {

    @Override
    public void insertString(int offs, String str, AttributeSet a)
            throws BadLocationException {
        boolean gueltig = true;
        String erlaubteZeichen = "[0-9]";
        for (int i = 0;gueltig && i < str.length(); i++) {
            if (!("" + str.charAt(i)).matches(erlaubteZeichen)) {
                Toolkit.getDefaultToolkit().beep();
                gueltig = false;
            }
        }
        if (gueltig) {
            super.insertString(offs, str, a);
        }
    }

}
