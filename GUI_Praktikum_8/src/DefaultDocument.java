
import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dennis
 */
public class DefaultDocument extends PlainDocument {
    private final String DELIM = ";";

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        boolean gueltig = true;
        if (str.equals(DELIM)) {
            gueltig = false;
            Toolkit.getDefaultToolkit().beep();
        }
        if (gueltig) {
            super.insertString(offs, str, a);
        }
        
    }
    
    
}
