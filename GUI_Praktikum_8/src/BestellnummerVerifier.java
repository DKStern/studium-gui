
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author dennis
 */
public class BestellnummerVerifier extends InputVerifier {

    private final String REGEX = "([A-Z]{2}|[a-z]{2})[0-9]{2}";

    @Override
    public boolean verify(JComponent component) {
        return ((JTextField) component).getText().matches(REGEX);
    }
}
