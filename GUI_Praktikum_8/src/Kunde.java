
import java.util.Objects;


/**
 *
 * @author dennis
 */
public class Kunde {

    private String kundenNr;
    private String anrede;
    private String vorname;
    private String nachname;
    private String strasse;
    private String hausNr;
    private String plz;
    private String ort;
    private String telefonNr;
    private String kontoinhaber;
    private String kontoNr;
    private String bankleitzahl;
    private String kreditinstitut;
    
    private static int anzahlAttribute = 13;
    private static String trenner = ";";
    
    private static String anzahlZuWenig = "Die Zeichenfolge enthält weniger"
            + "als " + anzahlAttribute + "Attribute";
    
    private static final String KUNDENNUMMER_UND_PLZ = "[0-9]{5}";

    public Kunde(String kundenNr, String anrede, String vorname, String nachname,
            String strasse, String hausNr, String plz, String ort, 
            String telefonNr, String kontoInhaber, String kontoNr,
            String bankleitzahl, String kreditinstitut) {
        this.kundenNr = kundenNr;
        this.anrede = anrede;
        this.vorname = vorname;
        this.nachname = nachname;
        this.strasse = strasse;
        this.hausNr = hausNr;
        this.plz = plz;
        this.ort = ort;
        this.telefonNr = telefonNr;
        this.kontoinhaber = kontoInhaber;
        this.kontoNr = kontoNr;
        this.bankleitzahl = bankleitzahl;
        this.kreditinstitut = kreditinstitut;
        
    }
    
    @Override
    public String toString() {
        return kundenNr + trenner
                + anrede + trenner
                + vorname + trenner
                + nachname + trenner
                + strasse + trenner
                + hausNr + trenner
                + plz + trenner
                + ort + trenner
                + telefonNr + trenner
                + kontoinhaber + trenner
                + kontoNr + trenner
                + bankleitzahl + trenner
                + kreditinstitut;
    }
    
    public String getKundenNummer() {
        return kundenNr;
    }
    
    public static Kunde valueOf(String[] str) {
        Kunde kunde = null;
        
        if (str.length < anzahlAttribute) {
            throw new IllegalArgumentException(anzahlZuWenig);
        }
        
        for (int i = 0; i < str.length; i++) {
            str[i] = str[i].trim();
        }
        
        if (!str[1].matches(KUNDENNUMMER_UND_PLZ)) {
            throw new IllegalArgumentException("Kundennummer ungültug!");
        }
        
        if (!str[7].matches(KUNDENNUMMER_UND_PLZ)) {
            throw new IllegalArgumentException("Postleitzahl ungültig!");
        }
        
        kunde = new Kunde(str[1], str[2], str[3], str[4], str[5], str[6],
                    str[7], str[8], str[9], str[10], str[11], str[12], 
                    str[13]);
        
        return kunde;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.kundenNr);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Kunde other = (Kunde) obj;
        if (!Objects.equals(this.kundenNr, other.kundenNr)) {
            return false;
        }
        return true;
    }
    
}
