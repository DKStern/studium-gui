
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Prüft Eingaben auf Gültigkeit.
 * 
 * @author dennis
 */
public class ZahlenVerifier extends InputVerifier{
    
    private static final String REGEX_KOMMA = "(\\d*,?\\d*)|(\\d{0,3}(\\.\\d{3})*,?\\d*)";
    
    private static final String PARSE_FEHLER_TITEL = "Keine Zahl";
    private static final String PARSE_FEHLER_MELDUNG = "Bitte geben Sie eine Zahl im deutschen Format \nmit Tausenderpunkten (z.B. 2.345,66) ein.";

    @Override
    public boolean verify(JComponent input) {
        return ((JTextField) input).getText().matches(REGEX_KOMMA);
    }
    
    @Override
    public boolean shouldYieldFocus(JComponent input) {
        boolean gueltig = verify(input);
        
        if(!gueltig) {
            JOptionPane.showMessageDialog(null, PARSE_FEHLER_MELDUNG, PARSE_FEHLER_TITEL, JOptionPane.WARNING_MESSAGE);
        }
        
        return gueltig;
    }
}
